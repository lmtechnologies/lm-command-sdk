
#line 1 "build/lm048lib.rl"
/** Copyright LM Technologies 2016
 *
 * This file is licensed under the terms of the Adaptive Public License 
 * set forth in licenses.txt in the root directory of the LM Command SDK 
 * repository.
 *
 * Author(s): Richard Palethorpe <richard@lm-technologies.com>
 */

#include "lm048lib.h"
#include <string.h>
#include <stdio.h>

#define CR LM048_COMMAND_DELIMETER
#define CRLF LM048_RESPONSE_DELIMETER
#define ATP "AT+"

#define MIN(a, b) (a) < (b) ? (a) : (b)

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#endif


#line 30 "build/lm048lib.c"
static const int skipline_start = 1;
static const int skipline_first_final = 2;
static const int skipline_error = 0;

static const int skipline_en_main = 1;


#line 32 "build/lm048lib.rl"


LM048_API enum LM048_STATUS lm048_skip_line(char *const data,
					    size_t *const length)
{
	if(*length > 0){
		int cs;
		char *p = data;
		char *pe = p + *length;

		
#line 50 "build/lm048lib.c"
	{
	cs = skipline_start;
	}

#line 43 "build/lm048lib.rl"
		
#line 57 "build/lm048lib.c"
	{
	if ( p == pe )
		goto _test_eof;
	switch ( cs )
	{
st1:
	if ( ++p == pe )
		goto _test_eof1;
case 1:
	if ( (*p) == 13 )
		goto tr1;
	goto st1;
tr1:
#line 29 "build/lm048lib.rl"
	{ {p++; cs = 2; goto _out;} }
	goto st2;
st2:
	if ( ++p == pe )
		goto _test_eof2;
case 2:
#line 78 "build/lm048lib.c"
	goto st0;
st0:
cs = 0;
	goto _out;
	}
	_test_eof1: cs = 1; goto _test_eof; 
	_test_eof2: cs = 2; goto _test_eof; 

	_test_eof: {}
	_out: {}
	}

#line 44 "build/lm048lib.rl"

		*length -= (size_t)(p - data);

		if(cs == 2){
			return LM048_COMPLETED;
		}
	}

	return LM048_OK;
}

#define PTYPE(t) pkt->type = t
#define PMOD(m) pkt->modifier = m

#line 106 "build/lm048lib.c"
static const int atcmd_start = 1;
static const int atcmd_first_final = 582;
static const int atcmd_error = 0;

static const int atcmd_en_value_resp = 437;
static const int atcmd_en_events = 444;
static const int atcmd_en_main = 1;


#line 403 "build/lm048lib.rl"


void lm048_no_op(){}
void lm048_no_op_e(int cs, char c){}

#ifdef __clang__
#pragma clang diagnostic pop
#endif

static struct lm048_packet
default_queue_array[LM048_DEFAULT_QUEUE_LENGTH][2];

static struct lm048_queue default_queue = {
	.array = default_queue_array,
	.front = 0,
	.back = 0,
	.length = LM048_DEFAULT_QUEUE_LENGTH
};

static char default_payload[LM048_DEFAULT_PAYLOAD_LENGTH];
LM048_API struct lm048_parser lm048_default_state = {
	.cs = 1,
	.on_ok_response = lm048_no_op,
	.on_error_response = lm048_no_op,
	.on_error = lm048_no_op_e,
	.on_completed = NULL,
	.queue = &default_queue,
	.current.payload = default_payload,
	.current.payload_length = 0,
	.current.payload_capacity = LM048_DEFAULT_PAYLOAD_LENGTH
};

static enum LM048_STATUS payload_add(struct lm048_packet *const pkt, char c)
{
	if(pkt->payload_length < pkt->payload_capacity){
		pkt->payload[pkt->payload_length] = c;
		pkt->payload_length += 1;
		return LM048_COMPLETED;
	}

	return LM048_FULL;
}

static int packet_is_evnt(struct lm048_packet const *const pkt)
{
	switch(pkt->type){
	case LM048_AT_CONNECT_EVNT:
	case LM048_AT_PINREQ_EVNT:
	case LM048_AT_PASSKEYCFM_EVNT:
	case LM048_AT_PASSKEYDSP_EVNT:
	case LM048_AT_PASSKEYREQ_EVNT:
		return 1;
	}

	return 0;
}

LM048_API enum LM048_STATUS lm048_packet_init(struct lm048_packet *const pkt,
				char *const payload,
				size_t payload_capacity)
{
	pkt->type = LM048_AT_NONE;
	pkt->modifier = LM048_ATM_NONE;
	pkt->payload = payload;
	pkt->payload_length = 0;
	pkt->payload_capacity = payload_capacity;

	return LM048_COMPLETED;
}

LM048_API enum LM048_STATUS lm048_enqueue(struct lm048_queue *const queue,
				struct lm048_packet const command,
				struct lm048_packet const response)
{
	struct lm048_queue *que = queue;

	if(que == NULL){
		que = &default_queue;
	}
	que->array[que->back][0] = command;
	que->array[que->back][1] = response;
	if(que->length <= ++que->back){
		que->back = 0;
		if(que->back == que->front){
			return LM048_FULL;
		}
	}

	return LM048_COMPLETED;
}

LM048_API enum LM048_STATUS lm048_queue_remove(struct lm048_queue *const queue,
					       size_t count)
{
	struct lm048_queue *que = queue;
	if(que == NULL)
		que = &default_queue;

	ptrdiff_t diff = que->back - que->front;
	size_t last = que->length - 1;

	if(diff == 0)
		goto exit;

	if(diff > 0){
		que->front += MIN((size_t)diff, count);
		goto exit;
	}

	if(last >= (que->front + count)){
		que->front += count;
		goto exit;
	}

	count -= last - que->front;
	que->front = MIN(count, que->back);
exit:
	if(que->front == que->back)
		return LM048_EMPTY;

	return LM048_COMPLETED;
}

LM048_API void lm048_queue_clear(struct lm048_queue *const queue)
{
	struct lm048_queue *que = queue;
	if(que == NULL)
		que = &default_queue;

	que->front = que->back;
}

static enum LM048_STATUS dequeue(struct lm048_queue *const queue,
				 struct lm048_packet const *const received)
{
	if(queue->front == queue->back){
		return LM048_COMPLETED;
	}

	struct lm048_packet *cmd_echo = &(queue->array[queue->front][0]);
	struct lm048_packet *expected = &(queue->array[queue->front][1]);

	if(cmd_echo->type == received->type){
		return LM048_OK;
	}

	if(queue->length <= ++queue->front){
		queue->front = 0;
	}

	if(expected->type != received->type
	   && !(expected->type == LM048_AT_ANY_EVNT
		&& packet_is_evnt(expected))){
		return LM048_UNEXPECTED;
	}

	if(lm048_packet_has_modifier(expected)
	   && expected->modifier != LM048_ATM_ANY
	   && expected->modifier != received->modifier){
		return LM048_UNEXPECTED;
	}

	if(lm048_packet_has_payload(expected)
	   && expected->payload_length != 0
	   && (expected->payload_length != received->payload_length
	       || strncmp(expected->payload,
			  received->payload,
			  expected->payload_length)))
	{
		return LM048_UNEXPECTED;
	}

	return LM048_DEQUEUED;
}

LM048_API enum LM048_STATUS
lm048_queue_front(struct lm048_queue const *const queue,
		  struct lm048_packet const **cmd,
		  struct lm048_packet const **resp)
{
	struct lm048_queue const *que = &default_queue;
	if(queue != NULL){
		que = queue;
	}

	enum LM048_STATUS ret;

	if(que->front == que->back){
		if(cmd != NULL)
			*cmd = NULL;
		if(resp != NULL)
			*resp = NULL;
		ret = LM048_EMPTY;
	}else{
		if(cmd != NULL)
			*cmd = &(que->array[que->front][0]);
		if(resp != NULL)
			*resp = &(que->array[que->front][1]);
		ret = LM048_COMPLETED;
	}

	return ret;
}

LM048_API struct lm048_queue
lm048_queue_init(struct lm048_packet (*const array)[2],
		 size_t const length)
{
	struct lm048_queue q = {
		.array = array,
		.front = 0,
		.back = 0,
		.length = length
	};
	return q;
}

LM048_API int lm048_packet_has_modifier(struct lm048_packet const *const pkt)
{
	switch(pkt->type){
	case LM048_AT_NONE:
	case LM048_AT_OK:
	case LM048_AT_ERROR:
	case LM048_AT_AT:
	case LM048_AT_VALUE_RESPONSE:
	case LM048_AT_VER:
	case LM048_AT_VER_RESPONSE:
	case LM048_AT_RESET:
	case LM048_AT_ENQ:
	case LM048_AT_ADDR:
	case LM048_AT_DROP:
	case LM048_AT_AUTO:
	case LM048_AT_RSSI:
	case LM048_AT_RSSI_RESPONSE:
	case LM048_AT_ANY_EVNT:
	case LM048_AT_CONNECT_EVNT:
	case LM048_AT_CONNECT_FAIL_EVNT:
	case LM048_AT_DISCONNECT_EVNT:
	case LM048_AT_PINREQ_EVNT:
	case LM048_AT_PASSKEYCFM_EVNT:
	case LM048_AT_PASSKEYDSP_EVNT:
	case LM048_AT_PASSKEYREQ_EVNT:
		return 0;
	case LM048_AT_PIN:
	case LM048_AT_BAUD:
	case LM048_AT_BAUD_RESPONSE:
	case LM048_AT_STOP_BITS:
	case LM048_AT_STOP_BITS_RESPONSE:
	case LM048_AT_PAR:
	case LM048_AT_PAR_RESPONSE:
	case LM048_AT_FLOW:
	case LM048_AT_FLOW_RESPONSE:
	case LM048_AT_ECHO:
	case LM048_AT_ECHO_RESPONSE:
	case LM048_AT_RESP:
	case LM048_AT_RESP_RESPONSE:
	case LM048_AT_MODEM:
	case LM048_AT_MODEM_RESPONSE:
	case LM048_AT_ROLE:
	case LM048_AT_ROLE_RESPONSE:
	case LM048_AT_NAME:
	case LM048_AT_DCOV:
	case LM048_AT_DCOV_RESPONSE:
	case LM048_AT_CONN:
	case LM048_AT_BOND:
	case LM048_AT_ACON:
	case LM048_AT_ACON_RESPONSE:
	case LM048_AT_ESC:
	case LM048_AT_ESC_RESPONSE:
	case LM048_AT_RCFG:
	case LM048_AT_RCFG_RESPONSE:
	case LM048_AT_SLEEP:
	case LM048_AT_SLEEP_RESPONSE:
	case LM048_AT_DPIN:
	case LM048_AT_DPIN_RESPONSE:
	case LM048_AT_IOTYPE:
	case LM048_AT_IOTYPE_RESPONSE:
	case LM048_AT_MITM:
	case LM048_AT_MITM_RESPONSE:
	case LM048_AT_PASSCFM:
	case LM048_AT_PASSKEY:
	case LM048_AT_RTS:
	case LM048_AT_RTS_RESPONSE:
	case LM048_AT_CTS:
	case LM048_AT_CTS_RESPONSE:
		return 1;
	default:
		return -1;
	}
}

LM048_API int lm048_packet_has_payload(struct lm048_packet const *const pkt)
{
	switch(pkt->type){

	//-Never has payload
	case LM048_AT_NONE:
	case LM048_AT_OK:
	case LM048_AT_ERROR:
	case LM048_AT_AT:
	case LM048_AT_VER:
	case LM048_AT_RESET:
	case LM048_AT_ENQ:
	case LM048_AT_BAUD:
	case LM048_AT_BAUD_RESPONSE:
	case LM048_AT_PAR:
	case LM048_AT_PAR_RESPONSE:
	case LM048_AT_FLOW:
	case LM048_AT_FLOW_RESPONSE:
	case LM048_AT_ECHO:
	case LM048_AT_ECHO_RESPONSE:
	case LM048_AT_RESP:
	case LM048_AT_RESP_RESPONSE:
	case LM048_AT_MODEM:
	case LM048_AT_MODEM_RESPONSE:
	case LM048_AT_ROLE:
	case LM048_AT_ROLE_RESPONSE:
	case LM048_AT_DCOV:
	case LM048_AT_DCOV_RESPONSE:
	case LM048_AT_DROP:
	case LM048_AT_ACON:
	case LM048_AT_ACON_RESPONSE:
	case LM048_AT_ESC:
	case LM048_AT_ESC_RESPONSE:
	case LM048_AT_AUTO:
	case LM048_AT_RCFG:
	case LM048_AT_RCFG_RESPONSE:
	case LM048_AT_SLEEP:
	case LM048_AT_SLEEP_RESPONSE:
	case LM048_AT_DPIN:
	case LM048_AT_DPIN_RESPONSE:
	case LM048_AT_IOTYPE:
	case LM048_AT_IOTYPE_RESPONSE:
	case LM048_AT_MITM:
	case LM048_AT_MITM_RESPONSE:
	case LM048_AT_CONNECT_FAIL_EVNT:
	case LM048_AT_RSSI:
	case LM048_AT_RTS:
	case LM048_AT_RTS_RESPONSE:
	case LM048_AT_CTS:
	case LM048_AT_CTS_RESPONSE:
		return 0;

	//-Always has payload
	case LM048_AT_VALUE_RESPONSE:
	case LM048_AT_VER_RESPONSE:
	case LM048_AT_CONNECT_EVNT:
	case LM048_AT_DISCONNECT_EVNT:
	case LM048_AT_CONN:
	case LM048_AT_PASSCFM:
	case LM048_AT_PASSKEY:
	case LM048_AT_PINREQ_EVNT:
	case LM048_AT_PASSKEYCFM_EVNT:
	case LM048_AT_PASSKEYDSP_EVNT:
	case LM048_AT_PASSKEYREQ_EVNT:
		return 1;

	//-Depends on modifier
	case LM048_AT_PIN:
	case LM048_AT_ADDR:
	case LM048_AT_NAME:
	case LM048_AT_BOND:
		break;
	default:
		return -1;
	}

	switch(pkt->modifier){
	case LM048_ATM_ENABLE:
	case LM048_ATM_DISABLE:
	case LM048_ATM_GET:
		return 0;
	case LM048_ATM_SET:
		return 1;
	}
	return -1;
}

#define CASE_CMD(c)\
	case LM048_AT_##c:\
		cmd = ATP #c;\
		break

#define CASE_RESP(c)\
	case LM048_AT_##c##_RESPONSE:\
		cmd = ATP #c;\
		end = CRLF "OK" CRLF;\
		break
LM048_API enum LM048_STATUS
lm048_write_packet(struct lm048_packet const *const packet,
		   char *const buf,
		   size_t *const length)
{
	size_t len = 0;
	char const *cmd;
	char const *mod;
	char const *end = CR;

	switch(packet->type){
	case LM048_AT_NONE:
		return LM048_OK;
	case LM048_AT_OK:
		cmd = CRLF "OK";
		end = CRLF;
		break;
	case LM048_AT_ERROR:
		cmd = CRLF "ERROR";
		end = CRLF;
		break;
	case LM048_AT_AT:
		cmd = "AT";
		break;
	case LM048_AT_VALUE_RESPONSE:
		cmd = CRLF;
		end = CRLF "OK" CRLF;
		break;
	CASE_CMD(VER);
	case LM048_AT_VER_RESPONSE:
		cmd = CRLF "F/W VERSION:";
		end = CRLF "OK" CRLF;
		break;
	CASE_CMD(BAUD);
	CASE_RESP(BAUD);
	CASE_CMD(PIN);
	CASE_CMD(RESET);
	CASE_CMD(ENQ);
	case LM048_AT_STOP_BITS:
		cmd = ATP "STOP";
		break;
	case LM048_AT_STOP_BITS_RESPONSE:
		cmd = CRLF "STOP";
		end = CRLF "OK" CRLF;
		break;
	CASE_CMD(PAR);
	CASE_RESP(PAR);
	CASE_CMD(FLOW);
	CASE_RESP(FLOW);
	CASE_CMD(ECHO);
	CASE_RESP(ECHO);
	CASE_CMD(RESP);
	CASE_RESP(RESP);
	CASE_CMD(MODEM);
	CASE_RESP(MODEM);
	CASE_CMD(ROLE);
	CASE_RESP(ROLE);
	CASE_CMD(ADDR);
	CASE_CMD(RSSI);
	CASE_CMD(NAME);
	CASE_CMD(DCOV);
	CASE_RESP(DCOV);
	CASE_CMD(CONN);
	CASE_CMD(DROP);
	CASE_CMD(BOND);
	CASE_CMD(ACON);
	CASE_RESP(ACON);
	CASE_CMD(ESC);
	CASE_RESP(ESC);
	CASE_CMD(AUTO);
	CASE_CMD(RCFG);
	CASE_RESP(RCFG);
	CASE_CMD(SLEEP);
	CASE_RESP(SLEEP);
	CASE_CMD(DPIN);
	CASE_RESP(DPIN);
	CASE_CMD(IOTYPE);
	CASE_RESP(IOTYPE);
	CASE_CMD(MITM);
	CASE_RESP(MITM);
	CASE_CMD(PASSCFM);
	CASE_CMD(PASSKEY);
	default:
		*length = 0;
		return LM048_ERROR;
	}

	switch(packet->modifier){
	case LM048_ATM_ENABLE:
		mod = "+";
		break;
	case LM048_ATM_DISABLE:
		mod = "-";
		break;
	case LM048_ATM_GET:
		mod = "?";
		break;
	case LM048_ATM_SET:
		mod = "=";
		break;
	case LM048_ATM_BAUD_1200:
		mod = "10";
		break;
	case LM048_ATM_BAUD_2400:
		mod = "11";
		break;
	case LM048_ATM_BAUD_4800:
		mod = "12";
		break;
	case LM048_ATM_BAUD_9600:
		mod = "13";
		break;
	case LM048_ATM_BAUD_19200:
		mod = "14";
		break;
	case LM048_ATM_BAUD_38400:
		mod = "15";
		break;
	case LM048_ATM_BAUD_57600:
		mod = "16";
		break;
	case LM048_ATM_BAUD_115200:
		mod = "17";
		break;
	case LM048_ATM_BAUD_230400:
		mod = "18";
		break;
	case LM048_ATM_BAUD_460800:
		mod = "19";
		break;
	case LM048_ATM_BAUD_921600:
		mod = "20";
		break;
	case LM048_ATM_NO_PARITY:
	case LM048_ATM_DISPLAY_ONLY:
		mod = "0";
		break;
	case LM048_ATM_ONE_STOPBIT:
	case LM048_ATM_ODD_PARITY:
	case LM048_ATM_DISPLAY_YES_NO:
		mod = "1";
		break;
	case LM048_ATM_TWO_STOPBIT:
	case LM048_ATM_EVEN_PARITY:
	case LM048_ATM_KEYBOARD_ONLY:
		mod = "2";
		break;
	case LM048_ATM_NO_DISPLAY_KEYBOARD:
		mod = "3";
		break;
	case LM048_ATM_REJECT_BT21:
		mod = "4";
		break;
	case LM048_ATM_LOCAL_LOOPBACK:
		mod = "L";
		break;
	case LM048_ATM_REMOTE_TRANSFER:
		mod = "R";
		break;
	case LM048_ATM_MASTER:
		mod = "M";
		break;
	case LM048_ATM_SLAVE:
		mod = "S";
		break;
	case LM048_ATM_NONE:
		break;
	}

	len = strlen(cmd) + strlen(end);
	if(lm048_packet_has_modifier(packet))
		len += strlen(mod);
	if(lm048_packet_has_payload(packet))
		len += packet->payload_length;

	if(len >= *length){
		*length = len;
		return LM048_FULL;
	}

	*buf = '\0';
	strcat(buf, cmd);
	if(lm048_packet_has_modifier(packet))
		strcat(buf, mod);
	if(lm048_packet_has_payload(packet))
		strncat(buf, packet->payload, packet->payload_length);
	strcat(buf, end);

	*length = len;
	return LM048_COMPLETED;
}

LM048_API enum LM048_STATUS
lm048_write_front_command(struct lm048_queue *const queue,
			 char *const buffer,
			 size_t *const length)
{
	struct lm048_packet const *cmd, *resp;
	struct lm048_queue *que = queue;
	if(que == NULL)
		que = &default_queue;

	enum LM048_STATUS s = lm048_queue_front(que, &cmd, &resp);

	if(s != LM048_COMPLETED){
		*length = 0;
		return s;
	}

	if(resp->type == LM048_AT_NONE)
		lm048_queue_remove(que, 1);

	return lm048_write_packet(cmd, buffer, length);
}


LM048_API enum LM048_STATUS lm048_inputs(struct lm048_parser *const state,
				         char const *const data,
				         size_t *const length)
{
	struct lm048_packet const *resp;
	struct lm048_packet *const pkt = &state->current;

	lm048_queue_front(state->queue, NULL, &resp);

	if(*length > 0 && data != NULL){
		char const *p = data;
		char const *pe = p + *length;
		char const *eof = NULL;

		
#line 736 "build/lm048lib.c"
	{
	}

#line 1022 "build/lm048lib.rl"
		
#line 742 "build/lm048lib.c"
	{
	int _ps = 0;
	if ( p == pe )
		goto _test_eof;
	switch (  state->cs )
	{
st1:
	if ( ++p == pe )
		goto _test_eof1;
case 1:
	_ps = 1;
	switch( (*p) ) {
		case 10: goto tr1;
		case 13: goto st1;
		case 65: goto st238;
		case 97: goto st238;
	}
	goto tr0;
tr0:
#line 72 "build/lm048lib.rl"
	{
		state->on_error((_ps), (*p));
	}
	goto st0;
#line 767 "build/lm048lib.c"
st0:
 state->cs = 0;
	goto _out;
tr1:
#line 150 "build/lm048lib.rl"
	{
		if(resp != NULL){
			switch(resp->type){
			case LM048_AT_VALUE_RESPONSE:
				pkt->payload_length = 0;
				{goto st437;}
			case LM048_AT_ANY_EVNT:
			case LM048_AT_CONNECT_EVNT:
			case LM048_AT_DISCONNECT_EVNT:
				{goto st444;}
			}
		}
	}
	goto st2;
st2:
	if ( ++p == pe )
		goto _test_eof2;
case 2:
#line 791 "build/lm048lib.c"
	_ps = 2;
	switch( (*p) ) {
		case 65: goto st3;
		case 66: goto st18;
		case 68: goto st30;
		case 69: goto st72;
		case 70: goto st95;
		case 73: goto st138;
		case 77: goto st150;
		case 79: goto st170;
		case 80: goto st173;
		case 82: goto st182;
		case 83: goto st201;
		case 87: goto st225;
		case 97: goto st234;
		case 98: goto st18;
		case 100: goto st30;
		case 101: goto st235;
		case 102: goto st236;
		case 105: goto st138;
		case 109: goto st150;
		case 112: goto st173;
		case 114: goto st182;
		case 115: goto st237;
	}
	goto tr0;
st3:
	if ( ++p == pe )
		goto _test_eof3;
case 3:
	_ps = 3;
	switch( (*p) ) {
		case 67: goto st4;
		case 86: goto st13;
		case 99: goto st4;
	}
	goto tr0;
st4:
	if ( ++p == pe )
		goto _test_eof4;
case 4:
	_ps = 4;
	switch( (*p) ) {
		case 79: goto st5;
		case 111: goto st5;
	}
	goto tr0;
st5:
	if ( ++p == pe )
		goto _test_eof5;
case 5:
	_ps = 5;
	switch( (*p) ) {
		case 78: goto st6;
		case 110: goto st6;
	}
	goto tr0;
st6:
	if ( ++p == pe )
		goto _test_eof6;
case 6:
	_ps = 6;
	switch( (*p) ) {
		case 43: goto tr24;
		case 45: goto tr25;
		case 63: goto tr26;
	}
	goto tr0;
tr24:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st7;
tr25:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st7;
tr26:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st7;
st7:
	if ( ++p == pe )
		goto _test_eof7;
case 7:
#line 876 "build/lm048lib.c"
	_ps = 7;
	if ( (*p) == 13 )
		goto st8;
	goto tr0;
st8:
	if ( ++p == pe )
		goto _test_eof8;
case 8:
	_ps = 8;
	if ( (*p) == 10 )
		goto st9;
	goto tr0;
st9:
	if ( ++p == pe )
		goto _test_eof9;
case 9:
	_ps = 9;
	if ( (*p) == 79 )
		goto st10;
	goto tr0;
st10:
	if ( ++p == pe )
		goto _test_eof10;
case 10:
	_ps = 10;
	if ( (*p) == 75 )
		goto st11;
	goto tr0;
st11:
	if ( ++p == pe )
		goto _test_eof11;
case 11:
	_ps = 11;
	if ( (*p) == 13 )
		goto st12;
	goto tr0;
st12:
	if ( ++p == pe )
		goto _test_eof12;
case 12:
	_ps = 12;
	if ( (*p) == 10 )
		goto tr32;
	goto tr0;
tr32:
#line 115 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_ACON_RESPONSE); }
	goto st582;
tr37:
#line 254 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_AVERAGE_RSSI); }
	goto st582;
tr59:
#line 90 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_BAUD_RESPONSE); }
	goto st582;
tr73:
#line 111 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_DCOV_RESPONSE); }
	goto st582;
tr85:
#line 124 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_DPIN_RESPONSE); }
	goto st582;
tr122:
#line 99 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_ECHO_RESPONSE); }
	goto st582;
tr127:
#line 67 "build/lm048lib.rl"
	{
		PTYPE(LM048_AT_ERROR);
		state->on_error_response();
	}
	goto st582;
tr137:
#line 117 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_ESC_RESPONSE); }
	goto st582;
tr168:
#line 86 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_VER_RESPONSE); }
	goto st582;
tr183:
#line 97 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_FLOW_RESPONSE); }
	goto st582;
tr200:
#line 126 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_IOTYPE_RESPONSE); }
	goto st582;
tr213:
#line 128 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_MITM_RESPONSE); }
	goto st582;
tr226:
#line 103 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_MODEM_RESPONSE); }
	goto st582;
tr229:
#line 62 "build/lm048lib.rl"
	{
		PTYPE(LM048_AT_OK);
		state->on_ok_response();
	}
	goto st582;
tr241:
#line 95 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_PAR_RESPONSE); }
	goto st582;
tr254:
#line 120 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_RCFG_RESPONSE); }
	goto st582;
tr265:
#line 105 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_ROLE_RESPONSE); }
	goto st582;
tr280:
#line 122 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_SLEEP_RESPONSE); }
	goto st582;
tr292:
#line 93 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_STOP_BITS_RESPONSE); }
	goto st582;
tr295:
#line 253 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_STRONG_RSSI); }
	goto st582;
tr304:
#line 108 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_RSSI_RESPONSE); }
	goto st582;
tr306:
#line 84 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_AT); }
	goto st582;
tr329:
#line 114 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_ACON); }
	goto st582;
tr333:
#line 106 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_ADDR); }
	goto st582;
tr336:
#line 118 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_AUTO); }
	goto st582;
tr354:
#line 89 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_BAUD); }
	goto st582;
tr361:
#line 113 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_BOND); }
	goto st582;
tr403:
#line 110 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_DCOV); }
	goto st582;
tr410:
#line 123 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_DPIN); }
	goto st582;
tr436:
#line 112 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_DROP); }
	goto st582;
tr444:
#line 98 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_ECHO); }
	goto st582;
tr449:
#line 116 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_ESC); }
	goto st582;
tr456:
#line 96 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_FLOW); }
	goto st582;
tr468:
#line 125 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_IOTYPE); }
	goto st582;
tr476:
#line 127 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_MITM); }
	goto st582;
tr484:
#line 102 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_MODEM); }
	goto st582;
tr491:
#line 109 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_NAME); }
	goto st582;
tr528:
#line 94 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_PAR); }
	goto st582;
tr534:
#line 88 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_PIN); }
	goto st582;
tr545:
#line 119 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_RCFG); }
	goto st582;
tr550:
#line 91 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_RESET); }
	goto st582;
tr554:
#line 100 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_RESP); }
	goto st582;
tr560:
#line 104 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_ROLE); }
	goto st582;
tr563:
#line 107 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_RSSI); }
	goto st582;
tr572:
#line 121 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_SLEEP); }
	goto st582;
tr578:
#line 92 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_STOP_BITS); }
	goto st582;
tr581:
#line 85 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_VER); }
	goto st582;
st582:
#line 76 "build/lm048lib.rl"
	{
		if(state->on_completed != NULL){
			state->on_completed();
		}else{
			{p++;  state->cs = 582; goto _out;}
		}
	}
	if ( ++p == pe )
		goto _test_eof582;
case 582:
#line 1127 "build/lm048lib.c"
	goto st0;
st13:
	if ( ++p == pe )
		goto _test_eof13;
case 13:
	_ps = 13;
	if ( (*p) == 69 )
		goto st14;
	goto tr0;
st14:
	if ( ++p == pe )
		goto _test_eof14;
case 14:
	_ps = 14;
	if ( (*p) == 82 )
		goto st15;
	goto tr0;
st15:
	if ( ++p == pe )
		goto _test_eof15;
case 15:
	_ps = 15;
	if ( (*p) == 65 )
		goto st16;
	goto tr0;
st16:
	if ( ++p == pe )
		goto _test_eof16;
case 16:
	_ps = 16;
	if ( (*p) == 71 )
		goto st17;
	goto tr0;
st17:
	if ( ++p == pe )
		goto _test_eof17;
case 17:
	_ps = 17;
	if ( (*p) == 69 )
		goto tr37;
	goto tr0;
st18:
	if ( ++p == pe )
		goto _test_eof18;
case 18:
	_ps = 18;
	switch( (*p) ) {
		case 65: goto st19;
		case 97: goto st19;
	}
	goto tr0;
st19:
	if ( ++p == pe )
		goto _test_eof19;
case 19:
	_ps = 19;
	switch( (*p) ) {
		case 85: goto st20;
		case 117: goto st20;
	}
	goto tr0;
st20:
	if ( ++p == pe )
		goto _test_eof20;
case 20:
	_ps = 20;
	switch( (*p) ) {
		case 68: goto tr40;
		case 100: goto tr40;
	}
	goto tr0;
tr40:
#line 148 "build/lm048lib.rl"
	{ pkt->payload_length = 0; }
	goto st21;
st21:
	if ( ++p == pe )
		goto _test_eof21;
case 21:
#line 1207 "build/lm048lib.c"
	_ps = 21;
	switch( (*p) ) {
		case 49: goto st22;
		case 50: goto st29;
		case 63: goto tr43;
	}
	goto tr0;
st22:
	if ( ++p == pe )
		goto _test_eof22;
case 22:
	_ps = 22;
	switch( (*p) ) {
		case 48: goto tr44;
		case 49: goto tr45;
		case 50: goto tr46;
		case 51: goto tr47;
		case 52: goto tr48;
		case 53: goto tr49;
		case 54: goto tr50;
		case 55: goto tr51;
		case 56: goto tr52;
		case 57: goto tr53;
	}
	goto tr0;
tr43:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st23;
tr44:
#line 190 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_1200); }
	goto st23;
tr45:
#line 191 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_2400); }
	goto st23;
tr46:
#line 192 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_4800); }
	goto st23;
tr47:
#line 193 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_9600); }
	goto st23;
tr48:
#line 194 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_19200); }
	goto st23;
tr49:
#line 195 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_38400); }
	goto st23;
tr50:
#line 196 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_57600); }
	goto st23;
tr51:
#line 197 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_115200); }
	goto st23;
tr52:
#line 198 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_230400); }
	goto st23;
tr53:
#line 199 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_460800); }
	goto st23;
tr60:
#line 200 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_921600); }
	goto st23;
st23:
	if ( ++p == pe )
		goto _test_eof23;
case 23:
#line 1285 "build/lm048lib.c"
	_ps = 23;
	if ( (*p) == 13 )
		goto st24;
	goto tr0;
st24:
	if ( ++p == pe )
		goto _test_eof24;
case 24:
	_ps = 24;
	if ( (*p) == 10 )
		goto st25;
	goto tr0;
st25:
	if ( ++p == pe )
		goto _test_eof25;
case 25:
	_ps = 25;
	if ( (*p) == 79 )
		goto st26;
	goto tr0;
st26:
	if ( ++p == pe )
		goto _test_eof26;
case 26:
	_ps = 26;
	if ( (*p) == 75 )
		goto st27;
	goto tr0;
st27:
	if ( ++p == pe )
		goto _test_eof27;
case 27:
	_ps = 27;
	if ( (*p) == 13 )
		goto st28;
	goto tr0;
st28:
	if ( ++p == pe )
		goto _test_eof28;
case 28:
	_ps = 28;
	if ( (*p) == 10 )
		goto tr59;
	goto tr0;
st29:
	if ( ++p == pe )
		goto _test_eof29;
case 29:
	_ps = 29;
	if ( (*p) == 48 )
		goto tr60;
	goto tr0;
st30:
	if ( ++p == pe )
		goto _test_eof30;
case 30:
	_ps = 30;
	switch( (*p) ) {
		case 67: goto st31;
		case 80: goto st40;
		case 99: goto st31;
		case 112: goto st40;
	}
	goto tr0;
st31:
	if ( ++p == pe )
		goto _test_eof31;
case 31:
	_ps = 31;
	switch( (*p) ) {
		case 79: goto st32;
		case 111: goto st32;
	}
	goto tr0;
st32:
	if ( ++p == pe )
		goto _test_eof32;
case 32:
	_ps = 32;
	switch( (*p) ) {
		case 86: goto st33;
		case 118: goto st33;
	}
	goto tr0;
st33:
	if ( ++p == pe )
		goto _test_eof33;
case 33:
	_ps = 33;
	switch( (*p) ) {
		case 43: goto tr65;
		case 45: goto tr66;
		case 63: goto tr67;
	}
	goto tr0;
tr65:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st34;
tr66:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st34;
tr67:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st34;
st34:
	if ( ++p == pe )
		goto _test_eof34;
case 34:
#line 1397 "build/lm048lib.c"
	_ps = 34;
	if ( (*p) == 13 )
		goto st35;
	goto tr0;
st35:
	if ( ++p == pe )
		goto _test_eof35;
case 35:
	_ps = 35;
	if ( (*p) == 10 )
		goto st36;
	goto tr0;
st36:
	if ( ++p == pe )
		goto _test_eof36;
case 36:
	_ps = 36;
	if ( (*p) == 79 )
		goto st37;
	goto tr0;
st37:
	if ( ++p == pe )
		goto _test_eof37;
case 37:
	_ps = 37;
	if ( (*p) == 75 )
		goto st38;
	goto tr0;
st38:
	if ( ++p == pe )
		goto _test_eof38;
case 38:
	_ps = 38;
	if ( (*p) == 13 )
		goto st39;
	goto tr0;
st39:
	if ( ++p == pe )
		goto _test_eof39;
case 39:
	_ps = 39;
	if ( (*p) == 10 )
		goto tr73;
	goto tr0;
st40:
	if ( ++p == pe )
		goto _test_eof40;
case 40:
	_ps = 40;
	switch( (*p) ) {
		case 73: goto st41;
		case 105: goto st41;
	}
	goto tr0;
st41:
	if ( ++p == pe )
		goto _test_eof41;
case 41:
	_ps = 41;
	switch( (*p) ) {
		case 78: goto st42;
		case 110: goto st42;
	}
	goto tr0;
st42:
	if ( ++p == pe )
		goto _test_eof42;
case 42:
	_ps = 42;
	switch( (*p) ) {
		case 43: goto tr76;
		case 45: goto tr77;
		case 61: goto tr78;
		case 63: goto tr79;
	}
	goto tr0;
tr76:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st43;
tr77:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st43;
tr79:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st43;
st43:
	if ( ++p == pe )
		goto _test_eof43;
case 43:
#line 1490 "build/lm048lib.c"
	_ps = 43;
	if ( (*p) == 13 )
		goto st44;
	goto tr0;
st44:
	if ( ++p == pe )
		goto _test_eof44;
case 44:
	_ps = 44;
	if ( (*p) == 10 )
		goto st45;
	goto tr0;
st45:
	if ( ++p == pe )
		goto _test_eof45;
case 45:
	_ps = 45;
	if ( (*p) == 79 )
		goto st46;
	goto tr0;
st46:
	if ( ++p == pe )
		goto _test_eof46;
case 46:
	_ps = 46;
	if ( (*p) == 75 )
		goto st47;
	goto tr0;
st47:
	if ( ++p == pe )
		goto _test_eof47;
case 47:
	_ps = 47;
	if ( (*p) == 13 )
		goto st48;
	goto tr0;
st48:
	if ( ++p == pe )
		goto _test_eof48;
case 48:
	_ps = 48;
	if ( (*p) == 10 )
		goto tr85;
	goto tr0;
tr78:
#line 137 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_SET); }
	goto st49;
st49:
	if ( ++p == pe )
		goto _test_eof49;
case 49:
#line 1543 "build/lm048lib.c"
	_ps = 49;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr86;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr86;
	} else
		goto tr86;
	goto tr0;
tr86:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st50;
st50:
	if ( ++p == pe )
		goto _test_eof50;
case 50:
#line 1562 "build/lm048lib.c"
	_ps = 50;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr87;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr87;
	} else
		goto tr87;
	goto tr0;
tr87:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st51;
st51:
	if ( ++p == pe )
		goto _test_eof51;
case 51:
#line 1581 "build/lm048lib.c"
	_ps = 51;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr88;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr88;
	} else
		goto tr88;
	goto tr0;
tr88:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st52;
st52:
	if ( ++p == pe )
		goto _test_eof52;
case 52:
#line 1600 "build/lm048lib.c"
	_ps = 52;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr89;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr89;
	} else
		goto tr89;
	goto tr0;
tr89:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st53;
st53:
	if ( ++p == pe )
		goto _test_eof53;
case 53:
#line 1619 "build/lm048lib.c"
	_ps = 53;
	if ( (*p) == 45 )
		goto tr90;
	goto tr0;
tr90:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st54;
st54:
	if ( ++p == pe )
		goto _test_eof54;
case 54:
#line 1632 "build/lm048lib.c"
	_ps = 54;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr91;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr91;
	} else
		goto tr91;
	goto tr0;
tr91:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st55;
st55:
	if ( ++p == pe )
		goto _test_eof55;
case 55:
#line 1651 "build/lm048lib.c"
	_ps = 55;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr92;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr92;
	} else
		goto tr92;
	goto tr0;
tr92:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st56;
st56:
	if ( ++p == pe )
		goto _test_eof56;
case 56:
#line 1670 "build/lm048lib.c"
	_ps = 56;
	if ( (*p) == 45 )
		goto tr93;
	goto tr0;
tr93:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st57;
st57:
	if ( ++p == pe )
		goto _test_eof57;
case 57:
#line 1683 "build/lm048lib.c"
	_ps = 57;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr94;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr94;
	} else
		goto tr94;
	goto tr0;
tr94:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st58;
st58:
	if ( ++p == pe )
		goto _test_eof58;
case 58:
#line 1702 "build/lm048lib.c"
	_ps = 58;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr95;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr95;
	} else
		goto tr95;
	goto tr0;
tr95:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st59;
st59:
	if ( ++p == pe )
		goto _test_eof59;
case 59:
#line 1721 "build/lm048lib.c"
	_ps = 59;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr96;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr96;
	} else
		goto tr96;
	goto tr0;
tr96:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st60;
st60:
	if ( ++p == pe )
		goto _test_eof60;
case 60:
#line 1740 "build/lm048lib.c"
	_ps = 60;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr97;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr97;
	} else
		goto tr97;
	goto tr0;
tr97:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st61;
st61:
	if ( ++p == pe )
		goto _test_eof61;
case 61:
#line 1759 "build/lm048lib.c"
	_ps = 61;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr98;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr98;
	} else
		goto tr98;
	goto tr0;
tr98:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st62;
st62:
	if ( ++p == pe )
		goto _test_eof62;
case 62:
#line 1778 "build/lm048lib.c"
	_ps = 62;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr99;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr99;
	} else
		goto tr99;
	goto tr0;
tr99:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st63;
st63:
	if ( ++p == pe )
		goto _test_eof63;
case 63:
#line 1797 "build/lm048lib.c"
	_ps = 63;
	if ( (*p) == 44 )
		goto st64;
	goto tr0;
st64:
	if ( ++p == pe )
		goto _test_eof64;
case 64:
	_ps = 64;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st65;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st65;
	} else
		goto st65;
	goto tr0;
st65:
	if ( ++p == pe )
		goto _test_eof65;
case 65:
	_ps = 65;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st66;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st66;
	} else
		goto st66;
	goto tr0;
st66:
	if ( ++p == pe )
		goto _test_eof66;
case 66:
	_ps = 66;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st67;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st67;
	} else
		goto st67;
	goto tr0;
st67:
	if ( ++p == pe )
		goto _test_eof67;
case 67:
	_ps = 67;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st68;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st68;
	} else
		goto st68;
	goto tr0;
st68:
	if ( ++p == pe )
		goto _test_eof68;
case 68:
	_ps = 68;
	if ( (*p) == 13 )
		goto st44;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st69;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st69;
	} else
		goto st69;
	goto tr0;
st69:
	if ( ++p == pe )
		goto _test_eof69;
case 69:
	_ps = 69;
	if ( (*p) == 13 )
		goto st44;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st70;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st70;
	} else
		goto st70;
	goto tr0;
st70:
	if ( ++p == pe )
		goto _test_eof70;
case 70:
	_ps = 70;
	if ( (*p) == 13 )
		goto st44;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st71;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st71;
	} else
		goto st71;
	goto tr0;
st71:
	if ( ++p == pe )
		goto _test_eof71;
case 71:
	_ps = 71;
	if ( (*p) == 13 )
		goto st44;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st43;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st43;
	} else
		goto st43;
	goto tr0;
st72:
	if ( ++p == pe )
		goto _test_eof72;
case 72:
	_ps = 72;
	switch( (*p) ) {
		case 67: goto st73;
		case 82: goto st82;
		case 83: goto st87;
		case 99: goto st73;
		case 115: goto st87;
	}
	goto tr0;
st73:
	if ( ++p == pe )
		goto _test_eof73;
case 73:
	_ps = 73;
	switch( (*p) ) {
		case 72: goto st74;
		case 104: goto st74;
	}
	goto tr0;
st74:
	if ( ++p == pe )
		goto _test_eof74;
case 74:
	_ps = 74;
	switch( (*p) ) {
		case 79: goto st75;
		case 111: goto st75;
	}
	goto tr0;
st75:
	if ( ++p == pe )
		goto _test_eof75;
case 75:
	_ps = 75;
	switch( (*p) ) {
		case 43: goto tr114;
		case 45: goto tr115;
		case 63: goto tr116;
	}
	goto tr0;
tr114:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st76;
tr115:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st76;
tr116:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st76;
st76:
	if ( ++p == pe )
		goto _test_eof76;
case 76:
#line 1982 "build/lm048lib.c"
	_ps = 76;
	if ( (*p) == 13 )
		goto st77;
	goto tr0;
st77:
	if ( ++p == pe )
		goto _test_eof77;
case 77:
	_ps = 77;
	if ( (*p) == 10 )
		goto st78;
	goto tr0;
st78:
	if ( ++p == pe )
		goto _test_eof78;
case 78:
	_ps = 78;
	if ( (*p) == 79 )
		goto st79;
	goto tr0;
st79:
	if ( ++p == pe )
		goto _test_eof79;
case 79:
	_ps = 79;
	if ( (*p) == 75 )
		goto st80;
	goto tr0;
st80:
	if ( ++p == pe )
		goto _test_eof80;
case 80:
	_ps = 80;
	if ( (*p) == 13 )
		goto st81;
	goto tr0;
st81:
	if ( ++p == pe )
		goto _test_eof81;
case 81:
	_ps = 81;
	if ( (*p) == 10 )
		goto tr122;
	goto tr0;
st82:
	if ( ++p == pe )
		goto _test_eof82;
case 82:
	_ps = 82;
	if ( (*p) == 82 )
		goto st83;
	goto tr0;
st83:
	if ( ++p == pe )
		goto _test_eof83;
case 83:
	_ps = 83;
	if ( (*p) == 79 )
		goto st84;
	goto tr0;
st84:
	if ( ++p == pe )
		goto _test_eof84;
case 84:
	_ps = 84;
	if ( (*p) == 82 )
		goto st85;
	goto tr0;
st85:
	if ( ++p == pe )
		goto _test_eof85;
case 85:
	_ps = 85;
	if ( (*p) == 13 )
		goto st86;
	goto tr0;
st86:
	if ( ++p == pe )
		goto _test_eof86;
case 86:
	_ps = 86;
	if ( (*p) == 10 )
		goto tr127;
	goto tr0;
st87:
	if ( ++p == pe )
		goto _test_eof87;
case 87:
	_ps = 87;
	switch( (*p) ) {
		case 67: goto st88;
		case 99: goto st88;
	}
	goto tr0;
st88:
	if ( ++p == pe )
		goto _test_eof88;
case 88:
	_ps = 88;
	switch( (*p) ) {
		case 43: goto tr129;
		case 45: goto tr130;
		case 63: goto tr131;
	}
	goto tr0;
tr129:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st89;
tr130:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st89;
tr131:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st89;
st89:
	if ( ++p == pe )
		goto _test_eof89;
case 89:
#line 2104 "build/lm048lib.c"
	_ps = 89;
	if ( (*p) == 13 )
		goto st90;
	goto tr0;
st90:
	if ( ++p == pe )
		goto _test_eof90;
case 90:
	_ps = 90;
	if ( (*p) == 10 )
		goto st91;
	goto tr0;
st91:
	if ( ++p == pe )
		goto _test_eof91;
case 91:
	_ps = 91;
	if ( (*p) == 79 )
		goto st92;
	goto tr0;
st92:
	if ( ++p == pe )
		goto _test_eof92;
case 92:
	_ps = 92;
	if ( (*p) == 75 )
		goto st93;
	goto tr0;
st93:
	if ( ++p == pe )
		goto _test_eof93;
case 93:
	_ps = 93;
	if ( (*p) == 13 )
		goto st94;
	goto tr0;
st94:
	if ( ++p == pe )
		goto _test_eof94;
case 94:
	_ps = 94;
	if ( (*p) == 10 )
		goto tr137;
	goto tr0;
st95:
	if ( ++p == pe )
		goto _test_eof95;
case 95:
	_ps = 95;
	switch( (*p) ) {
		case 47: goto st96;
		case 76: goto st129;
		case 108: goto st129;
	}
	goto tr0;
st96:
	if ( ++p == pe )
		goto _test_eof96;
case 96:
	_ps = 96;
	if ( (*p) == 87 )
		goto st97;
	goto tr0;
st97:
	if ( ++p == pe )
		goto _test_eof97;
case 97:
	_ps = 97;
	if ( (*p) == 32 )
		goto st98;
	goto tr0;
st98:
	if ( ++p == pe )
		goto _test_eof98;
case 98:
	_ps = 98;
	if ( (*p) == 86 )
		goto st99;
	goto tr0;
st99:
	if ( ++p == pe )
		goto _test_eof99;
case 99:
	_ps = 99;
	if ( (*p) == 69 )
		goto st100;
	goto tr0;
st100:
	if ( ++p == pe )
		goto _test_eof100;
case 100:
	_ps = 100;
	if ( (*p) == 82 )
		goto st101;
	goto tr0;
st101:
	if ( ++p == pe )
		goto _test_eof101;
case 101:
	_ps = 101;
	if ( (*p) == 83 )
		goto st102;
	goto tr0;
st102:
	if ( ++p == pe )
		goto _test_eof102;
case 102:
	_ps = 102;
	if ( (*p) == 73 )
		goto st103;
	goto tr0;
st103:
	if ( ++p == pe )
		goto _test_eof103;
case 103:
	_ps = 103;
	if ( (*p) == 79 )
		goto st104;
	goto tr0;
st104:
	if ( ++p == pe )
		goto _test_eof104;
case 104:
	_ps = 104;
	if ( (*p) == 78 )
		goto st105;
	goto tr0;
st105:
	if ( ++p == pe )
		goto _test_eof105;
case 105:
	_ps = 105;
	if ( (*p) == 58 )
		goto st106;
	goto tr0;
st106:
	if ( ++p == pe )
		goto _test_eof106;
case 106:
	_ps = 106;
	if ( (*p) == 32 )
		goto st107;
	goto tr0;
st107:
	if ( ++p == pe )
		goto _test_eof107;
case 107:
	_ps = 107;
	switch( (*p) ) {
		case 32: goto st108;
		case 118: goto tr152;
	}
	goto tr0;
st108:
	if ( ++p == pe )
		goto _test_eof108;
case 108:
	_ps = 108;
	switch( (*p) ) {
		case 32: goto st109;
		case 118: goto tr152;
	}
	goto tr0;
st109:
	if ( ++p == pe )
		goto _test_eof109;
case 109:
	_ps = 109;
	switch( (*p) ) {
		case 32: goto st110;
		case 118: goto tr152;
	}
	goto tr0;
st110:
	if ( ++p == pe )
		goto _test_eof110;
case 110:
	_ps = 110;
	switch( (*p) ) {
		case 32: goto st111;
		case 118: goto tr152;
	}
	goto tr0;
st111:
	if ( ++p == pe )
		goto _test_eof111;
case 111:
	_ps = 111;
	if ( (*p) == 118 )
		goto tr152;
	goto tr0;
tr152:
#line 148 "build/lm048lib.rl"
	{ pkt->payload_length = 0; }
	goto st112;
st112:
	if ( ++p == pe )
		goto _test_eof112;
case 112:
#line 2304 "build/lm048lib.c"
	_ps = 112;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr156;
	goto tr0;
tr156:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st113;
st113:
	if ( ++p == pe )
		goto _test_eof113;
case 113:
#line 2317 "build/lm048lib.c"
	_ps = 113;
	if ( (*p) == 46 )
		goto tr157;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr158;
	goto tr0;
tr157:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st114;
st114:
	if ( ++p == pe )
		goto _test_eof114;
case 114:
#line 2332 "build/lm048lib.c"
	_ps = 114;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr159;
	goto tr0;
tr159:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st115;
st115:
	if ( ++p == pe )
		goto _test_eof115;
case 115:
#line 2345 "build/lm048lib.c"
	_ps = 115;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr160;
	goto tr0;
tr160:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st116;
st116:
	if ( ++p == pe )
		goto _test_eof116;
case 116:
#line 2358 "build/lm048lib.c"
	_ps = 116;
	switch( (*p) ) {
		case 13: goto st117;
		case 32: goto st122;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr163;
	goto tr0;
st117:
	if ( ++p == pe )
		goto _test_eof117;
case 117:
	_ps = 117;
	if ( (*p) == 10 )
		goto st118;
	goto tr0;
st118:
	if ( ++p == pe )
		goto _test_eof118;
case 118:
	_ps = 118;
	if ( (*p) == 79 )
		goto st119;
	goto tr0;
st119:
	if ( ++p == pe )
		goto _test_eof119;
case 119:
	_ps = 119;
	if ( (*p) == 75 )
		goto st120;
	goto tr0;
st120:
	if ( ++p == pe )
		goto _test_eof120;
case 120:
	_ps = 120;
	if ( (*p) == 13 )
		goto st121;
	goto tr0;
st121:
	if ( ++p == pe )
		goto _test_eof121;
case 121:
	_ps = 121;
	if ( (*p) == 10 )
		goto tr168;
	goto tr0;
st122:
	if ( ++p == pe )
		goto _test_eof122;
case 122:
	_ps = 122;
	switch( (*p) ) {
		case 13: goto st117;
		case 32: goto st123;
	}
	goto tr0;
st123:
	if ( ++p == pe )
		goto _test_eof123;
case 123:
	_ps = 123;
	switch( (*p) ) {
		case 13: goto st117;
		case 32: goto st124;
	}
	goto tr0;
st124:
	if ( ++p == pe )
		goto _test_eof124;
case 124:
	_ps = 124;
	switch( (*p) ) {
		case 13: goto st117;
		case 32: goto st125;
	}
	goto tr0;
st125:
	if ( ++p == pe )
		goto _test_eof125;
case 125:
	_ps = 125;
	switch( (*p) ) {
		case 13: goto st117;
		case 32: goto st126;
	}
	goto tr0;
st126:
	if ( ++p == pe )
		goto _test_eof126;
case 126:
	_ps = 126;
	if ( (*p) == 13 )
		goto st117;
	goto tr0;
tr163:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st127;
st127:
	if ( ++p == pe )
		goto _test_eof127;
case 127:
#line 2463 "build/lm048lib.c"
	_ps = 127;
	switch( (*p) ) {
		case 13: goto st117;
		case 32: goto st122;
	}
	goto tr0;
tr158:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st128;
st128:
	if ( ++p == pe )
		goto _test_eof128;
case 128:
#line 2478 "build/lm048lib.c"
	_ps = 128;
	if ( (*p) == 46 )
		goto tr157;
	goto tr0;
st129:
	if ( ++p == pe )
		goto _test_eof129;
case 129:
	_ps = 129;
	switch( (*p) ) {
		case 79: goto st130;
		case 111: goto st130;
	}
	goto tr0;
st130:
	if ( ++p == pe )
		goto _test_eof130;
case 130:
	_ps = 130;
	switch( (*p) ) {
		case 87: goto st131;
		case 119: goto st131;
	}
	goto tr0;
st131:
	if ( ++p == pe )
		goto _test_eof131;
case 131:
	_ps = 131;
	switch( (*p) ) {
		case 43: goto tr175;
		case 45: goto tr176;
		case 63: goto tr177;
	}
	goto tr0;
tr175:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st132;
tr176:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st132;
tr177:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st132;
st132:
	if ( ++p == pe )
		goto _test_eof132;
case 132:
#line 2530 "build/lm048lib.c"
	_ps = 132;
	if ( (*p) == 13 )
		goto st133;
	goto tr0;
st133:
	if ( ++p == pe )
		goto _test_eof133;
case 133:
	_ps = 133;
	if ( (*p) == 10 )
		goto st134;
	goto tr0;
st134:
	if ( ++p == pe )
		goto _test_eof134;
case 134:
	_ps = 134;
	if ( (*p) == 79 )
		goto st135;
	goto tr0;
st135:
	if ( ++p == pe )
		goto _test_eof135;
case 135:
	_ps = 135;
	if ( (*p) == 75 )
		goto st136;
	goto tr0;
st136:
	if ( ++p == pe )
		goto _test_eof136;
case 136:
	_ps = 136;
	if ( (*p) == 13 )
		goto st137;
	goto tr0;
st137:
	if ( ++p == pe )
		goto _test_eof137;
case 137:
	_ps = 137;
	if ( (*p) == 10 )
		goto tr183;
	goto tr0;
st138:
	if ( ++p == pe )
		goto _test_eof138;
case 138:
	_ps = 138;
	switch( (*p) ) {
		case 79: goto st139;
		case 111: goto st139;
	}
	goto tr0;
st139:
	if ( ++p == pe )
		goto _test_eof139;
case 139:
	_ps = 139;
	switch( (*p) ) {
		case 84: goto st140;
		case 116: goto st140;
	}
	goto tr0;
st140:
	if ( ++p == pe )
		goto _test_eof140;
case 140:
	_ps = 140;
	switch( (*p) ) {
		case 89: goto st141;
		case 121: goto st141;
	}
	goto tr0;
st141:
	if ( ++p == pe )
		goto _test_eof141;
case 141:
	_ps = 141;
	switch( (*p) ) {
		case 80: goto st142;
		case 112: goto st142;
	}
	goto tr0;
st142:
	if ( ++p == pe )
		goto _test_eof142;
case 142:
	_ps = 142;
	switch( (*p) ) {
		case 69: goto st143;
		case 101: goto st143;
	}
	goto tr0;
st143:
	if ( ++p == pe )
		goto _test_eof143;
case 143:
	_ps = 143;
	switch( (*p) ) {
		case 48: goto tr189;
		case 49: goto tr190;
		case 50: goto tr191;
		case 51: goto tr192;
		case 52: goto tr193;
		case 63: goto tr194;
	}
	goto tr0;
tr194:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st144;
tr189:
#line 303 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISPLAY_ONLY); }
	goto st144;
tr190:
#line 304 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISPLAY_YES_NO); }
	goto st144;
tr191:
#line 305 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_KEYBOARD_ONLY); }
	goto st144;
tr192:
#line 306 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_NO_DISPLAY_KEYBOARD); }
	goto st144;
tr193:
#line 307 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_REJECT_BT21); }
	goto st144;
st144:
	if ( ++p == pe )
		goto _test_eof144;
case 144:
#line 2667 "build/lm048lib.c"
	_ps = 144;
	if ( (*p) == 13 )
		goto st145;
	goto tr0;
st145:
	if ( ++p == pe )
		goto _test_eof145;
case 145:
	_ps = 145;
	if ( (*p) == 10 )
		goto st146;
	goto tr0;
st146:
	if ( ++p == pe )
		goto _test_eof146;
case 146:
	_ps = 146;
	if ( (*p) == 79 )
		goto st147;
	goto tr0;
st147:
	if ( ++p == pe )
		goto _test_eof147;
case 147:
	_ps = 147;
	if ( (*p) == 75 )
		goto st148;
	goto tr0;
st148:
	if ( ++p == pe )
		goto _test_eof148;
case 148:
	_ps = 148;
	if ( (*p) == 13 )
		goto st149;
	goto tr0;
st149:
	if ( ++p == pe )
		goto _test_eof149;
case 149:
	_ps = 149;
	if ( (*p) == 10 )
		goto tr200;
	goto tr0;
st150:
	if ( ++p == pe )
		goto _test_eof150;
case 150:
	_ps = 150;
	switch( (*p) ) {
		case 73: goto st151;
		case 79: goto st160;
		case 105: goto st151;
		case 111: goto st160;
	}
	goto tr0;
st151:
	if ( ++p == pe )
		goto _test_eof151;
case 151:
	_ps = 151;
	switch( (*p) ) {
		case 84: goto st152;
		case 116: goto st152;
	}
	goto tr0;
st152:
	if ( ++p == pe )
		goto _test_eof152;
case 152:
	_ps = 152;
	switch( (*p) ) {
		case 77: goto st153;
		case 109: goto st153;
	}
	goto tr0;
st153:
	if ( ++p == pe )
		goto _test_eof153;
case 153:
	_ps = 153;
	switch( (*p) ) {
		case 43: goto tr205;
		case 45: goto tr206;
		case 63: goto tr207;
	}
	goto tr0;
tr205:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st154;
tr206:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st154;
tr207:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st154;
st154:
	if ( ++p == pe )
		goto _test_eof154;
case 154:
#line 2771 "build/lm048lib.c"
	_ps = 154;
	if ( (*p) == 13 )
		goto st155;
	goto tr0;
st155:
	if ( ++p == pe )
		goto _test_eof155;
case 155:
	_ps = 155;
	if ( (*p) == 10 )
		goto st156;
	goto tr0;
st156:
	if ( ++p == pe )
		goto _test_eof156;
case 156:
	_ps = 156;
	if ( (*p) == 79 )
		goto st157;
	goto tr0;
st157:
	if ( ++p == pe )
		goto _test_eof157;
case 157:
	_ps = 157;
	if ( (*p) == 75 )
		goto st158;
	goto tr0;
st158:
	if ( ++p == pe )
		goto _test_eof158;
case 158:
	_ps = 158;
	if ( (*p) == 13 )
		goto st159;
	goto tr0;
st159:
	if ( ++p == pe )
		goto _test_eof159;
case 159:
	_ps = 159;
	if ( (*p) == 10 )
		goto tr213;
	goto tr0;
st160:
	if ( ++p == pe )
		goto _test_eof160;
case 160:
	_ps = 160;
	switch( (*p) ) {
		case 68: goto st161;
		case 100: goto st161;
	}
	goto tr0;
st161:
	if ( ++p == pe )
		goto _test_eof161;
case 161:
	_ps = 161;
	switch( (*p) ) {
		case 69: goto st162;
		case 101: goto st162;
	}
	goto tr0;
st162:
	if ( ++p == pe )
		goto _test_eof162;
case 162:
	_ps = 162;
	switch( (*p) ) {
		case 77: goto tr216;
		case 109: goto tr216;
	}
	goto tr0;
tr216:
#line 148 "build/lm048lib.rl"
	{ pkt->payload_length = 0; }
	goto st163;
st163:
	if ( ++p == pe )
		goto _test_eof163;
case 163:
#line 2854 "build/lm048lib.c"
	_ps = 163;
	switch( (*p) ) {
		case 45: goto tr217;
		case 63: goto tr218;
		case 76: goto tr219;
		case 82: goto tr220;
		case 108: goto tr219;
		case 114: goto tr220;
	}
	goto tr0;
tr217:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st164;
tr218:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st164;
tr219:
#line 236 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_LOCAL_LOOPBACK); }
	goto st164;
tr220:
#line 237 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_REMOTE_TRANSFER); }
	goto st164;
st164:
	if ( ++p == pe )
		goto _test_eof164;
case 164:
#line 2885 "build/lm048lib.c"
	_ps = 164;
	if ( (*p) == 13 )
		goto st165;
	goto tr0;
st165:
	if ( ++p == pe )
		goto _test_eof165;
case 165:
	_ps = 165;
	if ( (*p) == 10 )
		goto st166;
	goto tr0;
st166:
	if ( ++p == pe )
		goto _test_eof166;
case 166:
	_ps = 166;
	if ( (*p) == 79 )
		goto st167;
	goto tr0;
st167:
	if ( ++p == pe )
		goto _test_eof167;
case 167:
	_ps = 167;
	if ( (*p) == 75 )
		goto st168;
	goto tr0;
st168:
	if ( ++p == pe )
		goto _test_eof168;
case 168:
	_ps = 168;
	if ( (*p) == 13 )
		goto st169;
	goto tr0;
st169:
	if ( ++p == pe )
		goto _test_eof169;
case 169:
	_ps = 169;
	if ( (*p) == 10 )
		goto tr226;
	goto tr0;
st170:
	if ( ++p == pe )
		goto _test_eof170;
case 170:
	_ps = 170;
	if ( (*p) == 75 )
		goto st171;
	goto tr0;
st171:
	if ( ++p == pe )
		goto _test_eof171;
case 171:
	_ps = 171;
	if ( (*p) == 13 )
		goto st172;
	goto tr0;
st172:
	if ( ++p == pe )
		goto _test_eof172;
case 172:
	_ps = 172;
	if ( (*p) == 10 )
		goto tr229;
	goto tr0;
st173:
	if ( ++p == pe )
		goto _test_eof173;
case 173:
	_ps = 173;
	switch( (*p) ) {
		case 65: goto st174;
		case 97: goto st174;
	}
	goto tr0;
st174:
	if ( ++p == pe )
		goto _test_eof174;
case 174:
	_ps = 174;
	switch( (*p) ) {
		case 82: goto st175;
		case 114: goto st175;
	}
	goto tr0;
st175:
	if ( ++p == pe )
		goto _test_eof175;
case 175:
	_ps = 175;
	switch( (*p) ) {
		case 48: goto tr232;
		case 49: goto tr233;
		case 50: goto tr234;
		case 63: goto tr235;
	}
	goto tr0;
tr235:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st176;
tr232:
#line 216 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_NO_PARITY); }
	goto st176;
tr233:
#line 217 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ODD_PARITY); }
	goto st176;
tr234:
#line 218 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_EVEN_PARITY); }
	goto st176;
st176:
	if ( ++p == pe )
		goto _test_eof176;
case 176:
#line 3006 "build/lm048lib.c"
	_ps = 176;
	if ( (*p) == 13 )
		goto st177;
	goto tr0;
st177:
	if ( ++p == pe )
		goto _test_eof177;
case 177:
	_ps = 177;
	if ( (*p) == 10 )
		goto st178;
	goto tr0;
st178:
	if ( ++p == pe )
		goto _test_eof178;
case 178:
	_ps = 178;
	if ( (*p) == 79 )
		goto st179;
	goto tr0;
st179:
	if ( ++p == pe )
		goto _test_eof179;
case 179:
	_ps = 179;
	if ( (*p) == 75 )
		goto st180;
	goto tr0;
st180:
	if ( ++p == pe )
		goto _test_eof180;
case 180:
	_ps = 180;
	if ( (*p) == 13 )
		goto st181;
	goto tr0;
st181:
	if ( ++p == pe )
		goto _test_eof181;
case 181:
	_ps = 181;
	if ( (*p) == 10 )
		goto tr241;
	goto tr0;
st182:
	if ( ++p == pe )
		goto _test_eof182;
case 182:
	_ps = 182;
	switch( (*p) ) {
		case 67: goto st183;
		case 79: goto st192;
		case 99: goto st183;
		case 111: goto st192;
	}
	goto tr0;
st183:
	if ( ++p == pe )
		goto _test_eof183;
case 183:
	_ps = 183;
	switch( (*p) ) {
		case 70: goto st184;
		case 102: goto st184;
	}
	goto tr0;
st184:
	if ( ++p == pe )
		goto _test_eof184;
case 184:
	_ps = 184;
	switch( (*p) ) {
		case 71: goto st185;
		case 103: goto st185;
	}
	goto tr0;
st185:
	if ( ++p == pe )
		goto _test_eof185;
case 185:
	_ps = 185;
	switch( (*p) ) {
		case 43: goto tr246;
		case 45: goto tr247;
		case 63: goto tr248;
	}
	goto tr0;
tr246:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st186;
tr247:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st186;
tr248:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st186;
st186:
	if ( ++p == pe )
		goto _test_eof186;
case 186:
#line 3110 "build/lm048lib.c"
	_ps = 186;
	if ( (*p) == 13 )
		goto st187;
	goto tr0;
st187:
	if ( ++p == pe )
		goto _test_eof187;
case 187:
	_ps = 187;
	if ( (*p) == 10 )
		goto st188;
	goto tr0;
st188:
	if ( ++p == pe )
		goto _test_eof188;
case 188:
	_ps = 188;
	if ( (*p) == 79 )
		goto st189;
	goto tr0;
st189:
	if ( ++p == pe )
		goto _test_eof189;
case 189:
	_ps = 189;
	if ( (*p) == 75 )
		goto st190;
	goto tr0;
st190:
	if ( ++p == pe )
		goto _test_eof190;
case 190:
	_ps = 190;
	if ( (*p) == 13 )
		goto st191;
	goto tr0;
st191:
	if ( ++p == pe )
		goto _test_eof191;
case 191:
	_ps = 191;
	if ( (*p) == 10 )
		goto tr254;
	goto tr0;
st192:
	if ( ++p == pe )
		goto _test_eof192;
case 192:
	_ps = 192;
	switch( (*p) ) {
		case 76: goto st193;
		case 108: goto st193;
	}
	goto tr0;
st193:
	if ( ++p == pe )
		goto _test_eof193;
case 193:
	_ps = 193;
	switch( (*p) ) {
		case 69: goto tr256;
		case 101: goto tr256;
	}
	goto tr0;
tr256:
#line 148 "build/lm048lib.rl"
	{ pkt->payload_length = 0; }
	goto st194;
st194:
	if ( ++p == pe )
		goto _test_eof194;
case 194:
#line 3183 "build/lm048lib.c"
	_ps = 194;
	switch( (*p) ) {
		case 63: goto tr257;
		case 77: goto tr258;
		case 83: goto tr259;
		case 109: goto tr258;
		case 115: goto tr259;
	}
	goto tr0;
tr257:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st195;
tr258:
#line 243 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_MASTER); }
	goto st195;
tr259:
#line 244 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_SLAVE); }
	goto st195;
st195:
	if ( ++p == pe )
		goto _test_eof195;
case 195:
#line 3209 "build/lm048lib.c"
	_ps = 195;
	if ( (*p) == 13 )
		goto st196;
	goto tr0;
st196:
	if ( ++p == pe )
		goto _test_eof196;
case 196:
	_ps = 196;
	if ( (*p) == 10 )
		goto st197;
	goto tr0;
st197:
	if ( ++p == pe )
		goto _test_eof197;
case 197:
	_ps = 197;
	if ( (*p) == 79 )
		goto st198;
	goto tr0;
st198:
	if ( ++p == pe )
		goto _test_eof198;
case 198:
	_ps = 198;
	if ( (*p) == 75 )
		goto st199;
	goto tr0;
st199:
	if ( ++p == pe )
		goto _test_eof199;
case 199:
	_ps = 199;
	if ( (*p) == 13 )
		goto st200;
	goto tr0;
st200:
	if ( ++p == pe )
		goto _test_eof200;
case 200:
	_ps = 200;
	if ( (*p) == 10 )
		goto tr265;
	goto tr0;
st201:
	if ( ++p == pe )
		goto _test_eof201;
case 201:
	_ps = 201;
	switch( (*p) ) {
		case 76: goto st202;
		case 84: goto st212;
		case 108: goto st202;
		case 116: goto st224;
	}
	goto tr0;
st202:
	if ( ++p == pe )
		goto _test_eof202;
case 202:
	_ps = 202;
	switch( (*p) ) {
		case 69: goto st203;
		case 101: goto st203;
	}
	goto tr0;
st203:
	if ( ++p == pe )
		goto _test_eof203;
case 203:
	_ps = 203;
	switch( (*p) ) {
		case 69: goto st204;
		case 101: goto st204;
	}
	goto tr0;
st204:
	if ( ++p == pe )
		goto _test_eof204;
case 204:
	_ps = 204;
	switch( (*p) ) {
		case 80: goto st205;
		case 112: goto st205;
	}
	goto tr0;
st205:
	if ( ++p == pe )
		goto _test_eof205;
case 205:
	_ps = 205;
	switch( (*p) ) {
		case 43: goto tr272;
		case 45: goto tr273;
		case 63: goto tr274;
	}
	goto tr0;
tr272:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st206;
tr273:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st206;
tr274:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st206;
st206:
	if ( ++p == pe )
		goto _test_eof206;
case 206:
#line 3323 "build/lm048lib.c"
	_ps = 206;
	if ( (*p) == 13 )
		goto st207;
	goto tr0;
st207:
	if ( ++p == pe )
		goto _test_eof207;
case 207:
	_ps = 207;
	if ( (*p) == 10 )
		goto st208;
	goto tr0;
st208:
	if ( ++p == pe )
		goto _test_eof208;
case 208:
	_ps = 208;
	if ( (*p) == 79 )
		goto st209;
	goto tr0;
st209:
	if ( ++p == pe )
		goto _test_eof209;
case 209:
	_ps = 209;
	if ( (*p) == 75 )
		goto st210;
	goto tr0;
st210:
	if ( ++p == pe )
		goto _test_eof210;
case 210:
	_ps = 210;
	if ( (*p) == 13 )
		goto st211;
	goto tr0;
st211:
	if ( ++p == pe )
		goto _test_eof211;
case 211:
	_ps = 211;
	if ( (*p) == 10 )
		goto tr280;
	goto tr0;
st212:
	if ( ++p == pe )
		goto _test_eof212;
case 212:
	_ps = 212;
	switch( (*p) ) {
		case 79: goto st213;
		case 82: goto st221;
		case 111: goto st213;
	}
	goto tr0;
st213:
	if ( ++p == pe )
		goto _test_eof213;
case 213:
	_ps = 213;
	switch( (*p) ) {
		case 80: goto st214;
		case 112: goto st214;
	}
	goto tr0;
st214:
	if ( ++p == pe )
		goto _test_eof214;
case 214:
	_ps = 214;
	switch( (*p) ) {
		case 49: goto tr284;
		case 50: goto tr285;
		case 63: goto tr286;
	}
	goto tr0;
tr286:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st215;
tr284:
#line 209 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ONE_STOPBIT); }
	goto st215;
tr285:
#line 210 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_TWO_STOPBIT); }
	goto st215;
st215:
	if ( ++p == pe )
		goto _test_eof215;
case 215:
#line 3416 "build/lm048lib.c"
	_ps = 215;
	if ( (*p) == 13 )
		goto st216;
	goto tr0;
st216:
	if ( ++p == pe )
		goto _test_eof216;
case 216:
	_ps = 216;
	if ( (*p) == 10 )
		goto st217;
	goto tr0;
st217:
	if ( ++p == pe )
		goto _test_eof217;
case 217:
	_ps = 217;
	if ( (*p) == 79 )
		goto st218;
	goto tr0;
st218:
	if ( ++p == pe )
		goto _test_eof218;
case 218:
	_ps = 218;
	if ( (*p) == 75 )
		goto st219;
	goto tr0;
st219:
	if ( ++p == pe )
		goto _test_eof219;
case 219:
	_ps = 219;
	if ( (*p) == 13 )
		goto st220;
	goto tr0;
st220:
	if ( ++p == pe )
		goto _test_eof220;
case 220:
	_ps = 220;
	if ( (*p) == 10 )
		goto tr292;
	goto tr0;
st221:
	if ( ++p == pe )
		goto _test_eof221;
case 221:
	_ps = 221;
	if ( (*p) == 79 )
		goto st222;
	goto tr0;
st222:
	if ( ++p == pe )
		goto _test_eof222;
case 222:
	_ps = 222;
	if ( (*p) == 78 )
		goto st223;
	goto tr0;
st223:
	if ( ++p == pe )
		goto _test_eof223;
case 223:
	_ps = 223;
	if ( (*p) == 71 )
		goto tr295;
	goto tr0;
st224:
	if ( ++p == pe )
		goto _test_eof224;
case 224:
	_ps = 224;
	switch( (*p) ) {
		case 79: goto st213;
		case 111: goto st213;
	}
	goto tr0;
st225:
	if ( ++p == pe )
		goto _test_eof225;
case 225:
	_ps = 225;
	if ( (*p) == 69 )
		goto st226;
	goto tr0;
st226:
	if ( ++p == pe )
		goto _test_eof226;
case 226:
	_ps = 226;
	if ( (*p) == 65 )
		goto st227;
	goto tr0;
st227:
	if ( ++p == pe )
		goto _test_eof227;
case 227:
	_ps = 227;
	if ( (*p) == 75 )
		goto tr298;
	goto tr0;
tr298:
#line 255 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_WEAK_RSSI); }
	goto st228;
st228:
	if ( ++p == pe )
		goto _test_eof228;
case 228:
#line 3527 "build/lm048lib.c"
	_ps = 228;
	if ( (*p) == 13 )
		goto st229;
	goto tr0;
st229:
	if ( ++p == pe )
		goto _test_eof229;
case 229:
	_ps = 229;
	if ( (*p) == 10 )
		goto st230;
	goto tr0;
st230:
	if ( ++p == pe )
		goto _test_eof230;
case 230:
	_ps = 230;
	if ( (*p) == 79 )
		goto st231;
	goto tr0;
st231:
	if ( ++p == pe )
		goto _test_eof231;
case 231:
	_ps = 231;
	if ( (*p) == 75 )
		goto st232;
	goto tr0;
st232:
	if ( ++p == pe )
		goto _test_eof232;
case 232:
	_ps = 232;
	if ( (*p) == 13 )
		goto st233;
	goto tr0;
st233:
	if ( ++p == pe )
		goto _test_eof233;
case 233:
	_ps = 233;
	if ( (*p) == 10 )
		goto tr304;
	goto tr0;
st234:
	if ( ++p == pe )
		goto _test_eof234;
case 234:
	_ps = 234;
	switch( (*p) ) {
		case 67: goto st4;
		case 99: goto st4;
	}
	goto tr0;
st235:
	if ( ++p == pe )
		goto _test_eof235;
case 235:
	_ps = 235;
	switch( (*p) ) {
		case 67: goto st73;
		case 83: goto st87;
		case 99: goto st73;
		case 115: goto st87;
	}
	goto tr0;
st236:
	if ( ++p == pe )
		goto _test_eof236;
case 236:
	_ps = 236;
	switch( (*p) ) {
		case 76: goto st129;
		case 108: goto st129;
	}
	goto tr0;
st237:
	if ( ++p == pe )
		goto _test_eof237;
case 237:
	_ps = 237;
	switch( (*p) ) {
		case 76: goto st202;
		case 84: goto st224;
		case 108: goto st202;
		case 116: goto st224;
	}
	goto tr0;
st238:
	if ( ++p == pe )
		goto _test_eof238;
case 238:
	_ps = 238;
	switch( (*p) ) {
		case 84: goto st239;
		case 116: goto st239;
	}
	goto tr0;
st239:
	if ( ++p == pe )
		goto _test_eof239;
case 239:
	_ps = 239;
	switch( (*p) ) {
		case 13: goto tr306;
		case 43: goto st240;
	}
	goto tr0;
st240:
	if ( ++p == pe )
		goto _test_eof240;
case 240:
	_ps = 240;
	switch( (*p) ) {
		case 65: goto st241;
		case 66: goto st253;
		case 67: goto st278;
		case 68: goto st297;
		case 69: goto st332;
		case 70: goto st340;
		case 73: goto st345;
		case 77: goto st352;
		case 78: goto st362;
		case 80: goto st397;
		case 82: goto st406;
		case 83: goto st424;
		case 86: goto st434;
		case 97: goto st241;
		case 98: goto st253;
		case 99: goto st278;
		case 100: goto st297;
		case 101: goto st332;
		case 102: goto st340;
		case 105: goto st345;
		case 109: goto st352;
		case 110: goto st362;
		case 112: goto st397;
		case 114: goto st406;
		case 115: goto st424;
		case 118: goto st434;
	}
	goto tr0;
st241:
	if ( ++p == pe )
		goto _test_eof241;
case 241:
	_ps = 241;
	switch( (*p) ) {
		case 67: goto st242;
		case 68: goto st246;
		case 85: goto st250;
		case 99: goto st242;
		case 100: goto st246;
		case 117: goto st250;
	}
	goto tr0;
st242:
	if ( ++p == pe )
		goto _test_eof242;
case 242:
	_ps = 242;
	switch( (*p) ) {
		case 79: goto st243;
		case 111: goto st243;
	}
	goto tr0;
st243:
	if ( ++p == pe )
		goto _test_eof243;
case 243:
	_ps = 243;
	switch( (*p) ) {
		case 78: goto st244;
		case 110: goto st244;
	}
	goto tr0;
st244:
	if ( ++p == pe )
		goto _test_eof244;
case 244:
	_ps = 244;
	switch( (*p) ) {
		case 43: goto tr326;
		case 45: goto tr327;
		case 63: goto tr328;
	}
	goto tr0;
tr326:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st245;
tr327:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st245;
tr328:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st245;
st245:
	if ( ++p == pe )
		goto _test_eof245;
case 245:
#line 3731 "build/lm048lib.c"
	_ps = 245;
	if ( (*p) == 13 )
		goto tr329;
	goto tr0;
st246:
	if ( ++p == pe )
		goto _test_eof246;
case 246:
	_ps = 246;
	switch( (*p) ) {
		case 68: goto st247;
		case 100: goto st247;
	}
	goto tr0;
st247:
	if ( ++p == pe )
		goto _test_eof247;
case 247:
	_ps = 247;
	switch( (*p) ) {
		case 82: goto st248;
		case 114: goto st248;
	}
	goto tr0;
st248:
	if ( ++p == pe )
		goto _test_eof248;
case 248:
	_ps = 248;
	if ( (*p) == 63 )
		goto tr332;
	goto tr0;
tr332:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st249;
st249:
	if ( ++p == pe )
		goto _test_eof249;
case 249:
#line 3772 "build/lm048lib.c"
	_ps = 249;
	if ( (*p) == 13 )
		goto tr333;
	goto tr0;
st250:
	if ( ++p == pe )
		goto _test_eof250;
case 250:
	_ps = 250;
	switch( (*p) ) {
		case 84: goto st251;
		case 116: goto st251;
	}
	goto tr0;
st251:
	if ( ++p == pe )
		goto _test_eof251;
case 251:
	_ps = 251;
	switch( (*p) ) {
		case 79: goto st252;
		case 111: goto st252;
	}
	goto tr0;
st252:
	if ( ++p == pe )
		goto _test_eof252;
case 252:
	_ps = 252;
	if ( (*p) == 13 )
		goto tr336;
	goto tr0;
st253:
	if ( ++p == pe )
		goto _test_eof253;
case 253:
	_ps = 253;
	switch( (*p) ) {
		case 65: goto st254;
		case 79: goto st260;
		case 97: goto st254;
		case 111: goto st260;
	}
	goto tr0;
st254:
	if ( ++p == pe )
		goto _test_eof254;
case 254:
	_ps = 254;
	switch( (*p) ) {
		case 85: goto st255;
		case 117: goto st255;
	}
	goto tr0;
st255:
	if ( ++p == pe )
		goto _test_eof255;
case 255:
	_ps = 255;
	switch( (*p) ) {
		case 68: goto tr340;
		case 100: goto tr340;
	}
	goto tr0;
tr340:
#line 148 "build/lm048lib.rl"
	{ pkt->payload_length = 0; }
	goto st256;
st256:
	if ( ++p == pe )
		goto _test_eof256;
case 256:
#line 3845 "build/lm048lib.c"
	_ps = 256;
	switch( (*p) ) {
		case 49: goto st257;
		case 50: goto st259;
		case 63: goto tr343;
	}
	goto tr0;
st257:
	if ( ++p == pe )
		goto _test_eof257;
case 257:
	_ps = 257;
	switch( (*p) ) {
		case 48: goto tr344;
		case 49: goto tr345;
		case 50: goto tr346;
		case 51: goto tr347;
		case 52: goto tr348;
		case 53: goto tr349;
		case 54: goto tr350;
		case 55: goto tr351;
		case 56: goto tr352;
		case 57: goto tr353;
	}
	goto tr0;
tr343:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st258;
tr344:
#line 190 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_1200); }
	goto st258;
tr345:
#line 191 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_2400); }
	goto st258;
tr346:
#line 192 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_4800); }
	goto st258;
tr347:
#line 193 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_9600); }
	goto st258;
tr348:
#line 194 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_19200); }
	goto st258;
tr349:
#line 195 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_38400); }
	goto st258;
tr350:
#line 196 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_57600); }
	goto st258;
tr351:
#line 197 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_115200); }
	goto st258;
tr352:
#line 198 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_230400); }
	goto st258;
tr353:
#line 199 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_460800); }
	goto st258;
tr355:
#line 200 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_BAUD_921600); }
	goto st258;
st258:
	if ( ++p == pe )
		goto _test_eof258;
case 258:
#line 3923 "build/lm048lib.c"
	_ps = 258;
	if ( (*p) == 13 )
		goto tr354;
	goto tr0;
st259:
	if ( ++p == pe )
		goto _test_eof259;
case 259:
	_ps = 259;
	if ( (*p) == 48 )
		goto tr355;
	goto tr0;
st260:
	if ( ++p == pe )
		goto _test_eof260;
case 260:
	_ps = 260;
	switch( (*p) ) {
		case 78: goto st261;
		case 110: goto st261;
	}
	goto tr0;
st261:
	if ( ++p == pe )
		goto _test_eof261;
case 261:
	_ps = 261;
	switch( (*p) ) {
		case 68: goto st262;
		case 100: goto st262;
	}
	goto tr0;
st262:
	if ( ++p == pe )
		goto _test_eof262;
case 262:
	_ps = 262;
	switch( (*p) ) {
		case 45: goto tr358;
		case 61: goto tr359;
		case 63: goto tr360;
	}
	goto tr0;
tr358:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st263;
tr360:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st263;
tr375:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st263;
st263:
	if ( ++p == pe )
		goto _test_eof263;
case 263:
#line 3983 "build/lm048lib.c"
	_ps = 263;
	if ( (*p) == 13 )
		goto tr361;
	goto tr0;
tr359:
#line 137 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_SET); }
	goto st264;
st264:
	if ( ++p == pe )
		goto _test_eof264;
case 264:
#line 3996 "build/lm048lib.c"
	_ps = 264;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr362;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr362;
	} else
		goto tr362;
	goto tr0;
tr362:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st265;
st265:
	if ( ++p == pe )
		goto _test_eof265;
case 265:
#line 4015 "build/lm048lib.c"
	_ps = 265;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr363;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr363;
	} else
		goto tr363;
	goto tr0;
tr363:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st266;
st266:
	if ( ++p == pe )
		goto _test_eof266;
case 266:
#line 4034 "build/lm048lib.c"
	_ps = 266;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr364;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr364;
	} else
		goto tr364;
	goto tr0;
tr364:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st267;
st267:
	if ( ++p == pe )
		goto _test_eof267;
case 267:
#line 4053 "build/lm048lib.c"
	_ps = 267;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr365;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr365;
	} else
		goto tr365;
	goto tr0;
tr365:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st268;
st268:
	if ( ++p == pe )
		goto _test_eof268;
case 268:
#line 4072 "build/lm048lib.c"
	_ps = 268;
	if ( (*p) == 45 )
		goto tr366;
	goto tr0;
tr366:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st269;
st269:
	if ( ++p == pe )
		goto _test_eof269;
case 269:
#line 4085 "build/lm048lib.c"
	_ps = 269;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr367;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr367;
	} else
		goto tr367;
	goto tr0;
tr367:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st270;
st270:
	if ( ++p == pe )
		goto _test_eof270;
case 270:
#line 4104 "build/lm048lib.c"
	_ps = 270;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr368;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr368;
	} else
		goto tr368;
	goto tr0;
tr368:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st271;
st271:
	if ( ++p == pe )
		goto _test_eof271;
case 271:
#line 4123 "build/lm048lib.c"
	_ps = 271;
	if ( (*p) == 45 )
		goto tr369;
	goto tr0;
tr369:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st272;
st272:
	if ( ++p == pe )
		goto _test_eof272;
case 272:
#line 4136 "build/lm048lib.c"
	_ps = 272;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr370;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr370;
	} else
		goto tr370;
	goto tr0;
tr370:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st273;
st273:
	if ( ++p == pe )
		goto _test_eof273;
case 273:
#line 4155 "build/lm048lib.c"
	_ps = 273;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr371;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr371;
	} else
		goto tr371;
	goto tr0;
tr371:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st274;
st274:
	if ( ++p == pe )
		goto _test_eof274;
case 274:
#line 4174 "build/lm048lib.c"
	_ps = 274;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr372;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr372;
	} else
		goto tr372;
	goto tr0;
tr372:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st275;
st275:
	if ( ++p == pe )
		goto _test_eof275;
case 275:
#line 4193 "build/lm048lib.c"
	_ps = 275;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr373;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr373;
	} else
		goto tr373;
	goto tr0;
tr373:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st276;
st276:
	if ( ++p == pe )
		goto _test_eof276;
case 276:
#line 4212 "build/lm048lib.c"
	_ps = 276;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr374;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr374;
	} else
		goto tr374;
	goto tr0;
tr374:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st277;
st277:
	if ( ++p == pe )
		goto _test_eof277;
case 277:
#line 4231 "build/lm048lib.c"
	_ps = 277;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr375;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr375;
	} else
		goto tr375;
	goto tr0;
st278:
	if ( ++p == pe )
		goto _test_eof278;
case 278:
	_ps = 278;
	switch( (*p) ) {
		case 79: goto st279;
		case 111: goto st279;
	}
	goto tr0;
st279:
	if ( ++p == pe )
		goto _test_eof279;
case 279:
	_ps = 279;
	switch( (*p) ) {
		case 78: goto st280;
		case 110: goto st280;
	}
	goto tr0;
st280:
	if ( ++p == pe )
		goto _test_eof280;
case 280:
	_ps = 280;
	switch( (*p) ) {
		case 78: goto tr378;
		case 110: goto tr378;
	}
	goto tr0;
tr378:
#line 271 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_NONE); }
	goto st281;
st281:
	if ( ++p == pe )
		goto _test_eof281;
case 281:
#line 4280 "build/lm048lib.c"
	_ps = 281;
	switch( (*p) ) {
		case 13: goto st582;
		case 61: goto tr380;
	}
	goto tr0;
tr380:
#line 137 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_SET); }
#line 148 "build/lm048lib.rl"
	{ pkt->payload_length = 0; }
	goto st282;
st282:
	if ( ++p == pe )
		goto _test_eof282;
case 282:
#line 4297 "build/lm048lib.c"
	_ps = 282;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr381;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr381;
	} else
		goto tr381;
	goto tr0;
tr381:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st283;
st283:
	if ( ++p == pe )
		goto _test_eof283;
case 283:
#line 4316 "build/lm048lib.c"
	_ps = 283;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr382;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr382;
	} else
		goto tr382;
	goto tr0;
tr382:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st284;
st284:
	if ( ++p == pe )
		goto _test_eof284;
case 284:
#line 4335 "build/lm048lib.c"
	_ps = 284;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr383;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr383;
	} else
		goto tr383;
	goto tr0;
tr383:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st285;
st285:
	if ( ++p == pe )
		goto _test_eof285;
case 285:
#line 4354 "build/lm048lib.c"
	_ps = 285;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr384;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr384;
	} else
		goto tr384;
	goto tr0;
tr384:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st286;
st286:
	if ( ++p == pe )
		goto _test_eof286;
case 286:
#line 4373 "build/lm048lib.c"
	_ps = 286;
	if ( (*p) == 45 )
		goto tr385;
	goto tr0;
tr385:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st287;
st287:
	if ( ++p == pe )
		goto _test_eof287;
case 287:
#line 4386 "build/lm048lib.c"
	_ps = 287;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr386;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr386;
	} else
		goto tr386;
	goto tr0;
tr386:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st288;
st288:
	if ( ++p == pe )
		goto _test_eof288;
case 288:
#line 4405 "build/lm048lib.c"
	_ps = 288;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr387;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr387;
	} else
		goto tr387;
	goto tr0;
tr387:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st289;
st289:
	if ( ++p == pe )
		goto _test_eof289;
case 289:
#line 4424 "build/lm048lib.c"
	_ps = 289;
	if ( (*p) == 45 )
		goto tr388;
	goto tr0;
tr388:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st290;
st290:
	if ( ++p == pe )
		goto _test_eof290;
case 290:
#line 4437 "build/lm048lib.c"
	_ps = 290;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr389;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr389;
	} else
		goto tr389;
	goto tr0;
tr389:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st291;
st291:
	if ( ++p == pe )
		goto _test_eof291;
case 291:
#line 4456 "build/lm048lib.c"
	_ps = 291;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr390;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr390;
	} else
		goto tr390;
	goto tr0;
tr390:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st292;
st292:
	if ( ++p == pe )
		goto _test_eof292;
case 292:
#line 4475 "build/lm048lib.c"
	_ps = 292;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr391;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr391;
	} else
		goto tr391;
	goto tr0;
tr391:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st293;
st293:
	if ( ++p == pe )
		goto _test_eof293;
case 293:
#line 4494 "build/lm048lib.c"
	_ps = 293;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr392;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr392;
	} else
		goto tr392;
	goto tr0;
tr392:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st294;
st294:
	if ( ++p == pe )
		goto _test_eof294;
case 294:
#line 4513 "build/lm048lib.c"
	_ps = 294;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr393;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr393;
	} else
		goto tr393;
	goto tr0;
tr393:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st295;
st295:
	if ( ++p == pe )
		goto _test_eof295;
case 295:
#line 4532 "build/lm048lib.c"
	_ps = 295;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr394;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr394;
	} else
		goto tr394;
	goto tr0;
tr394:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st296;
st296:
	if ( ++p == pe )
		goto _test_eof296;
case 296:
#line 4551 "build/lm048lib.c"
	_ps = 296;
	if ( (*p) == 13 )
		goto st582;
	goto tr0;
st297:
	if ( ++p == pe )
		goto _test_eof297;
case 297:
	_ps = 297;
	switch( (*p) ) {
		case 67: goto st298;
		case 80: goto st302;
		case 82: goto st329;
		case 99: goto st298;
		case 112: goto st302;
		case 114: goto st329;
	}
	goto tr0;
st298:
	if ( ++p == pe )
		goto _test_eof298;
case 298:
	_ps = 298;
	switch( (*p) ) {
		case 79: goto st299;
		case 111: goto st299;
	}
	goto tr0;
st299:
	if ( ++p == pe )
		goto _test_eof299;
case 299:
	_ps = 299;
	switch( (*p) ) {
		case 86: goto st300;
		case 118: goto st300;
	}
	goto tr0;
st300:
	if ( ++p == pe )
		goto _test_eof300;
case 300:
	_ps = 300;
	switch( (*p) ) {
		case 43: goto tr400;
		case 45: goto tr401;
		case 63: goto tr402;
	}
	goto tr0;
tr400:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st301;
tr401:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st301;
tr402:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st301;
st301:
	if ( ++p == pe )
		goto _test_eof301;
case 301:
#line 4617 "build/lm048lib.c"
	_ps = 301;
	if ( (*p) == 13 )
		goto tr403;
	goto tr0;
st302:
	if ( ++p == pe )
		goto _test_eof302;
case 302:
	_ps = 302;
	switch( (*p) ) {
		case 73: goto st303;
		case 105: goto st303;
	}
	goto tr0;
st303:
	if ( ++p == pe )
		goto _test_eof303;
case 303:
	_ps = 303;
	switch( (*p) ) {
		case 78: goto st304;
		case 110: goto st304;
	}
	goto tr0;
st304:
	if ( ++p == pe )
		goto _test_eof304;
case 304:
	_ps = 304;
	switch( (*p) ) {
		case 43: goto tr406;
		case 45: goto tr407;
		case 61: goto tr408;
		case 63: goto tr409;
	}
	goto tr0;
tr406:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st305;
tr407:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st305;
tr409:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st305;
st305:
	if ( ++p == pe )
		goto _test_eof305;
case 305:
#line 4670 "build/lm048lib.c"
	_ps = 305;
	if ( (*p) == 13 )
		goto tr410;
	goto tr0;
tr408:
#line 137 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_SET); }
	goto st306;
st306:
	if ( ++p == pe )
		goto _test_eof306;
case 306:
#line 4683 "build/lm048lib.c"
	_ps = 306;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr411;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr411;
	} else
		goto tr411;
	goto tr0;
tr411:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st307;
st307:
	if ( ++p == pe )
		goto _test_eof307;
case 307:
#line 4702 "build/lm048lib.c"
	_ps = 307;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr412;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr412;
	} else
		goto tr412;
	goto tr0;
tr412:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st308;
st308:
	if ( ++p == pe )
		goto _test_eof308;
case 308:
#line 4721 "build/lm048lib.c"
	_ps = 308;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr413;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr413;
	} else
		goto tr413;
	goto tr0;
tr413:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st309;
st309:
	if ( ++p == pe )
		goto _test_eof309;
case 309:
#line 4740 "build/lm048lib.c"
	_ps = 309;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr414;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr414;
	} else
		goto tr414;
	goto tr0;
tr414:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st310;
st310:
	if ( ++p == pe )
		goto _test_eof310;
case 310:
#line 4759 "build/lm048lib.c"
	_ps = 310;
	if ( (*p) == 45 )
		goto tr415;
	goto tr0;
tr415:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st311;
st311:
	if ( ++p == pe )
		goto _test_eof311;
case 311:
#line 4772 "build/lm048lib.c"
	_ps = 311;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr416;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr416;
	} else
		goto tr416;
	goto tr0;
tr416:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st312;
st312:
	if ( ++p == pe )
		goto _test_eof312;
case 312:
#line 4791 "build/lm048lib.c"
	_ps = 312;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr417;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr417;
	} else
		goto tr417;
	goto tr0;
tr417:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st313;
st313:
	if ( ++p == pe )
		goto _test_eof313;
case 313:
#line 4810 "build/lm048lib.c"
	_ps = 313;
	if ( (*p) == 45 )
		goto tr418;
	goto tr0;
tr418:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st314;
st314:
	if ( ++p == pe )
		goto _test_eof314;
case 314:
#line 4823 "build/lm048lib.c"
	_ps = 314;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr419;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr419;
	} else
		goto tr419;
	goto tr0;
tr419:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st315;
st315:
	if ( ++p == pe )
		goto _test_eof315;
case 315:
#line 4842 "build/lm048lib.c"
	_ps = 315;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr420;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr420;
	} else
		goto tr420;
	goto tr0;
tr420:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st316;
st316:
	if ( ++p == pe )
		goto _test_eof316;
case 316:
#line 4861 "build/lm048lib.c"
	_ps = 316;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr421;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr421;
	} else
		goto tr421;
	goto tr0;
tr421:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st317;
st317:
	if ( ++p == pe )
		goto _test_eof317;
case 317:
#line 4880 "build/lm048lib.c"
	_ps = 317;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr422;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr422;
	} else
		goto tr422;
	goto tr0;
tr422:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st318;
st318:
	if ( ++p == pe )
		goto _test_eof318;
case 318:
#line 4899 "build/lm048lib.c"
	_ps = 318;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr423;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr423;
	} else
		goto tr423;
	goto tr0;
tr423:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st319;
st319:
	if ( ++p == pe )
		goto _test_eof319;
case 319:
#line 4918 "build/lm048lib.c"
	_ps = 319;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr424;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr424;
	} else
		goto tr424;
	goto tr0;
tr424:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st320;
st320:
	if ( ++p == pe )
		goto _test_eof320;
case 320:
#line 4937 "build/lm048lib.c"
	_ps = 320;
	if ( (*p) == 44 )
		goto st321;
	goto tr0;
st321:
	if ( ++p == pe )
		goto _test_eof321;
case 321:
	_ps = 321;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st322;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st322;
	} else
		goto st322;
	goto tr0;
st322:
	if ( ++p == pe )
		goto _test_eof322;
case 322:
	_ps = 322;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st323;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st323;
	} else
		goto st323;
	goto tr0;
st323:
	if ( ++p == pe )
		goto _test_eof323;
case 323:
	_ps = 323;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st324;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st324;
	} else
		goto st324;
	goto tr0;
st324:
	if ( ++p == pe )
		goto _test_eof324;
case 324:
	_ps = 324;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st325;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st325;
	} else
		goto st325;
	goto tr0;
st325:
	if ( ++p == pe )
		goto _test_eof325;
case 325:
	_ps = 325;
	if ( (*p) == 13 )
		goto tr410;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st326;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st326;
	} else
		goto st326;
	goto tr0;
st326:
	if ( ++p == pe )
		goto _test_eof326;
case 326:
	_ps = 326;
	if ( (*p) == 13 )
		goto tr410;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st327;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st327;
	} else
		goto st327;
	goto tr0;
st327:
	if ( ++p == pe )
		goto _test_eof327;
case 327:
	_ps = 327;
	if ( (*p) == 13 )
		goto tr410;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st328;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st328;
	} else
		goto st328;
	goto tr0;
st328:
	if ( ++p == pe )
		goto _test_eof328;
case 328:
	_ps = 328;
	if ( (*p) == 13 )
		goto tr410;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st305;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st305;
	} else
		goto st305;
	goto tr0;
st329:
	if ( ++p == pe )
		goto _test_eof329;
case 329:
	_ps = 329;
	switch( (*p) ) {
		case 79: goto st330;
		case 111: goto st330;
	}
	goto tr0;
st330:
	if ( ++p == pe )
		goto _test_eof330;
case 330:
	_ps = 330;
	switch( (*p) ) {
		case 80: goto st331;
		case 112: goto st331;
	}
	goto tr0;
st331:
	if ( ++p == pe )
		goto _test_eof331;
case 331:
	_ps = 331;
	if ( (*p) == 13 )
		goto tr436;
	goto tr0;
st332:
	if ( ++p == pe )
		goto _test_eof332;
case 332:
	_ps = 332;
	switch( (*p) ) {
		case 67: goto st333;
		case 83: goto st337;
		case 99: goto st333;
		case 115: goto st337;
	}
	goto tr0;
st333:
	if ( ++p == pe )
		goto _test_eof333;
case 333:
	_ps = 333;
	switch( (*p) ) {
		case 72: goto st334;
		case 104: goto st334;
	}
	goto tr0;
st334:
	if ( ++p == pe )
		goto _test_eof334;
case 334:
	_ps = 334;
	switch( (*p) ) {
		case 79: goto st335;
		case 111: goto st335;
	}
	goto tr0;
st335:
	if ( ++p == pe )
		goto _test_eof335;
case 335:
	_ps = 335;
	switch( (*p) ) {
		case 43: goto tr441;
		case 45: goto tr442;
		case 63: goto tr443;
	}
	goto tr0;
tr441:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st336;
tr442:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st336;
tr443:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st336;
st336:
	if ( ++p == pe )
		goto _test_eof336;
case 336:
#line 5149 "build/lm048lib.c"
	_ps = 336;
	if ( (*p) == 13 )
		goto tr444;
	goto tr0;
st337:
	if ( ++p == pe )
		goto _test_eof337;
case 337:
	_ps = 337;
	switch( (*p) ) {
		case 67: goto st338;
		case 99: goto st338;
	}
	goto tr0;
st338:
	if ( ++p == pe )
		goto _test_eof338;
case 338:
	_ps = 338;
	switch( (*p) ) {
		case 43: goto tr446;
		case 45: goto tr447;
		case 63: goto tr448;
	}
	goto tr0;
tr446:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st339;
tr447:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st339;
tr448:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st339;
st339:
	if ( ++p == pe )
		goto _test_eof339;
case 339:
#line 5191 "build/lm048lib.c"
	_ps = 339;
	if ( (*p) == 13 )
		goto tr449;
	goto tr0;
st340:
	if ( ++p == pe )
		goto _test_eof340;
case 340:
	_ps = 340;
	switch( (*p) ) {
		case 76: goto st341;
		case 108: goto st341;
	}
	goto tr0;
st341:
	if ( ++p == pe )
		goto _test_eof341;
case 341:
	_ps = 341;
	switch( (*p) ) {
		case 79: goto st342;
		case 111: goto st342;
	}
	goto tr0;
st342:
	if ( ++p == pe )
		goto _test_eof342;
case 342:
	_ps = 342;
	switch( (*p) ) {
		case 87: goto st343;
		case 119: goto st343;
	}
	goto tr0;
st343:
	if ( ++p == pe )
		goto _test_eof343;
case 343:
	_ps = 343;
	switch( (*p) ) {
		case 43: goto tr453;
		case 45: goto tr454;
		case 63: goto tr455;
	}
	goto tr0;
tr453:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st344;
tr454:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st344;
tr455:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st344;
st344:
	if ( ++p == pe )
		goto _test_eof344;
case 344:
#line 5253 "build/lm048lib.c"
	_ps = 344;
	if ( (*p) == 13 )
		goto tr456;
	goto tr0;
st345:
	if ( ++p == pe )
		goto _test_eof345;
case 345:
	_ps = 345;
	switch( (*p) ) {
		case 79: goto st346;
		case 111: goto st346;
	}
	goto tr0;
st346:
	if ( ++p == pe )
		goto _test_eof346;
case 346:
	_ps = 346;
	switch( (*p) ) {
		case 84: goto st347;
		case 116: goto st347;
	}
	goto tr0;
st347:
	if ( ++p == pe )
		goto _test_eof347;
case 347:
	_ps = 347;
	switch( (*p) ) {
		case 89: goto st348;
		case 121: goto st348;
	}
	goto tr0;
st348:
	if ( ++p == pe )
		goto _test_eof348;
case 348:
	_ps = 348;
	switch( (*p) ) {
		case 80: goto st349;
		case 112: goto st349;
	}
	goto tr0;
st349:
	if ( ++p == pe )
		goto _test_eof349;
case 349:
	_ps = 349;
	switch( (*p) ) {
		case 69: goto st350;
		case 101: goto st350;
	}
	goto tr0;
st350:
	if ( ++p == pe )
		goto _test_eof350;
case 350:
	_ps = 350;
	switch( (*p) ) {
		case 48: goto tr462;
		case 49: goto tr463;
		case 50: goto tr464;
		case 51: goto tr465;
		case 52: goto tr466;
		case 63: goto tr467;
	}
	goto tr0;
tr467:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st351;
tr462:
#line 303 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISPLAY_ONLY); }
	goto st351;
tr463:
#line 304 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISPLAY_YES_NO); }
	goto st351;
tr464:
#line 305 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_KEYBOARD_ONLY); }
	goto st351;
tr465:
#line 306 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_NO_DISPLAY_KEYBOARD); }
	goto st351;
tr466:
#line 307 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_REJECT_BT21); }
	goto st351;
st351:
	if ( ++p == pe )
		goto _test_eof351;
case 351:
#line 5350 "build/lm048lib.c"
	_ps = 351;
	if ( (*p) == 13 )
		goto tr468;
	goto tr0;
st352:
	if ( ++p == pe )
		goto _test_eof352;
case 352:
	_ps = 352;
	switch( (*p) ) {
		case 73: goto st353;
		case 79: goto st357;
		case 105: goto st353;
		case 111: goto st357;
	}
	goto tr0;
st353:
	if ( ++p == pe )
		goto _test_eof353;
case 353:
	_ps = 353;
	switch( (*p) ) {
		case 84: goto st354;
		case 116: goto st354;
	}
	goto tr0;
st354:
	if ( ++p == pe )
		goto _test_eof354;
case 354:
	_ps = 354;
	switch( (*p) ) {
		case 77: goto st355;
		case 109: goto st355;
	}
	goto tr0;
st355:
	if ( ++p == pe )
		goto _test_eof355;
case 355:
	_ps = 355;
	switch( (*p) ) {
		case 43: goto tr473;
		case 45: goto tr474;
		case 63: goto tr475;
	}
	goto tr0;
tr473:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st356;
tr474:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st356;
tr475:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st356;
st356:
	if ( ++p == pe )
		goto _test_eof356;
case 356:
#line 5414 "build/lm048lib.c"
	_ps = 356;
	if ( (*p) == 13 )
		goto tr476;
	goto tr0;
st357:
	if ( ++p == pe )
		goto _test_eof357;
case 357:
	_ps = 357;
	switch( (*p) ) {
		case 68: goto st358;
		case 100: goto st358;
	}
	goto tr0;
st358:
	if ( ++p == pe )
		goto _test_eof358;
case 358:
	_ps = 358;
	switch( (*p) ) {
		case 69: goto st359;
		case 101: goto st359;
	}
	goto tr0;
st359:
	if ( ++p == pe )
		goto _test_eof359;
case 359:
	_ps = 359;
	switch( (*p) ) {
		case 77: goto tr479;
		case 109: goto tr479;
	}
	goto tr0;
tr479:
#line 148 "build/lm048lib.rl"
	{ pkt->payload_length = 0; }
	goto st360;
st360:
	if ( ++p == pe )
		goto _test_eof360;
case 360:
#line 5457 "build/lm048lib.c"
	_ps = 360;
	switch( (*p) ) {
		case 45: goto tr480;
		case 63: goto tr481;
		case 76: goto tr482;
		case 82: goto tr483;
		case 108: goto tr482;
		case 114: goto tr483;
	}
	goto tr0;
tr480:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st361;
tr481:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st361;
tr482:
#line 236 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_LOCAL_LOOPBACK); }
	goto st361;
tr483:
#line 237 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_REMOTE_TRANSFER); }
	goto st361;
st361:
	if ( ++p == pe )
		goto _test_eof361;
case 361:
#line 5488 "build/lm048lib.c"
	_ps = 361;
	if ( (*p) == 13 )
		goto tr484;
	goto tr0;
st362:
	if ( ++p == pe )
		goto _test_eof362;
case 362:
	_ps = 362;
	switch( (*p) ) {
		case 65: goto st363;
		case 97: goto st363;
	}
	goto tr0;
st363:
	if ( ++p == pe )
		goto _test_eof363;
case 363:
	_ps = 363;
	switch( (*p) ) {
		case 77: goto st364;
		case 109: goto st364;
	}
	goto tr0;
st364:
	if ( ++p == pe )
		goto _test_eof364;
case 364:
	_ps = 364;
	switch( (*p) ) {
		case 69: goto st365;
		case 101: goto st365;
	}
	goto tr0;
st365:
	if ( ++p == pe )
		goto _test_eof365;
case 365:
	_ps = 365;
	switch( (*p) ) {
		case 61: goto tr488;
		case 63: goto tr489;
	}
	goto tr0;
tr488:
#line 137 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_SET); }
#line 148 "build/lm048lib.rl"
	{ pkt->payload_length = 0; }
	goto st366;
st366:
	if ( ++p == pe )
		goto _test_eof366;
case 366:
#line 5543 "build/lm048lib.c"
	_ps = 366;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr490;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr490;
	} else
		goto tr490;
	goto tr0;
tr490:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st367;
st367:
	if ( ++p == pe )
		goto _test_eof367;
case 367:
#line 5562 "build/lm048lib.c"
	_ps = 367;
	switch( (*p) ) {
		case 13: goto tr491;
		case 32: goto tr492;
		case 45: goto tr492;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr493;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr493;
	} else
		goto tr493;
	goto tr0;
tr492:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st368;
st368:
	if ( ++p == pe )
		goto _test_eof368;
case 368:
#line 5586 "build/lm048lib.c"
	_ps = 368;
	switch( (*p) ) {
		case 32: goto tr494;
		case 45: goto tr494;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr495;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr495;
	} else
		goto tr495;
	goto tr0;
tr494:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st369;
st369:
	if ( ++p == pe )
		goto _test_eof369;
case 369:
#line 5609 "build/lm048lib.c"
	_ps = 369;
	switch( (*p) ) {
		case 32: goto tr496;
		case 45: goto tr496;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr497;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr497;
	} else
		goto tr497;
	goto tr0;
tr496:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st370;
st370:
	if ( ++p == pe )
		goto _test_eof370;
case 370:
#line 5632 "build/lm048lib.c"
	_ps = 370;
	switch( (*p) ) {
		case 32: goto tr498;
		case 45: goto tr498;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr499;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr499;
	} else
		goto tr499;
	goto tr0;
tr498:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st371;
st371:
	if ( ++p == pe )
		goto _test_eof371;
case 371:
#line 5655 "build/lm048lib.c"
	_ps = 371;
	switch( (*p) ) {
		case 32: goto tr500;
		case 45: goto tr500;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr501;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr501;
	} else
		goto tr501;
	goto tr0;
tr500:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st372;
st372:
	if ( ++p == pe )
		goto _test_eof372;
case 372:
#line 5678 "build/lm048lib.c"
	_ps = 372;
	switch( (*p) ) {
		case 32: goto tr502;
		case 45: goto tr502;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr503;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr503;
	} else
		goto tr503;
	goto tr0;
tr502:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st373;
st373:
	if ( ++p == pe )
		goto _test_eof373;
case 373:
#line 5701 "build/lm048lib.c"
	_ps = 373;
	switch( (*p) ) {
		case 32: goto tr504;
		case 45: goto tr504;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr505;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr505;
	} else
		goto tr505;
	goto tr0;
tr504:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st374;
st374:
	if ( ++p == pe )
		goto _test_eof374;
case 374:
#line 5724 "build/lm048lib.c"
	_ps = 374;
	switch( (*p) ) {
		case 32: goto tr506;
		case 45: goto tr506;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr507;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr507;
	} else
		goto tr507;
	goto tr0;
tr506:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st375;
st375:
	if ( ++p == pe )
		goto _test_eof375;
case 375:
#line 5747 "build/lm048lib.c"
	_ps = 375;
	switch( (*p) ) {
		case 32: goto tr508;
		case 45: goto tr508;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr509;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr509;
	} else
		goto tr509;
	goto tr0;
tr508:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st376;
st376:
	if ( ++p == pe )
		goto _test_eof376;
case 376:
#line 5770 "build/lm048lib.c"
	_ps = 376;
	switch( (*p) ) {
		case 32: goto tr510;
		case 45: goto tr510;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr511;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr511;
	} else
		goto tr511;
	goto tr0;
tr510:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st377;
st377:
	if ( ++p == pe )
		goto _test_eof377;
case 377:
#line 5793 "build/lm048lib.c"
	_ps = 377;
	switch( (*p) ) {
		case 32: goto tr512;
		case 45: goto tr512;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr513;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr513;
	} else
		goto tr513;
	goto tr0;
tr512:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st378;
st378:
	if ( ++p == pe )
		goto _test_eof378;
case 378:
#line 5816 "build/lm048lib.c"
	_ps = 378;
	switch( (*p) ) {
		case 32: goto tr514;
		case 45: goto tr514;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr515;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr515;
	} else
		goto tr515;
	goto tr0;
tr514:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st379;
st379:
	if ( ++p == pe )
		goto _test_eof379;
case 379:
#line 5839 "build/lm048lib.c"
	_ps = 379;
	switch( (*p) ) {
		case 32: goto tr516;
		case 45: goto tr516;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr517;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr517;
	} else
		goto tr517;
	goto tr0;
tr516:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st380;
st380:
	if ( ++p == pe )
		goto _test_eof380;
case 380:
#line 5862 "build/lm048lib.c"
	_ps = 380;
	switch( (*p) ) {
		case 32: goto tr518;
		case 45: goto tr518;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr519;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr519;
	} else
		goto tr519;
	goto tr0;
tr518:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st381;
st381:
	if ( ++p == pe )
		goto _test_eof381;
case 381:
#line 5885 "build/lm048lib.c"
	_ps = 381;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr520;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr520;
	} else
		goto tr520;
	goto tr0;
tr489:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st382;
tr520:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st382;
st382:
	if ( ++p == pe )
		goto _test_eof382;
case 382:
#line 5908 "build/lm048lib.c"
	_ps = 382;
	if ( (*p) == 13 )
		goto tr491;
	goto tr0;
tr519:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st383;
st383:
	if ( ++p == pe )
		goto _test_eof383;
case 383:
#line 5921 "build/lm048lib.c"
	_ps = 383;
	if ( (*p) == 13 )
		goto tr491;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr520;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr520;
	} else
		goto tr520;
	goto tr0;
tr517:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st384;
st384:
	if ( ++p == pe )
		goto _test_eof384;
case 384:
#line 5942 "build/lm048lib.c"
	_ps = 384;
	switch( (*p) ) {
		case 13: goto tr491;
		case 32: goto tr518;
		case 45: goto tr518;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr519;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr519;
	} else
		goto tr519;
	goto tr0;
tr515:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st385;
st385:
	if ( ++p == pe )
		goto _test_eof385;
case 385:
#line 5966 "build/lm048lib.c"
	_ps = 385;
	switch( (*p) ) {
		case 13: goto tr491;
		case 32: goto tr516;
		case 45: goto tr516;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr517;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr517;
	} else
		goto tr517;
	goto tr0;
tr513:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st386;
st386:
	if ( ++p == pe )
		goto _test_eof386;
case 386:
#line 5990 "build/lm048lib.c"
	_ps = 386;
	switch( (*p) ) {
		case 13: goto tr491;
		case 32: goto tr514;
		case 45: goto tr514;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr515;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr515;
	} else
		goto tr515;
	goto tr0;
tr511:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st387;
st387:
	if ( ++p == pe )
		goto _test_eof387;
case 387:
#line 6014 "build/lm048lib.c"
	_ps = 387;
	switch( (*p) ) {
		case 13: goto tr491;
		case 32: goto tr512;
		case 45: goto tr512;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr513;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr513;
	} else
		goto tr513;
	goto tr0;
tr509:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st388;
st388:
	if ( ++p == pe )
		goto _test_eof388;
case 388:
#line 6038 "build/lm048lib.c"
	_ps = 388;
	switch( (*p) ) {
		case 13: goto tr491;
		case 32: goto tr510;
		case 45: goto tr510;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr511;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr511;
	} else
		goto tr511;
	goto tr0;
tr507:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st389;
st389:
	if ( ++p == pe )
		goto _test_eof389;
case 389:
#line 6062 "build/lm048lib.c"
	_ps = 389;
	switch( (*p) ) {
		case 13: goto tr491;
		case 32: goto tr508;
		case 45: goto tr508;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr509;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr509;
	} else
		goto tr509;
	goto tr0;
tr505:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st390;
st390:
	if ( ++p == pe )
		goto _test_eof390;
case 390:
#line 6086 "build/lm048lib.c"
	_ps = 390;
	switch( (*p) ) {
		case 13: goto tr491;
		case 32: goto tr506;
		case 45: goto tr506;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr507;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr507;
	} else
		goto tr507;
	goto tr0;
tr503:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st391;
st391:
	if ( ++p == pe )
		goto _test_eof391;
case 391:
#line 6110 "build/lm048lib.c"
	_ps = 391;
	switch( (*p) ) {
		case 13: goto tr491;
		case 32: goto tr504;
		case 45: goto tr504;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr505;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr505;
	} else
		goto tr505;
	goto tr0;
tr501:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st392;
st392:
	if ( ++p == pe )
		goto _test_eof392;
case 392:
#line 6134 "build/lm048lib.c"
	_ps = 392;
	switch( (*p) ) {
		case 13: goto tr491;
		case 32: goto tr502;
		case 45: goto tr502;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr503;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr503;
	} else
		goto tr503;
	goto tr0;
tr499:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st393;
st393:
	if ( ++p == pe )
		goto _test_eof393;
case 393:
#line 6158 "build/lm048lib.c"
	_ps = 393;
	switch( (*p) ) {
		case 13: goto tr491;
		case 32: goto tr500;
		case 45: goto tr500;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr501;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr501;
	} else
		goto tr501;
	goto tr0;
tr497:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st394;
st394:
	if ( ++p == pe )
		goto _test_eof394;
case 394:
#line 6182 "build/lm048lib.c"
	_ps = 394;
	switch( (*p) ) {
		case 13: goto tr491;
		case 32: goto tr498;
		case 45: goto tr498;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr499;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr499;
	} else
		goto tr499;
	goto tr0;
tr495:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st395;
st395:
	if ( ++p == pe )
		goto _test_eof395;
case 395:
#line 6206 "build/lm048lib.c"
	_ps = 395;
	switch( (*p) ) {
		case 13: goto tr491;
		case 32: goto tr496;
		case 45: goto tr496;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr497;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr497;
	} else
		goto tr497;
	goto tr0;
tr493:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st396;
st396:
	if ( ++p == pe )
		goto _test_eof396;
case 396:
#line 6230 "build/lm048lib.c"
	_ps = 396;
	switch( (*p) ) {
		case 13: goto tr491;
		case 32: goto tr494;
		case 45: goto tr494;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr495;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr495;
	} else
		goto tr495;
	goto tr0;
st397:
	if ( ++p == pe )
		goto _test_eof397;
case 397:
	_ps = 397;
	switch( (*p) ) {
		case 65: goto st398;
		case 73: goto st401;
		case 97: goto st398;
		case 105: goto st401;
	}
	goto tr0;
st398:
	if ( ++p == pe )
		goto _test_eof398;
case 398:
	_ps = 398;
	switch( (*p) ) {
		case 82: goto st399;
		case 114: goto st399;
	}
	goto tr0;
st399:
	if ( ++p == pe )
		goto _test_eof399;
case 399:
	_ps = 399;
	switch( (*p) ) {
		case 48: goto tr524;
		case 49: goto tr525;
		case 50: goto tr526;
		case 63: goto tr527;
	}
	goto tr0;
tr527:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st400;
tr524:
#line 216 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_NO_PARITY); }
	goto st400;
tr525:
#line 217 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ODD_PARITY); }
	goto st400;
tr526:
#line 218 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_EVEN_PARITY); }
	goto st400;
st400:
	if ( ++p == pe )
		goto _test_eof400;
case 400:
#line 6300 "build/lm048lib.c"
	_ps = 400;
	if ( (*p) == 13 )
		goto tr528;
	goto tr0;
st401:
	if ( ++p == pe )
		goto _test_eof401;
case 401:
	_ps = 401;
	switch( (*p) ) {
		case 78: goto st402;
		case 110: goto st402;
	}
	goto tr0;
st402:
	if ( ++p == pe )
		goto _test_eof402;
case 402:
	_ps = 402;
	switch( (*p) ) {
		case 43: goto tr530;
		case 45: goto tr531;
		case 61: goto tr532;
		case 63: goto tr533;
	}
	goto tr0;
tr530:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st403;
tr531:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st403;
tr533:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st403;
st403:
	if ( ++p == pe )
		goto _test_eof403;
case 403:
#line 6343 "build/lm048lib.c"
	_ps = 403;
	if ( (*p) == 13 )
		goto tr534;
	goto tr0;
tr532:
#line 137 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_SET); }
#line 148 "build/lm048lib.rl"
	{ pkt->payload_length = 0; }
	goto st404;
st404:
	if ( ++p == pe )
		goto _test_eof404;
case 404:
#line 6358 "build/lm048lib.c"
	_ps = 404;
	if ( (*p) == 9 )
		goto tr535;
	if ( (*p) > 12 ) {
		if ( 32 <= (*p) && (*p) <= 126 )
			goto tr535;
	} else if ( (*p) >= 11 )
		goto tr535;
	goto tr0;
tr535:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st405;
st405:
	if ( ++p == pe )
		goto _test_eof405;
case 405:
#line 6376 "build/lm048lib.c"
	_ps = 405;
	switch( (*p) ) {
		case 9: goto tr535;
		case 13: goto tr534;
	}
	if ( (*p) > 12 ) {
		if ( 32 <= (*p) && (*p) <= 126 )
			goto tr535;
	} else if ( (*p) >= 11 )
		goto tr535;
	goto tr0;
st406:
	if ( ++p == pe )
		goto _test_eof406;
case 406:
	_ps = 406;
	switch( (*p) ) {
		case 67: goto st407;
		case 69: goto st411;
		case 79: goto st417;
		case 83: goto st421;
		case 99: goto st407;
		case 101: goto st411;
		case 111: goto st417;
		case 115: goto st421;
	}
	goto tr0;
st407:
	if ( ++p == pe )
		goto _test_eof407;
case 407:
	_ps = 407;
	switch( (*p) ) {
		case 70: goto st408;
		case 102: goto st408;
	}
	goto tr0;
st408:
	if ( ++p == pe )
		goto _test_eof408;
case 408:
	_ps = 408;
	switch( (*p) ) {
		case 71: goto st409;
		case 103: goto st409;
	}
	goto tr0;
st409:
	if ( ++p == pe )
		goto _test_eof409;
case 409:
	_ps = 409;
	switch( (*p) ) {
		case 43: goto tr542;
		case 45: goto tr543;
		case 63: goto tr544;
	}
	goto tr0;
tr542:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st410;
tr543:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st410;
tr544:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st410;
st410:
	if ( ++p == pe )
		goto _test_eof410;
case 410:
#line 6451 "build/lm048lib.c"
	_ps = 410;
	if ( (*p) == 13 )
		goto tr545;
	goto tr0;
st411:
	if ( ++p == pe )
		goto _test_eof411;
case 411:
	_ps = 411;
	switch( (*p) ) {
		case 83: goto st412;
		case 115: goto st412;
	}
	goto tr0;
st412:
	if ( ++p == pe )
		goto _test_eof412;
case 412:
	_ps = 412;
	switch( (*p) ) {
		case 69: goto st413;
		case 80: goto st415;
		case 101: goto st413;
		case 112: goto st415;
	}
	goto tr0;
st413:
	if ( ++p == pe )
		goto _test_eof413;
case 413:
	_ps = 413;
	switch( (*p) ) {
		case 84: goto st414;
		case 116: goto st414;
	}
	goto tr0;
st414:
	if ( ++p == pe )
		goto _test_eof414;
case 414:
	_ps = 414;
	if ( (*p) == 13 )
		goto tr550;
	goto tr0;
st415:
	if ( ++p == pe )
		goto _test_eof415;
case 415:
	_ps = 415;
	switch( (*p) ) {
		case 43: goto tr551;
		case 45: goto tr552;
		case 63: goto tr553;
	}
	goto tr0;
tr551:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st416;
tr552:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st416;
tr553:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st416;
st416:
	if ( ++p == pe )
		goto _test_eof416;
case 416:
#line 6523 "build/lm048lib.c"
	_ps = 416;
	if ( (*p) == 13 )
		goto tr554;
	goto tr0;
st417:
	if ( ++p == pe )
		goto _test_eof417;
case 417:
	_ps = 417;
	switch( (*p) ) {
		case 76: goto st418;
		case 108: goto st418;
	}
	goto tr0;
st418:
	if ( ++p == pe )
		goto _test_eof418;
case 418:
	_ps = 418;
	switch( (*p) ) {
		case 69: goto tr556;
		case 101: goto tr556;
	}
	goto tr0;
tr556:
#line 148 "build/lm048lib.rl"
	{ pkt->payload_length = 0; }
	goto st419;
st419:
	if ( ++p == pe )
		goto _test_eof419;
case 419:
#line 6556 "build/lm048lib.c"
	_ps = 419;
	switch( (*p) ) {
		case 63: goto tr557;
		case 77: goto tr558;
		case 83: goto tr559;
		case 109: goto tr558;
		case 115: goto tr559;
	}
	goto tr0;
tr557:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st420;
tr558:
#line 243 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_MASTER); }
	goto st420;
tr559:
#line 244 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_SLAVE); }
	goto st420;
st420:
	if ( ++p == pe )
		goto _test_eof420;
case 420:
#line 6582 "build/lm048lib.c"
	_ps = 420;
	if ( (*p) == 13 )
		goto tr560;
	goto tr0;
st421:
	if ( ++p == pe )
		goto _test_eof421;
case 421:
	_ps = 421;
	switch( (*p) ) {
		case 83: goto st422;
		case 115: goto st422;
	}
	goto tr0;
st422:
	if ( ++p == pe )
		goto _test_eof422;
case 422:
	_ps = 422;
	switch( (*p) ) {
		case 73: goto st423;
		case 105: goto st423;
	}
	goto tr0;
st423:
	if ( ++p == pe )
		goto _test_eof423;
case 423:
	_ps = 423;
	if ( (*p) == 13 )
		goto tr563;
	goto tr0;
st424:
	if ( ++p == pe )
		goto _test_eof424;
case 424:
	_ps = 424;
	switch( (*p) ) {
		case 76: goto st425;
		case 84: goto st430;
		case 108: goto st425;
		case 116: goto st430;
	}
	goto tr0;
st425:
	if ( ++p == pe )
		goto _test_eof425;
case 425:
	_ps = 425;
	switch( (*p) ) {
		case 69: goto st426;
		case 101: goto st426;
	}
	goto tr0;
st426:
	if ( ++p == pe )
		goto _test_eof426;
case 426:
	_ps = 426;
	switch( (*p) ) {
		case 69: goto st427;
		case 101: goto st427;
	}
	goto tr0;
st427:
	if ( ++p == pe )
		goto _test_eof427;
case 427:
	_ps = 427;
	switch( (*p) ) {
		case 80: goto st428;
		case 112: goto st428;
	}
	goto tr0;
st428:
	if ( ++p == pe )
		goto _test_eof428;
case 428:
	_ps = 428;
	switch( (*p) ) {
		case 43: goto tr569;
		case 45: goto tr570;
		case 63: goto tr571;
	}
	goto tr0;
tr569:
#line 134 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ENABLE); }
	goto st429;
tr570:
#line 135 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_DISABLE); }
	goto st429;
tr571:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st429;
st429:
	if ( ++p == pe )
		goto _test_eof429;
case 429:
#line 6684 "build/lm048lib.c"
	_ps = 429;
	if ( (*p) == 13 )
		goto tr572;
	goto tr0;
st430:
	if ( ++p == pe )
		goto _test_eof430;
case 430:
	_ps = 430;
	switch( (*p) ) {
		case 79: goto st431;
		case 111: goto st431;
	}
	goto tr0;
st431:
	if ( ++p == pe )
		goto _test_eof431;
case 431:
	_ps = 431;
	switch( (*p) ) {
		case 80: goto st432;
		case 112: goto st432;
	}
	goto tr0;
st432:
	if ( ++p == pe )
		goto _test_eof432;
case 432:
	_ps = 432;
	switch( (*p) ) {
		case 49: goto tr575;
		case 50: goto tr576;
		case 63: goto tr577;
	}
	goto tr0;
tr577:
#line 136 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_GET); }
	goto st433;
tr575:
#line 209 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_ONE_STOPBIT); }
	goto st433;
tr576:
#line 210 "build/lm048lib.rl"
	{ PMOD(LM048_ATM_TWO_STOPBIT); }
	goto st433;
st433:
	if ( ++p == pe )
		goto _test_eof433;
case 433:
#line 6736 "build/lm048lib.c"
	_ps = 433;
	if ( (*p) == 13 )
		goto tr578;
	goto tr0;
st434:
	if ( ++p == pe )
		goto _test_eof434;
case 434:
	_ps = 434;
	switch( (*p) ) {
		case 69: goto st435;
		case 101: goto st435;
	}
	goto tr0;
st435:
	if ( ++p == pe )
		goto _test_eof435;
case 435:
	_ps = 435;
	switch( (*p) ) {
		case 82: goto st436;
		case 114: goto st436;
	}
	goto tr0;
st436:
	if ( ++p == pe )
		goto _test_eof436;
case 436:
	_ps = 436;
	if ( (*p) == 13 )
		goto tr581;
	goto tr0;
st437:
	if ( ++p == pe )
		goto _test_eof437;
case 437:
	_ps = 437;
	if ( (*p) == 13 )
		goto tr0;
	goto tr582;
tr582:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st438;
st438:
	if ( ++p == pe )
		goto _test_eof438;
case 438:
#line 6785 "build/lm048lib.c"
	if ( (*p) == 13 )
		goto st439;
	goto tr582;
st439:
	if ( ++p == pe )
		goto _test_eof439;
case 439:
	_ps = 439;
	if ( (*p) == 10 )
		goto st440;
	goto tr0;
st440:
	if ( ++p == pe )
		goto _test_eof440;
case 440:
	_ps = 440;
	if ( (*p) == 79 )
		goto st441;
	goto tr0;
st441:
	if ( ++p == pe )
		goto _test_eof441;
case 441:
	_ps = 441;
	if ( (*p) == 75 )
		goto st442;
	goto tr0;
st442:
	if ( ++p == pe )
		goto _test_eof442;
case 442:
	_ps = 442;
	if ( (*p) == 13 )
		goto st443;
	goto tr0;
st443:
	if ( ++p == pe )
		goto _test_eof443;
case 443:
	_ps = 443;
	if ( (*p) == 10 )
		goto tr588;
	goto tr0;
tr588:
#line 87 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_VALUE_RESPONSE); }
	goto st583;
st583:
#line 76 "build/lm048lib.rl"
	{
		if(state->on_completed != NULL){
			state->on_completed();
		}else{
			{p++;  state->cs = 583; goto _out;}
		}
	}
	if ( ++p == pe )
		goto _test_eof583;
case 583:
#line 6845 "build/lm048lib.c"
	goto st0;
st444:
	if ( ++p == pe )
		goto _test_eof444;
case 444:
	_ps = 444;
	switch( (*p) ) {
		case 67: goto st445;
		case 68: goto st471;
		case 80: goto st500;
	}
	goto tr0;
st445:
	if ( ++p == pe )
		goto _test_eof445;
case 445:
	_ps = 445;
	if ( (*p) == 79 )
		goto st446;
	goto tr0;
st446:
	if ( ++p == pe )
		goto _test_eof446;
case 446:
	_ps = 446;
	if ( (*p) == 78 )
		goto st447;
	goto tr0;
st447:
	if ( ++p == pe )
		goto _test_eof447;
case 447:
	_ps = 447;
	if ( (*p) == 78 )
		goto st448;
	goto tr0;
st448:
	if ( ++p == pe )
		goto _test_eof448;
case 448:
	_ps = 448;
	if ( (*p) == 69 )
		goto st449;
	goto tr0;
st449:
	if ( ++p == pe )
		goto _test_eof449;
case 449:
	_ps = 449;
	if ( (*p) == 67 )
		goto st450;
	goto tr0;
st450:
	if ( ++p == pe )
		goto _test_eof450;
case 450:
	_ps = 450;
	if ( (*p) == 84 )
		goto st451;
	goto tr0;
st451:
	if ( ++p == pe )
		goto _test_eof451;
case 451:
	_ps = 451;
	switch( (*p) ) {
		case 32: goto st452;
		case 34: goto tr599;
	}
	goto tr0;
st452:
	if ( ++p == pe )
		goto _test_eof452;
case 452:
	_ps = 452;
	switch( (*p) ) {
		case 32: goto st453;
		case 34: goto tr599;
	}
	goto tr0;
st453:
	if ( ++p == pe )
		goto _test_eof453;
case 453:
	_ps = 453;
	if ( (*p) == 34 )
		goto tr599;
	goto tr0;
tr599:
#line 148 "build/lm048lib.rl"
	{ pkt->payload_length = 0; }
	goto st454;
st454:
	if ( ++p == pe )
		goto _test_eof454;
case 454:
#line 6942 "build/lm048lib.c"
	_ps = 454;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr601;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr601;
	} else
		goto tr601;
	goto tr0;
tr601:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st455;
st455:
	if ( ++p == pe )
		goto _test_eof455;
case 455:
#line 6961 "build/lm048lib.c"
	_ps = 455;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr602;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr602;
	} else
		goto tr602;
	goto tr0;
tr602:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st456;
st456:
	if ( ++p == pe )
		goto _test_eof456;
case 456:
#line 6980 "build/lm048lib.c"
	_ps = 456;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr603;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr603;
	} else
		goto tr603;
	goto tr0;
tr603:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st457;
st457:
	if ( ++p == pe )
		goto _test_eof457;
case 457:
#line 6999 "build/lm048lib.c"
	_ps = 457;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr604;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr604;
	} else
		goto tr604;
	goto tr0;
tr604:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st458;
st458:
	if ( ++p == pe )
		goto _test_eof458;
case 458:
#line 7018 "build/lm048lib.c"
	_ps = 458;
	if ( (*p) == 45 )
		goto tr605;
	goto tr0;
tr605:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st459;
st459:
	if ( ++p == pe )
		goto _test_eof459;
case 459:
#line 7031 "build/lm048lib.c"
	_ps = 459;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr606;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr606;
	} else
		goto tr606;
	goto tr0;
tr606:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st460;
st460:
	if ( ++p == pe )
		goto _test_eof460;
case 460:
#line 7050 "build/lm048lib.c"
	_ps = 460;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr607;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr607;
	} else
		goto tr607;
	goto tr0;
tr607:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st461;
st461:
	if ( ++p == pe )
		goto _test_eof461;
case 461:
#line 7069 "build/lm048lib.c"
	_ps = 461;
	if ( (*p) == 45 )
		goto tr608;
	goto tr0;
tr608:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st462;
st462:
	if ( ++p == pe )
		goto _test_eof462;
case 462:
#line 7082 "build/lm048lib.c"
	_ps = 462;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr609;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr609;
	} else
		goto tr609;
	goto tr0;
tr609:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st463;
st463:
	if ( ++p == pe )
		goto _test_eof463;
case 463:
#line 7101 "build/lm048lib.c"
	_ps = 463;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr610;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr610;
	} else
		goto tr610;
	goto tr0;
tr610:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st464;
st464:
	if ( ++p == pe )
		goto _test_eof464;
case 464:
#line 7120 "build/lm048lib.c"
	_ps = 464;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr611;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr611;
	} else
		goto tr611;
	goto tr0;
tr611:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st465;
st465:
	if ( ++p == pe )
		goto _test_eof465;
case 465:
#line 7139 "build/lm048lib.c"
	_ps = 465;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr612;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr612;
	} else
		goto tr612;
	goto tr0;
tr612:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st466;
st466:
	if ( ++p == pe )
		goto _test_eof466;
case 466:
#line 7158 "build/lm048lib.c"
	_ps = 466;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr613;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr613;
	} else
		goto tr613;
	goto tr0;
tr613:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st467;
st467:
	if ( ++p == pe )
		goto _test_eof467;
case 467:
#line 7177 "build/lm048lib.c"
	_ps = 467;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr614;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr614;
	} else
		goto tr614;
	goto tr0;
tr614:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st468;
st468:
	if ( ++p == pe )
		goto _test_eof468;
case 468:
#line 7196 "build/lm048lib.c"
	_ps = 468;
	if ( (*p) == 34 )
		goto st469;
	goto tr0;
st469:
	if ( ++p == pe )
		goto _test_eof469;
case 469:
	_ps = 469;
	if ( (*p) == 13 )
		goto st470;
	goto tr0;
st470:
	if ( ++p == pe )
		goto _test_eof470;
case 470:
	_ps = 470;
	if ( (*p) == 10 )
		goto tr617;
	goto tr0;
tr617:
#line 139 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_CONNECT_EVNT); }
	goto st584;
tr646:
#line 141 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_DISCONNECT_EVNT); }
	goto st584;
tr683:
#line 143 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_PASSKEYCFM_EVNT); }
	goto st584;
tr710:
#line 144 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_PASSKEYDSP_EVNT); }
	goto st584;
tr730:
#line 145 "build/lm048lib.rl"
	{ PTYPE(LM048_AT_PASSKEYREQ_EVNT); }
	goto st584;
st584:
#line 76 "build/lm048lib.rl"
	{
		if(state->on_completed != NULL){
			state->on_completed();
		}else{
			{p++;  state->cs = 584; goto _out;}
		}
	}
	if ( ++p == pe )
		goto _test_eof584;
case 584:
#line 7249 "build/lm048lib.c"
	goto st0;
st471:
	if ( ++p == pe )
		goto _test_eof471;
case 471:
	_ps = 471;
	if ( (*p) == 73 )
		goto st472;
	goto tr0;
st472:
	if ( ++p == pe )
		goto _test_eof472;
case 472:
	_ps = 472;
	if ( (*p) == 83 )
		goto st473;
	goto tr0;
st473:
	if ( ++p == pe )
		goto _test_eof473;
case 473:
	_ps = 473;
	if ( (*p) == 67 )
		goto st474;
	goto tr0;
st474:
	if ( ++p == pe )
		goto _test_eof474;
case 474:
	_ps = 474;
	if ( (*p) == 79 )
		goto st475;
	goto tr0;
st475:
	if ( ++p == pe )
		goto _test_eof475;
case 475:
	_ps = 475;
	if ( (*p) == 78 )
		goto st476;
	goto tr0;
st476:
	if ( ++p == pe )
		goto _test_eof476;
case 476:
	_ps = 476;
	if ( (*p) == 78 )
		goto st477;
	goto tr0;
st477:
	if ( ++p == pe )
		goto _test_eof477;
case 477:
	_ps = 477;
	if ( (*p) == 69 )
		goto st478;
	goto tr0;
st478:
	if ( ++p == pe )
		goto _test_eof478;
case 478:
	_ps = 478;
	if ( (*p) == 67 )
		goto st479;
	goto tr0;
st479:
	if ( ++p == pe )
		goto _test_eof479;
case 479:
	_ps = 479;
	if ( (*p) == 84 )
		goto st480;
	goto tr0;
st480:
	if ( ++p == pe )
		goto _test_eof480;
case 480:
	_ps = 480;
	switch( (*p) ) {
		case 32: goto st481;
		case 34: goto tr628;
	}
	goto tr0;
st481:
	if ( ++p == pe )
		goto _test_eof481;
case 481:
	_ps = 481;
	switch( (*p) ) {
		case 32: goto st482;
		case 34: goto tr628;
	}
	goto tr0;
st482:
	if ( ++p == pe )
		goto _test_eof482;
case 482:
	_ps = 482;
	if ( (*p) == 34 )
		goto tr628;
	goto tr0;
tr628:
#line 148 "build/lm048lib.rl"
	{ pkt->payload_length = 0; }
	goto st483;
st483:
	if ( ++p == pe )
		goto _test_eof483;
case 483:
#line 7359 "build/lm048lib.c"
	_ps = 483;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr630;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr630;
	} else
		goto tr630;
	goto tr0;
tr630:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st484;
st484:
	if ( ++p == pe )
		goto _test_eof484;
case 484:
#line 7378 "build/lm048lib.c"
	_ps = 484;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr631;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr631;
	} else
		goto tr631;
	goto tr0;
tr631:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st485;
st485:
	if ( ++p == pe )
		goto _test_eof485;
case 485:
#line 7397 "build/lm048lib.c"
	_ps = 485;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr632;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr632;
	} else
		goto tr632;
	goto tr0;
tr632:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st486;
st486:
	if ( ++p == pe )
		goto _test_eof486;
case 486:
#line 7416 "build/lm048lib.c"
	_ps = 486;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr633;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr633;
	} else
		goto tr633;
	goto tr0;
tr633:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st487;
st487:
	if ( ++p == pe )
		goto _test_eof487;
case 487:
#line 7435 "build/lm048lib.c"
	_ps = 487;
	if ( (*p) == 45 )
		goto tr634;
	goto tr0;
tr634:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st488;
st488:
	if ( ++p == pe )
		goto _test_eof488;
case 488:
#line 7448 "build/lm048lib.c"
	_ps = 488;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr635;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr635;
	} else
		goto tr635;
	goto tr0;
tr635:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st489;
st489:
	if ( ++p == pe )
		goto _test_eof489;
case 489:
#line 7467 "build/lm048lib.c"
	_ps = 489;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr636;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr636;
	} else
		goto tr636;
	goto tr0;
tr636:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st490;
st490:
	if ( ++p == pe )
		goto _test_eof490;
case 490:
#line 7486 "build/lm048lib.c"
	_ps = 490;
	if ( (*p) == 45 )
		goto tr637;
	goto tr0;
tr637:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st491;
st491:
	if ( ++p == pe )
		goto _test_eof491;
case 491:
#line 7499 "build/lm048lib.c"
	_ps = 491;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr638;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr638;
	} else
		goto tr638;
	goto tr0;
tr638:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st492;
st492:
	if ( ++p == pe )
		goto _test_eof492;
case 492:
#line 7518 "build/lm048lib.c"
	_ps = 492;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr639;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr639;
	} else
		goto tr639;
	goto tr0;
tr639:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st493;
st493:
	if ( ++p == pe )
		goto _test_eof493;
case 493:
#line 7537 "build/lm048lib.c"
	_ps = 493;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr640;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr640;
	} else
		goto tr640;
	goto tr0;
tr640:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st494;
st494:
	if ( ++p == pe )
		goto _test_eof494;
case 494:
#line 7556 "build/lm048lib.c"
	_ps = 494;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr641;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr641;
	} else
		goto tr641;
	goto tr0;
tr641:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st495;
st495:
	if ( ++p == pe )
		goto _test_eof495;
case 495:
#line 7575 "build/lm048lib.c"
	_ps = 495;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr642;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr642;
	} else
		goto tr642;
	goto tr0;
tr642:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st496;
st496:
	if ( ++p == pe )
		goto _test_eof496;
case 496:
#line 7594 "build/lm048lib.c"
	_ps = 496;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr643;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr643;
	} else
		goto tr643;
	goto tr0;
tr643:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st497;
st497:
	if ( ++p == pe )
		goto _test_eof497;
case 497:
#line 7613 "build/lm048lib.c"
	_ps = 497;
	if ( (*p) == 34 )
		goto st498;
	goto tr0;
st498:
	if ( ++p == pe )
		goto _test_eof498;
case 498:
	_ps = 498;
	if ( (*p) == 13 )
		goto st499;
	goto tr0;
st499:
	if ( ++p == pe )
		goto _test_eof499;
case 499:
	_ps = 499;
	if ( (*p) == 10 )
		goto tr646;
	goto tr0;
st500:
	if ( ++p == pe )
		goto _test_eof500;
case 500:
	_ps = 500;
	if ( (*p) == 65 )
		goto st501;
	goto tr0;
st501:
	if ( ++p == pe )
		goto _test_eof501;
case 501:
	_ps = 501;
	if ( (*p) == 83 )
		goto st502;
	goto tr0;
st502:
	if ( ++p == pe )
		goto _test_eof502;
case 502:
	_ps = 502;
	if ( (*p) == 83 )
		goto st503;
	goto tr0;
st503:
	if ( ++p == pe )
		goto _test_eof503;
case 503:
	_ps = 503;
	if ( (*p) == 75 )
		goto st504;
	goto tr0;
st504:
	if ( ++p == pe )
		goto _test_eof504;
case 504:
	_ps = 504;
	if ( (*p) == 69 )
		goto st505;
	goto tr0;
st505:
	if ( ++p == pe )
		goto _test_eof505;
case 505:
	_ps = 505;
	if ( (*p) == 89 )
		goto st506;
	goto tr0;
st506:
	if ( ++p == pe )
		goto _test_eof506;
case 506:
	_ps = 506;
	if ( (*p) == 32 )
		goto st507;
	goto tr0;
st507:
	if ( ++p == pe )
		goto _test_eof507;
case 507:
	_ps = 507;
	switch( (*p) ) {
		case 67: goto st508;
		case 68: goto st535;
		case 82: goto st562;
	}
	goto tr0;
st508:
	if ( ++p == pe )
		goto _test_eof508;
case 508:
	_ps = 508;
	if ( (*p) == 70 )
		goto st509;
	goto tr0;
st509:
	if ( ++p == pe )
		goto _test_eof509;
case 509:
	_ps = 509;
	if ( (*p) == 77 )
		goto st510;
	goto tr0;
st510:
	if ( ++p == pe )
		goto _test_eof510;
case 510:
	_ps = 510;
	if ( (*p) == 34 )
		goto tr659;
	goto tr0;
tr659:
#line 148 "build/lm048lib.rl"
	{ pkt->payload_length = 0; }
	goto st511;
st511:
	if ( ++p == pe )
		goto _test_eof511;
case 511:
#line 7733 "build/lm048lib.c"
	_ps = 511;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr660;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr660;
	} else
		goto tr660;
	goto tr0;
tr660:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st512;
st512:
	if ( ++p == pe )
		goto _test_eof512;
case 512:
#line 7752 "build/lm048lib.c"
	_ps = 512;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr661;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr661;
	} else
		goto tr661;
	goto tr0;
tr661:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st513;
st513:
	if ( ++p == pe )
		goto _test_eof513;
case 513:
#line 7771 "build/lm048lib.c"
	_ps = 513;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr662;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr662;
	} else
		goto tr662;
	goto tr0;
tr662:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st514;
st514:
	if ( ++p == pe )
		goto _test_eof514;
case 514:
#line 7790 "build/lm048lib.c"
	_ps = 514;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr663;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr663;
	} else
		goto tr663;
	goto tr0;
tr663:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st515;
st515:
	if ( ++p == pe )
		goto _test_eof515;
case 515:
#line 7809 "build/lm048lib.c"
	_ps = 515;
	if ( (*p) == 45 )
		goto tr664;
	goto tr0;
tr664:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st516;
st516:
	if ( ++p == pe )
		goto _test_eof516;
case 516:
#line 7822 "build/lm048lib.c"
	_ps = 516;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr665;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr665;
	} else
		goto tr665;
	goto tr0;
tr665:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st517;
st517:
	if ( ++p == pe )
		goto _test_eof517;
case 517:
#line 7841 "build/lm048lib.c"
	_ps = 517;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr666;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr666;
	} else
		goto tr666;
	goto tr0;
tr666:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st518;
st518:
	if ( ++p == pe )
		goto _test_eof518;
case 518:
#line 7860 "build/lm048lib.c"
	_ps = 518;
	if ( (*p) == 45 )
		goto tr667;
	goto tr0;
tr667:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st519;
st519:
	if ( ++p == pe )
		goto _test_eof519;
case 519:
#line 7873 "build/lm048lib.c"
	_ps = 519;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr668;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr668;
	} else
		goto tr668;
	goto tr0;
tr668:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st520;
st520:
	if ( ++p == pe )
		goto _test_eof520;
case 520:
#line 7892 "build/lm048lib.c"
	_ps = 520;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr669;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr669;
	} else
		goto tr669;
	goto tr0;
tr669:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st521;
st521:
	if ( ++p == pe )
		goto _test_eof521;
case 521:
#line 7911 "build/lm048lib.c"
	_ps = 521;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr670;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr670;
	} else
		goto tr670;
	goto tr0;
tr670:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st522;
st522:
	if ( ++p == pe )
		goto _test_eof522;
case 522:
#line 7930 "build/lm048lib.c"
	_ps = 522;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr671;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr671;
	} else
		goto tr671;
	goto tr0;
tr671:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st523;
st523:
	if ( ++p == pe )
		goto _test_eof523;
case 523:
#line 7949 "build/lm048lib.c"
	_ps = 523;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr672;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr672;
	} else
		goto tr672;
	goto tr0;
tr672:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st524;
st524:
	if ( ++p == pe )
		goto _test_eof524;
case 524:
#line 7968 "build/lm048lib.c"
	_ps = 524;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr673;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr673;
	} else
		goto tr673;
	goto tr0;
tr673:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st525;
st525:
	if ( ++p == pe )
		goto _test_eof525;
case 525:
#line 7987 "build/lm048lib.c"
	_ps = 525;
	if ( (*p) == 34 )
		goto st526;
	goto tr0;
st526:
	if ( ++p == pe )
		goto _test_eof526;
case 526:
	_ps = 526;
	if ( (*p) == 44 )
		goto tr675;
	goto tr0;
tr675:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st527;
st527:
	if ( ++p == pe )
		goto _test_eof527;
case 527:
#line 8008 "build/lm048lib.c"
	_ps = 527;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr676;
	goto tr0;
tr676:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st528;
st528:
	if ( ++p == pe )
		goto _test_eof528;
case 528:
#line 8021 "build/lm048lib.c"
	_ps = 528;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr677;
	goto tr0;
tr677:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st529;
st529:
	if ( ++p == pe )
		goto _test_eof529;
case 529:
#line 8034 "build/lm048lib.c"
	_ps = 529;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr678;
	goto tr0;
tr678:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st530;
st530:
	if ( ++p == pe )
		goto _test_eof530;
case 530:
#line 8047 "build/lm048lib.c"
	_ps = 530;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr679;
	goto tr0;
tr679:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st531;
st531:
	if ( ++p == pe )
		goto _test_eof531;
case 531:
#line 8060 "build/lm048lib.c"
	_ps = 531;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr680;
	goto tr0;
tr680:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st532;
st532:
	if ( ++p == pe )
		goto _test_eof532;
case 532:
#line 8073 "build/lm048lib.c"
	_ps = 532;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr681;
	goto tr0;
tr681:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st533;
st533:
	if ( ++p == pe )
		goto _test_eof533;
case 533:
#line 8086 "build/lm048lib.c"
	_ps = 533;
	if ( (*p) == 13 )
		goto st534;
	goto tr0;
st534:
	if ( ++p == pe )
		goto _test_eof534;
case 534:
	_ps = 534;
	if ( (*p) == 10 )
		goto tr683;
	goto tr0;
st535:
	if ( ++p == pe )
		goto _test_eof535;
case 535:
	_ps = 535;
	if ( (*p) == 83 )
		goto st536;
	goto tr0;
st536:
	if ( ++p == pe )
		goto _test_eof536;
case 536:
	_ps = 536;
	if ( (*p) == 80 )
		goto st537;
	goto tr0;
st537:
	if ( ++p == pe )
		goto _test_eof537;
case 537:
	_ps = 537;
	if ( (*p) == 34 )
		goto tr686;
	goto tr0;
tr686:
#line 148 "build/lm048lib.rl"
	{ pkt->payload_length = 0; }
	goto st538;
st538:
	if ( ++p == pe )
		goto _test_eof538;
case 538:
#line 8131 "build/lm048lib.c"
	_ps = 538;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr687;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr687;
	} else
		goto tr687;
	goto tr0;
tr687:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st539;
st539:
	if ( ++p == pe )
		goto _test_eof539;
case 539:
#line 8150 "build/lm048lib.c"
	_ps = 539;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr688;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr688;
	} else
		goto tr688;
	goto tr0;
tr688:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st540;
st540:
	if ( ++p == pe )
		goto _test_eof540;
case 540:
#line 8169 "build/lm048lib.c"
	_ps = 540;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr689;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr689;
	} else
		goto tr689;
	goto tr0;
tr689:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st541;
st541:
	if ( ++p == pe )
		goto _test_eof541;
case 541:
#line 8188 "build/lm048lib.c"
	_ps = 541;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr690;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr690;
	} else
		goto tr690;
	goto tr0;
tr690:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st542;
st542:
	if ( ++p == pe )
		goto _test_eof542;
case 542:
#line 8207 "build/lm048lib.c"
	_ps = 542;
	if ( (*p) == 45 )
		goto tr691;
	goto tr0;
tr691:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st543;
st543:
	if ( ++p == pe )
		goto _test_eof543;
case 543:
#line 8220 "build/lm048lib.c"
	_ps = 543;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr692;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr692;
	} else
		goto tr692;
	goto tr0;
tr692:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st544;
st544:
	if ( ++p == pe )
		goto _test_eof544;
case 544:
#line 8239 "build/lm048lib.c"
	_ps = 544;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr693;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr693;
	} else
		goto tr693;
	goto tr0;
tr693:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st545;
st545:
	if ( ++p == pe )
		goto _test_eof545;
case 545:
#line 8258 "build/lm048lib.c"
	_ps = 545;
	if ( (*p) == 45 )
		goto tr694;
	goto tr0;
tr694:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st546;
st546:
	if ( ++p == pe )
		goto _test_eof546;
case 546:
#line 8271 "build/lm048lib.c"
	_ps = 546;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr695;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr695;
	} else
		goto tr695;
	goto tr0;
tr695:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st547;
st547:
	if ( ++p == pe )
		goto _test_eof547;
case 547:
#line 8290 "build/lm048lib.c"
	_ps = 547;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr696;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr696;
	} else
		goto tr696;
	goto tr0;
tr696:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st548;
st548:
	if ( ++p == pe )
		goto _test_eof548;
case 548:
#line 8309 "build/lm048lib.c"
	_ps = 548;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr697;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr697;
	} else
		goto tr697;
	goto tr0;
tr697:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st549;
st549:
	if ( ++p == pe )
		goto _test_eof549;
case 549:
#line 8328 "build/lm048lib.c"
	_ps = 549;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr698;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr698;
	} else
		goto tr698;
	goto tr0;
tr698:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st550;
st550:
	if ( ++p == pe )
		goto _test_eof550;
case 550:
#line 8347 "build/lm048lib.c"
	_ps = 550;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr699;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr699;
	} else
		goto tr699;
	goto tr0;
tr699:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st551;
st551:
	if ( ++p == pe )
		goto _test_eof551;
case 551:
#line 8366 "build/lm048lib.c"
	_ps = 551;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr700;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr700;
	} else
		goto tr700;
	goto tr0;
tr700:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st552;
st552:
	if ( ++p == pe )
		goto _test_eof552;
case 552:
#line 8385 "build/lm048lib.c"
	_ps = 552;
	if ( (*p) == 34 )
		goto st553;
	goto tr0;
st553:
	if ( ++p == pe )
		goto _test_eof553;
case 553:
	_ps = 553;
	if ( (*p) == 44 )
		goto tr702;
	goto tr0;
tr702:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st554;
st554:
	if ( ++p == pe )
		goto _test_eof554;
case 554:
#line 8406 "build/lm048lib.c"
	_ps = 554;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr703;
	goto tr0;
tr703:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st555;
st555:
	if ( ++p == pe )
		goto _test_eof555;
case 555:
#line 8419 "build/lm048lib.c"
	_ps = 555;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr704;
	goto tr0;
tr704:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st556;
st556:
	if ( ++p == pe )
		goto _test_eof556;
case 556:
#line 8432 "build/lm048lib.c"
	_ps = 556;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr705;
	goto tr0;
tr705:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st557;
st557:
	if ( ++p == pe )
		goto _test_eof557;
case 557:
#line 8445 "build/lm048lib.c"
	_ps = 557;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr706;
	goto tr0;
tr706:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st558;
st558:
	if ( ++p == pe )
		goto _test_eof558;
case 558:
#line 8458 "build/lm048lib.c"
	_ps = 558;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr707;
	goto tr0;
tr707:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st559;
st559:
	if ( ++p == pe )
		goto _test_eof559;
case 559:
#line 8471 "build/lm048lib.c"
	_ps = 559;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr708;
	goto tr0;
tr708:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st560;
st560:
	if ( ++p == pe )
		goto _test_eof560;
case 560:
#line 8484 "build/lm048lib.c"
	_ps = 560;
	if ( (*p) == 13 )
		goto st561;
	goto tr0;
st561:
	if ( ++p == pe )
		goto _test_eof561;
case 561:
	_ps = 561;
	if ( (*p) == 10 )
		goto tr710;
	goto tr0;
st562:
	if ( ++p == pe )
		goto _test_eof562;
case 562:
	_ps = 562;
	if ( (*p) == 69 )
		goto st563;
	goto tr0;
st563:
	if ( ++p == pe )
		goto _test_eof563;
case 563:
	_ps = 563;
	if ( (*p) == 81 )
		goto st564;
	goto tr0;
st564:
	if ( ++p == pe )
		goto _test_eof564;
case 564:
	_ps = 564;
	if ( (*p) == 34 )
		goto tr713;
	goto tr0;
tr713:
#line 148 "build/lm048lib.rl"
	{ pkt->payload_length = 0; }
	goto st565;
st565:
	if ( ++p == pe )
		goto _test_eof565;
case 565:
#line 8529 "build/lm048lib.c"
	_ps = 565;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr714;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr714;
	} else
		goto tr714;
	goto tr0;
tr714:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st566;
st566:
	if ( ++p == pe )
		goto _test_eof566;
case 566:
#line 8548 "build/lm048lib.c"
	_ps = 566;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr715;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr715;
	} else
		goto tr715;
	goto tr0;
tr715:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st567;
st567:
	if ( ++p == pe )
		goto _test_eof567;
case 567:
#line 8567 "build/lm048lib.c"
	_ps = 567;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr716;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr716;
	} else
		goto tr716;
	goto tr0;
tr716:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st568;
st568:
	if ( ++p == pe )
		goto _test_eof568;
case 568:
#line 8586 "build/lm048lib.c"
	_ps = 568;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr717;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr717;
	} else
		goto tr717;
	goto tr0;
tr717:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st569;
st569:
	if ( ++p == pe )
		goto _test_eof569;
case 569:
#line 8605 "build/lm048lib.c"
	_ps = 569;
	if ( (*p) == 45 )
		goto tr718;
	goto tr0;
tr718:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st570;
st570:
	if ( ++p == pe )
		goto _test_eof570;
case 570:
#line 8618 "build/lm048lib.c"
	_ps = 570;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr719;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr719;
	} else
		goto tr719;
	goto tr0;
tr719:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st571;
st571:
	if ( ++p == pe )
		goto _test_eof571;
case 571:
#line 8637 "build/lm048lib.c"
	_ps = 571;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr720;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr720;
	} else
		goto tr720;
	goto tr0;
tr720:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st572;
st572:
	if ( ++p == pe )
		goto _test_eof572;
case 572:
#line 8656 "build/lm048lib.c"
	_ps = 572;
	if ( (*p) == 45 )
		goto tr721;
	goto tr0;
tr721:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st573;
st573:
	if ( ++p == pe )
		goto _test_eof573;
case 573:
#line 8669 "build/lm048lib.c"
	_ps = 573;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr722;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr722;
	} else
		goto tr722;
	goto tr0;
tr722:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st574;
st574:
	if ( ++p == pe )
		goto _test_eof574;
case 574:
#line 8688 "build/lm048lib.c"
	_ps = 574;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr723;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr723;
	} else
		goto tr723;
	goto tr0;
tr723:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st575;
st575:
	if ( ++p == pe )
		goto _test_eof575;
case 575:
#line 8707 "build/lm048lib.c"
	_ps = 575;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr724;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr724;
	} else
		goto tr724;
	goto tr0;
tr724:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st576;
st576:
	if ( ++p == pe )
		goto _test_eof576;
case 576:
#line 8726 "build/lm048lib.c"
	_ps = 576;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr725;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr725;
	} else
		goto tr725;
	goto tr0;
tr725:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st577;
st577:
	if ( ++p == pe )
		goto _test_eof577;
case 577:
#line 8745 "build/lm048lib.c"
	_ps = 577;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr726;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr726;
	} else
		goto tr726;
	goto tr0;
tr726:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st578;
st578:
	if ( ++p == pe )
		goto _test_eof578;
case 578:
#line 8764 "build/lm048lib.c"
	_ps = 578;
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr727;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr727;
	} else
		goto tr727;
	goto tr0;
tr727:
#line 147 "build/lm048lib.rl"
	{ payload_add(pkt, *p); }
	goto st579;
st579:
	if ( ++p == pe )
		goto _test_eof579;
case 579:
#line 8783 "build/lm048lib.c"
	_ps = 579;
	if ( (*p) == 34 )
		goto st580;
	goto tr0;
st580:
	if ( ++p == pe )
		goto _test_eof580;
case 580:
	_ps = 580;
	if ( (*p) == 13 )
		goto st581;
	goto tr0;
st581:
	if ( ++p == pe )
		goto _test_eof581;
case 581:
	_ps = 581;
	if ( (*p) == 10 )
		goto tr730;
	goto tr0;
	}
	_test_eof1:  state->cs = 1; goto _test_eof; 
	_test_eof2:  state->cs = 2; goto _test_eof; 
	_test_eof3:  state->cs = 3; goto _test_eof; 
	_test_eof4:  state->cs = 4; goto _test_eof; 
	_test_eof5:  state->cs = 5; goto _test_eof; 
	_test_eof6:  state->cs = 6; goto _test_eof; 
	_test_eof7:  state->cs = 7; goto _test_eof; 
	_test_eof8:  state->cs = 8; goto _test_eof; 
	_test_eof9:  state->cs = 9; goto _test_eof; 
	_test_eof10:  state->cs = 10; goto _test_eof; 
	_test_eof11:  state->cs = 11; goto _test_eof; 
	_test_eof12:  state->cs = 12; goto _test_eof; 
	_test_eof582:  state->cs = 582; goto _test_eof; 
	_test_eof13:  state->cs = 13; goto _test_eof; 
	_test_eof14:  state->cs = 14; goto _test_eof; 
	_test_eof15:  state->cs = 15; goto _test_eof; 
	_test_eof16:  state->cs = 16; goto _test_eof; 
	_test_eof17:  state->cs = 17; goto _test_eof; 
	_test_eof18:  state->cs = 18; goto _test_eof; 
	_test_eof19:  state->cs = 19; goto _test_eof; 
	_test_eof20:  state->cs = 20; goto _test_eof; 
	_test_eof21:  state->cs = 21; goto _test_eof; 
	_test_eof22:  state->cs = 22; goto _test_eof; 
	_test_eof23:  state->cs = 23; goto _test_eof; 
	_test_eof24:  state->cs = 24; goto _test_eof; 
	_test_eof25:  state->cs = 25; goto _test_eof; 
	_test_eof26:  state->cs = 26; goto _test_eof; 
	_test_eof27:  state->cs = 27; goto _test_eof; 
	_test_eof28:  state->cs = 28; goto _test_eof; 
	_test_eof29:  state->cs = 29; goto _test_eof; 
	_test_eof30:  state->cs = 30; goto _test_eof; 
	_test_eof31:  state->cs = 31; goto _test_eof; 
	_test_eof32:  state->cs = 32; goto _test_eof; 
	_test_eof33:  state->cs = 33; goto _test_eof; 
	_test_eof34:  state->cs = 34; goto _test_eof; 
	_test_eof35:  state->cs = 35; goto _test_eof; 
	_test_eof36:  state->cs = 36; goto _test_eof; 
	_test_eof37:  state->cs = 37; goto _test_eof; 
	_test_eof38:  state->cs = 38; goto _test_eof; 
	_test_eof39:  state->cs = 39; goto _test_eof; 
	_test_eof40:  state->cs = 40; goto _test_eof; 
	_test_eof41:  state->cs = 41; goto _test_eof; 
	_test_eof42:  state->cs = 42; goto _test_eof; 
	_test_eof43:  state->cs = 43; goto _test_eof; 
	_test_eof44:  state->cs = 44; goto _test_eof; 
	_test_eof45:  state->cs = 45; goto _test_eof; 
	_test_eof46:  state->cs = 46; goto _test_eof; 
	_test_eof47:  state->cs = 47; goto _test_eof; 
	_test_eof48:  state->cs = 48; goto _test_eof; 
	_test_eof49:  state->cs = 49; goto _test_eof; 
	_test_eof50:  state->cs = 50; goto _test_eof; 
	_test_eof51:  state->cs = 51; goto _test_eof; 
	_test_eof52:  state->cs = 52; goto _test_eof; 
	_test_eof53:  state->cs = 53; goto _test_eof; 
	_test_eof54:  state->cs = 54; goto _test_eof; 
	_test_eof55:  state->cs = 55; goto _test_eof; 
	_test_eof56:  state->cs = 56; goto _test_eof; 
	_test_eof57:  state->cs = 57; goto _test_eof; 
	_test_eof58:  state->cs = 58; goto _test_eof; 
	_test_eof59:  state->cs = 59; goto _test_eof; 
	_test_eof60:  state->cs = 60; goto _test_eof; 
	_test_eof61:  state->cs = 61; goto _test_eof; 
	_test_eof62:  state->cs = 62; goto _test_eof; 
	_test_eof63:  state->cs = 63; goto _test_eof; 
	_test_eof64:  state->cs = 64; goto _test_eof; 
	_test_eof65:  state->cs = 65; goto _test_eof; 
	_test_eof66:  state->cs = 66; goto _test_eof; 
	_test_eof67:  state->cs = 67; goto _test_eof; 
	_test_eof68:  state->cs = 68; goto _test_eof; 
	_test_eof69:  state->cs = 69; goto _test_eof; 
	_test_eof70:  state->cs = 70; goto _test_eof; 
	_test_eof71:  state->cs = 71; goto _test_eof; 
	_test_eof72:  state->cs = 72; goto _test_eof; 
	_test_eof73:  state->cs = 73; goto _test_eof; 
	_test_eof74:  state->cs = 74; goto _test_eof; 
	_test_eof75:  state->cs = 75; goto _test_eof; 
	_test_eof76:  state->cs = 76; goto _test_eof; 
	_test_eof77:  state->cs = 77; goto _test_eof; 
	_test_eof78:  state->cs = 78; goto _test_eof; 
	_test_eof79:  state->cs = 79; goto _test_eof; 
	_test_eof80:  state->cs = 80; goto _test_eof; 
	_test_eof81:  state->cs = 81; goto _test_eof; 
	_test_eof82:  state->cs = 82; goto _test_eof; 
	_test_eof83:  state->cs = 83; goto _test_eof; 
	_test_eof84:  state->cs = 84; goto _test_eof; 
	_test_eof85:  state->cs = 85; goto _test_eof; 
	_test_eof86:  state->cs = 86; goto _test_eof; 
	_test_eof87:  state->cs = 87; goto _test_eof; 
	_test_eof88:  state->cs = 88; goto _test_eof; 
	_test_eof89:  state->cs = 89; goto _test_eof; 
	_test_eof90:  state->cs = 90; goto _test_eof; 
	_test_eof91:  state->cs = 91; goto _test_eof; 
	_test_eof92:  state->cs = 92; goto _test_eof; 
	_test_eof93:  state->cs = 93; goto _test_eof; 
	_test_eof94:  state->cs = 94; goto _test_eof; 
	_test_eof95:  state->cs = 95; goto _test_eof; 
	_test_eof96:  state->cs = 96; goto _test_eof; 
	_test_eof97:  state->cs = 97; goto _test_eof; 
	_test_eof98:  state->cs = 98; goto _test_eof; 
	_test_eof99:  state->cs = 99; goto _test_eof; 
	_test_eof100:  state->cs = 100; goto _test_eof; 
	_test_eof101:  state->cs = 101; goto _test_eof; 
	_test_eof102:  state->cs = 102; goto _test_eof; 
	_test_eof103:  state->cs = 103; goto _test_eof; 
	_test_eof104:  state->cs = 104; goto _test_eof; 
	_test_eof105:  state->cs = 105; goto _test_eof; 
	_test_eof106:  state->cs = 106; goto _test_eof; 
	_test_eof107:  state->cs = 107; goto _test_eof; 
	_test_eof108:  state->cs = 108; goto _test_eof; 
	_test_eof109:  state->cs = 109; goto _test_eof; 
	_test_eof110:  state->cs = 110; goto _test_eof; 
	_test_eof111:  state->cs = 111; goto _test_eof; 
	_test_eof112:  state->cs = 112; goto _test_eof; 
	_test_eof113:  state->cs = 113; goto _test_eof; 
	_test_eof114:  state->cs = 114; goto _test_eof; 
	_test_eof115:  state->cs = 115; goto _test_eof; 
	_test_eof116:  state->cs = 116; goto _test_eof; 
	_test_eof117:  state->cs = 117; goto _test_eof; 
	_test_eof118:  state->cs = 118; goto _test_eof; 
	_test_eof119:  state->cs = 119; goto _test_eof; 
	_test_eof120:  state->cs = 120; goto _test_eof; 
	_test_eof121:  state->cs = 121; goto _test_eof; 
	_test_eof122:  state->cs = 122; goto _test_eof; 
	_test_eof123:  state->cs = 123; goto _test_eof; 
	_test_eof124:  state->cs = 124; goto _test_eof; 
	_test_eof125:  state->cs = 125; goto _test_eof; 
	_test_eof126:  state->cs = 126; goto _test_eof; 
	_test_eof127:  state->cs = 127; goto _test_eof; 
	_test_eof128:  state->cs = 128; goto _test_eof; 
	_test_eof129:  state->cs = 129; goto _test_eof; 
	_test_eof130:  state->cs = 130; goto _test_eof; 
	_test_eof131:  state->cs = 131; goto _test_eof; 
	_test_eof132:  state->cs = 132; goto _test_eof; 
	_test_eof133:  state->cs = 133; goto _test_eof; 
	_test_eof134:  state->cs = 134; goto _test_eof; 
	_test_eof135:  state->cs = 135; goto _test_eof; 
	_test_eof136:  state->cs = 136; goto _test_eof; 
	_test_eof137:  state->cs = 137; goto _test_eof; 
	_test_eof138:  state->cs = 138; goto _test_eof; 
	_test_eof139:  state->cs = 139; goto _test_eof; 
	_test_eof140:  state->cs = 140; goto _test_eof; 
	_test_eof141:  state->cs = 141; goto _test_eof; 
	_test_eof142:  state->cs = 142; goto _test_eof; 
	_test_eof143:  state->cs = 143; goto _test_eof; 
	_test_eof144:  state->cs = 144; goto _test_eof; 
	_test_eof145:  state->cs = 145; goto _test_eof; 
	_test_eof146:  state->cs = 146; goto _test_eof; 
	_test_eof147:  state->cs = 147; goto _test_eof; 
	_test_eof148:  state->cs = 148; goto _test_eof; 
	_test_eof149:  state->cs = 149; goto _test_eof; 
	_test_eof150:  state->cs = 150; goto _test_eof; 
	_test_eof151:  state->cs = 151; goto _test_eof; 
	_test_eof152:  state->cs = 152; goto _test_eof; 
	_test_eof153:  state->cs = 153; goto _test_eof; 
	_test_eof154:  state->cs = 154; goto _test_eof; 
	_test_eof155:  state->cs = 155; goto _test_eof; 
	_test_eof156:  state->cs = 156; goto _test_eof; 
	_test_eof157:  state->cs = 157; goto _test_eof; 
	_test_eof158:  state->cs = 158; goto _test_eof; 
	_test_eof159:  state->cs = 159; goto _test_eof; 
	_test_eof160:  state->cs = 160; goto _test_eof; 
	_test_eof161:  state->cs = 161; goto _test_eof; 
	_test_eof162:  state->cs = 162; goto _test_eof; 
	_test_eof163:  state->cs = 163; goto _test_eof; 
	_test_eof164:  state->cs = 164; goto _test_eof; 
	_test_eof165:  state->cs = 165; goto _test_eof; 
	_test_eof166:  state->cs = 166; goto _test_eof; 
	_test_eof167:  state->cs = 167; goto _test_eof; 
	_test_eof168:  state->cs = 168; goto _test_eof; 
	_test_eof169:  state->cs = 169; goto _test_eof; 
	_test_eof170:  state->cs = 170; goto _test_eof; 
	_test_eof171:  state->cs = 171; goto _test_eof; 
	_test_eof172:  state->cs = 172; goto _test_eof; 
	_test_eof173:  state->cs = 173; goto _test_eof; 
	_test_eof174:  state->cs = 174; goto _test_eof; 
	_test_eof175:  state->cs = 175; goto _test_eof; 
	_test_eof176:  state->cs = 176; goto _test_eof; 
	_test_eof177:  state->cs = 177; goto _test_eof; 
	_test_eof178:  state->cs = 178; goto _test_eof; 
	_test_eof179:  state->cs = 179; goto _test_eof; 
	_test_eof180:  state->cs = 180; goto _test_eof; 
	_test_eof181:  state->cs = 181; goto _test_eof; 
	_test_eof182:  state->cs = 182; goto _test_eof; 
	_test_eof183:  state->cs = 183; goto _test_eof; 
	_test_eof184:  state->cs = 184; goto _test_eof; 
	_test_eof185:  state->cs = 185; goto _test_eof; 
	_test_eof186:  state->cs = 186; goto _test_eof; 
	_test_eof187:  state->cs = 187; goto _test_eof; 
	_test_eof188:  state->cs = 188; goto _test_eof; 
	_test_eof189:  state->cs = 189; goto _test_eof; 
	_test_eof190:  state->cs = 190; goto _test_eof; 
	_test_eof191:  state->cs = 191; goto _test_eof; 
	_test_eof192:  state->cs = 192; goto _test_eof; 
	_test_eof193:  state->cs = 193; goto _test_eof; 
	_test_eof194:  state->cs = 194; goto _test_eof; 
	_test_eof195:  state->cs = 195; goto _test_eof; 
	_test_eof196:  state->cs = 196; goto _test_eof; 
	_test_eof197:  state->cs = 197; goto _test_eof; 
	_test_eof198:  state->cs = 198; goto _test_eof; 
	_test_eof199:  state->cs = 199; goto _test_eof; 
	_test_eof200:  state->cs = 200; goto _test_eof; 
	_test_eof201:  state->cs = 201; goto _test_eof; 
	_test_eof202:  state->cs = 202; goto _test_eof; 
	_test_eof203:  state->cs = 203; goto _test_eof; 
	_test_eof204:  state->cs = 204; goto _test_eof; 
	_test_eof205:  state->cs = 205; goto _test_eof; 
	_test_eof206:  state->cs = 206; goto _test_eof; 
	_test_eof207:  state->cs = 207; goto _test_eof; 
	_test_eof208:  state->cs = 208; goto _test_eof; 
	_test_eof209:  state->cs = 209; goto _test_eof; 
	_test_eof210:  state->cs = 210; goto _test_eof; 
	_test_eof211:  state->cs = 211; goto _test_eof; 
	_test_eof212:  state->cs = 212; goto _test_eof; 
	_test_eof213:  state->cs = 213; goto _test_eof; 
	_test_eof214:  state->cs = 214; goto _test_eof; 
	_test_eof215:  state->cs = 215; goto _test_eof; 
	_test_eof216:  state->cs = 216; goto _test_eof; 
	_test_eof217:  state->cs = 217; goto _test_eof; 
	_test_eof218:  state->cs = 218; goto _test_eof; 
	_test_eof219:  state->cs = 219; goto _test_eof; 
	_test_eof220:  state->cs = 220; goto _test_eof; 
	_test_eof221:  state->cs = 221; goto _test_eof; 
	_test_eof222:  state->cs = 222; goto _test_eof; 
	_test_eof223:  state->cs = 223; goto _test_eof; 
	_test_eof224:  state->cs = 224; goto _test_eof; 
	_test_eof225:  state->cs = 225; goto _test_eof; 
	_test_eof226:  state->cs = 226; goto _test_eof; 
	_test_eof227:  state->cs = 227; goto _test_eof; 
	_test_eof228:  state->cs = 228; goto _test_eof; 
	_test_eof229:  state->cs = 229; goto _test_eof; 
	_test_eof230:  state->cs = 230; goto _test_eof; 
	_test_eof231:  state->cs = 231; goto _test_eof; 
	_test_eof232:  state->cs = 232; goto _test_eof; 
	_test_eof233:  state->cs = 233; goto _test_eof; 
	_test_eof234:  state->cs = 234; goto _test_eof; 
	_test_eof235:  state->cs = 235; goto _test_eof; 
	_test_eof236:  state->cs = 236; goto _test_eof; 
	_test_eof237:  state->cs = 237; goto _test_eof; 
	_test_eof238:  state->cs = 238; goto _test_eof; 
	_test_eof239:  state->cs = 239; goto _test_eof; 
	_test_eof240:  state->cs = 240; goto _test_eof; 
	_test_eof241:  state->cs = 241; goto _test_eof; 
	_test_eof242:  state->cs = 242; goto _test_eof; 
	_test_eof243:  state->cs = 243; goto _test_eof; 
	_test_eof244:  state->cs = 244; goto _test_eof; 
	_test_eof245:  state->cs = 245; goto _test_eof; 
	_test_eof246:  state->cs = 246; goto _test_eof; 
	_test_eof247:  state->cs = 247; goto _test_eof; 
	_test_eof248:  state->cs = 248; goto _test_eof; 
	_test_eof249:  state->cs = 249; goto _test_eof; 
	_test_eof250:  state->cs = 250; goto _test_eof; 
	_test_eof251:  state->cs = 251; goto _test_eof; 
	_test_eof252:  state->cs = 252; goto _test_eof; 
	_test_eof253:  state->cs = 253; goto _test_eof; 
	_test_eof254:  state->cs = 254; goto _test_eof; 
	_test_eof255:  state->cs = 255; goto _test_eof; 
	_test_eof256:  state->cs = 256; goto _test_eof; 
	_test_eof257:  state->cs = 257; goto _test_eof; 
	_test_eof258:  state->cs = 258; goto _test_eof; 
	_test_eof259:  state->cs = 259; goto _test_eof; 
	_test_eof260:  state->cs = 260; goto _test_eof; 
	_test_eof261:  state->cs = 261; goto _test_eof; 
	_test_eof262:  state->cs = 262; goto _test_eof; 
	_test_eof263:  state->cs = 263; goto _test_eof; 
	_test_eof264:  state->cs = 264; goto _test_eof; 
	_test_eof265:  state->cs = 265; goto _test_eof; 
	_test_eof266:  state->cs = 266; goto _test_eof; 
	_test_eof267:  state->cs = 267; goto _test_eof; 
	_test_eof268:  state->cs = 268; goto _test_eof; 
	_test_eof269:  state->cs = 269; goto _test_eof; 
	_test_eof270:  state->cs = 270; goto _test_eof; 
	_test_eof271:  state->cs = 271; goto _test_eof; 
	_test_eof272:  state->cs = 272; goto _test_eof; 
	_test_eof273:  state->cs = 273; goto _test_eof; 
	_test_eof274:  state->cs = 274; goto _test_eof; 
	_test_eof275:  state->cs = 275; goto _test_eof; 
	_test_eof276:  state->cs = 276; goto _test_eof; 
	_test_eof277:  state->cs = 277; goto _test_eof; 
	_test_eof278:  state->cs = 278; goto _test_eof; 
	_test_eof279:  state->cs = 279; goto _test_eof; 
	_test_eof280:  state->cs = 280; goto _test_eof; 
	_test_eof281:  state->cs = 281; goto _test_eof; 
	_test_eof282:  state->cs = 282; goto _test_eof; 
	_test_eof283:  state->cs = 283; goto _test_eof; 
	_test_eof284:  state->cs = 284; goto _test_eof; 
	_test_eof285:  state->cs = 285; goto _test_eof; 
	_test_eof286:  state->cs = 286; goto _test_eof; 
	_test_eof287:  state->cs = 287; goto _test_eof; 
	_test_eof288:  state->cs = 288; goto _test_eof; 
	_test_eof289:  state->cs = 289; goto _test_eof; 
	_test_eof290:  state->cs = 290; goto _test_eof; 
	_test_eof291:  state->cs = 291; goto _test_eof; 
	_test_eof292:  state->cs = 292; goto _test_eof; 
	_test_eof293:  state->cs = 293; goto _test_eof; 
	_test_eof294:  state->cs = 294; goto _test_eof; 
	_test_eof295:  state->cs = 295; goto _test_eof; 
	_test_eof296:  state->cs = 296; goto _test_eof; 
	_test_eof297:  state->cs = 297; goto _test_eof; 
	_test_eof298:  state->cs = 298; goto _test_eof; 
	_test_eof299:  state->cs = 299; goto _test_eof; 
	_test_eof300:  state->cs = 300; goto _test_eof; 
	_test_eof301:  state->cs = 301; goto _test_eof; 
	_test_eof302:  state->cs = 302; goto _test_eof; 
	_test_eof303:  state->cs = 303; goto _test_eof; 
	_test_eof304:  state->cs = 304; goto _test_eof; 
	_test_eof305:  state->cs = 305; goto _test_eof; 
	_test_eof306:  state->cs = 306; goto _test_eof; 
	_test_eof307:  state->cs = 307; goto _test_eof; 
	_test_eof308:  state->cs = 308; goto _test_eof; 
	_test_eof309:  state->cs = 309; goto _test_eof; 
	_test_eof310:  state->cs = 310; goto _test_eof; 
	_test_eof311:  state->cs = 311; goto _test_eof; 
	_test_eof312:  state->cs = 312; goto _test_eof; 
	_test_eof313:  state->cs = 313; goto _test_eof; 
	_test_eof314:  state->cs = 314; goto _test_eof; 
	_test_eof315:  state->cs = 315; goto _test_eof; 
	_test_eof316:  state->cs = 316; goto _test_eof; 
	_test_eof317:  state->cs = 317; goto _test_eof; 
	_test_eof318:  state->cs = 318; goto _test_eof; 
	_test_eof319:  state->cs = 319; goto _test_eof; 
	_test_eof320:  state->cs = 320; goto _test_eof; 
	_test_eof321:  state->cs = 321; goto _test_eof; 
	_test_eof322:  state->cs = 322; goto _test_eof; 
	_test_eof323:  state->cs = 323; goto _test_eof; 
	_test_eof324:  state->cs = 324; goto _test_eof; 
	_test_eof325:  state->cs = 325; goto _test_eof; 
	_test_eof326:  state->cs = 326; goto _test_eof; 
	_test_eof327:  state->cs = 327; goto _test_eof; 
	_test_eof328:  state->cs = 328; goto _test_eof; 
	_test_eof329:  state->cs = 329; goto _test_eof; 
	_test_eof330:  state->cs = 330; goto _test_eof; 
	_test_eof331:  state->cs = 331; goto _test_eof; 
	_test_eof332:  state->cs = 332; goto _test_eof; 
	_test_eof333:  state->cs = 333; goto _test_eof; 
	_test_eof334:  state->cs = 334; goto _test_eof; 
	_test_eof335:  state->cs = 335; goto _test_eof; 
	_test_eof336:  state->cs = 336; goto _test_eof; 
	_test_eof337:  state->cs = 337; goto _test_eof; 
	_test_eof338:  state->cs = 338; goto _test_eof; 
	_test_eof339:  state->cs = 339; goto _test_eof; 
	_test_eof340:  state->cs = 340; goto _test_eof; 
	_test_eof341:  state->cs = 341; goto _test_eof; 
	_test_eof342:  state->cs = 342; goto _test_eof; 
	_test_eof343:  state->cs = 343; goto _test_eof; 
	_test_eof344:  state->cs = 344; goto _test_eof; 
	_test_eof345:  state->cs = 345; goto _test_eof; 
	_test_eof346:  state->cs = 346; goto _test_eof; 
	_test_eof347:  state->cs = 347; goto _test_eof; 
	_test_eof348:  state->cs = 348; goto _test_eof; 
	_test_eof349:  state->cs = 349; goto _test_eof; 
	_test_eof350:  state->cs = 350; goto _test_eof; 
	_test_eof351:  state->cs = 351; goto _test_eof; 
	_test_eof352:  state->cs = 352; goto _test_eof; 
	_test_eof353:  state->cs = 353; goto _test_eof; 
	_test_eof354:  state->cs = 354; goto _test_eof; 
	_test_eof355:  state->cs = 355; goto _test_eof; 
	_test_eof356:  state->cs = 356; goto _test_eof; 
	_test_eof357:  state->cs = 357; goto _test_eof; 
	_test_eof358:  state->cs = 358; goto _test_eof; 
	_test_eof359:  state->cs = 359; goto _test_eof; 
	_test_eof360:  state->cs = 360; goto _test_eof; 
	_test_eof361:  state->cs = 361; goto _test_eof; 
	_test_eof362:  state->cs = 362; goto _test_eof; 
	_test_eof363:  state->cs = 363; goto _test_eof; 
	_test_eof364:  state->cs = 364; goto _test_eof; 
	_test_eof365:  state->cs = 365; goto _test_eof; 
	_test_eof366:  state->cs = 366; goto _test_eof; 
	_test_eof367:  state->cs = 367; goto _test_eof; 
	_test_eof368:  state->cs = 368; goto _test_eof; 
	_test_eof369:  state->cs = 369; goto _test_eof; 
	_test_eof370:  state->cs = 370; goto _test_eof; 
	_test_eof371:  state->cs = 371; goto _test_eof; 
	_test_eof372:  state->cs = 372; goto _test_eof; 
	_test_eof373:  state->cs = 373; goto _test_eof; 
	_test_eof374:  state->cs = 374; goto _test_eof; 
	_test_eof375:  state->cs = 375; goto _test_eof; 
	_test_eof376:  state->cs = 376; goto _test_eof; 
	_test_eof377:  state->cs = 377; goto _test_eof; 
	_test_eof378:  state->cs = 378; goto _test_eof; 
	_test_eof379:  state->cs = 379; goto _test_eof; 
	_test_eof380:  state->cs = 380; goto _test_eof; 
	_test_eof381:  state->cs = 381; goto _test_eof; 
	_test_eof382:  state->cs = 382; goto _test_eof; 
	_test_eof383:  state->cs = 383; goto _test_eof; 
	_test_eof384:  state->cs = 384; goto _test_eof; 
	_test_eof385:  state->cs = 385; goto _test_eof; 
	_test_eof386:  state->cs = 386; goto _test_eof; 
	_test_eof387:  state->cs = 387; goto _test_eof; 
	_test_eof388:  state->cs = 388; goto _test_eof; 
	_test_eof389:  state->cs = 389; goto _test_eof; 
	_test_eof390:  state->cs = 390; goto _test_eof; 
	_test_eof391:  state->cs = 391; goto _test_eof; 
	_test_eof392:  state->cs = 392; goto _test_eof; 
	_test_eof393:  state->cs = 393; goto _test_eof; 
	_test_eof394:  state->cs = 394; goto _test_eof; 
	_test_eof395:  state->cs = 395; goto _test_eof; 
	_test_eof396:  state->cs = 396; goto _test_eof; 
	_test_eof397:  state->cs = 397; goto _test_eof; 
	_test_eof398:  state->cs = 398; goto _test_eof; 
	_test_eof399:  state->cs = 399; goto _test_eof; 
	_test_eof400:  state->cs = 400; goto _test_eof; 
	_test_eof401:  state->cs = 401; goto _test_eof; 
	_test_eof402:  state->cs = 402; goto _test_eof; 
	_test_eof403:  state->cs = 403; goto _test_eof; 
	_test_eof404:  state->cs = 404; goto _test_eof; 
	_test_eof405:  state->cs = 405; goto _test_eof; 
	_test_eof406:  state->cs = 406; goto _test_eof; 
	_test_eof407:  state->cs = 407; goto _test_eof; 
	_test_eof408:  state->cs = 408; goto _test_eof; 
	_test_eof409:  state->cs = 409; goto _test_eof; 
	_test_eof410:  state->cs = 410; goto _test_eof; 
	_test_eof411:  state->cs = 411; goto _test_eof; 
	_test_eof412:  state->cs = 412; goto _test_eof; 
	_test_eof413:  state->cs = 413; goto _test_eof; 
	_test_eof414:  state->cs = 414; goto _test_eof; 
	_test_eof415:  state->cs = 415; goto _test_eof; 
	_test_eof416:  state->cs = 416; goto _test_eof; 
	_test_eof417:  state->cs = 417; goto _test_eof; 
	_test_eof418:  state->cs = 418; goto _test_eof; 
	_test_eof419:  state->cs = 419; goto _test_eof; 
	_test_eof420:  state->cs = 420; goto _test_eof; 
	_test_eof421:  state->cs = 421; goto _test_eof; 
	_test_eof422:  state->cs = 422; goto _test_eof; 
	_test_eof423:  state->cs = 423; goto _test_eof; 
	_test_eof424:  state->cs = 424; goto _test_eof; 
	_test_eof425:  state->cs = 425; goto _test_eof; 
	_test_eof426:  state->cs = 426; goto _test_eof; 
	_test_eof427:  state->cs = 427; goto _test_eof; 
	_test_eof428:  state->cs = 428; goto _test_eof; 
	_test_eof429:  state->cs = 429; goto _test_eof; 
	_test_eof430:  state->cs = 430; goto _test_eof; 
	_test_eof431:  state->cs = 431; goto _test_eof; 
	_test_eof432:  state->cs = 432; goto _test_eof; 
	_test_eof433:  state->cs = 433; goto _test_eof; 
	_test_eof434:  state->cs = 434; goto _test_eof; 
	_test_eof435:  state->cs = 435; goto _test_eof; 
	_test_eof436:  state->cs = 436; goto _test_eof; 
	_test_eof437:  state->cs = 437; goto _test_eof; 
	_test_eof438:  state->cs = 438; goto _test_eof; 
	_test_eof439:  state->cs = 439; goto _test_eof; 
	_test_eof440:  state->cs = 440; goto _test_eof; 
	_test_eof441:  state->cs = 441; goto _test_eof; 
	_test_eof442:  state->cs = 442; goto _test_eof; 
	_test_eof443:  state->cs = 443; goto _test_eof; 
	_test_eof583:  state->cs = 583; goto _test_eof; 
	_test_eof444:  state->cs = 444; goto _test_eof; 
	_test_eof445:  state->cs = 445; goto _test_eof; 
	_test_eof446:  state->cs = 446; goto _test_eof; 
	_test_eof447:  state->cs = 447; goto _test_eof; 
	_test_eof448:  state->cs = 448; goto _test_eof; 
	_test_eof449:  state->cs = 449; goto _test_eof; 
	_test_eof450:  state->cs = 450; goto _test_eof; 
	_test_eof451:  state->cs = 451; goto _test_eof; 
	_test_eof452:  state->cs = 452; goto _test_eof; 
	_test_eof453:  state->cs = 453; goto _test_eof; 
	_test_eof454:  state->cs = 454; goto _test_eof; 
	_test_eof455:  state->cs = 455; goto _test_eof; 
	_test_eof456:  state->cs = 456; goto _test_eof; 
	_test_eof457:  state->cs = 457; goto _test_eof; 
	_test_eof458:  state->cs = 458; goto _test_eof; 
	_test_eof459:  state->cs = 459; goto _test_eof; 
	_test_eof460:  state->cs = 460; goto _test_eof; 
	_test_eof461:  state->cs = 461; goto _test_eof; 
	_test_eof462:  state->cs = 462; goto _test_eof; 
	_test_eof463:  state->cs = 463; goto _test_eof; 
	_test_eof464:  state->cs = 464; goto _test_eof; 
	_test_eof465:  state->cs = 465; goto _test_eof; 
	_test_eof466:  state->cs = 466; goto _test_eof; 
	_test_eof467:  state->cs = 467; goto _test_eof; 
	_test_eof468:  state->cs = 468; goto _test_eof; 
	_test_eof469:  state->cs = 469; goto _test_eof; 
	_test_eof470:  state->cs = 470; goto _test_eof; 
	_test_eof584:  state->cs = 584; goto _test_eof; 
	_test_eof471:  state->cs = 471; goto _test_eof; 
	_test_eof472:  state->cs = 472; goto _test_eof; 
	_test_eof473:  state->cs = 473; goto _test_eof; 
	_test_eof474:  state->cs = 474; goto _test_eof; 
	_test_eof475:  state->cs = 475; goto _test_eof; 
	_test_eof476:  state->cs = 476; goto _test_eof; 
	_test_eof477:  state->cs = 477; goto _test_eof; 
	_test_eof478:  state->cs = 478; goto _test_eof; 
	_test_eof479:  state->cs = 479; goto _test_eof; 
	_test_eof480:  state->cs = 480; goto _test_eof; 
	_test_eof481:  state->cs = 481; goto _test_eof; 
	_test_eof482:  state->cs = 482; goto _test_eof; 
	_test_eof483:  state->cs = 483; goto _test_eof; 
	_test_eof484:  state->cs = 484; goto _test_eof; 
	_test_eof485:  state->cs = 485; goto _test_eof; 
	_test_eof486:  state->cs = 486; goto _test_eof; 
	_test_eof487:  state->cs = 487; goto _test_eof; 
	_test_eof488:  state->cs = 488; goto _test_eof; 
	_test_eof489:  state->cs = 489; goto _test_eof; 
	_test_eof490:  state->cs = 490; goto _test_eof; 
	_test_eof491:  state->cs = 491; goto _test_eof; 
	_test_eof492:  state->cs = 492; goto _test_eof; 
	_test_eof493:  state->cs = 493; goto _test_eof; 
	_test_eof494:  state->cs = 494; goto _test_eof; 
	_test_eof495:  state->cs = 495; goto _test_eof; 
	_test_eof496:  state->cs = 496; goto _test_eof; 
	_test_eof497:  state->cs = 497; goto _test_eof; 
	_test_eof498:  state->cs = 498; goto _test_eof; 
	_test_eof499:  state->cs = 499; goto _test_eof; 
	_test_eof500:  state->cs = 500; goto _test_eof; 
	_test_eof501:  state->cs = 501; goto _test_eof; 
	_test_eof502:  state->cs = 502; goto _test_eof; 
	_test_eof503:  state->cs = 503; goto _test_eof; 
	_test_eof504:  state->cs = 504; goto _test_eof; 
	_test_eof505:  state->cs = 505; goto _test_eof; 
	_test_eof506:  state->cs = 506; goto _test_eof; 
	_test_eof507:  state->cs = 507; goto _test_eof; 
	_test_eof508:  state->cs = 508; goto _test_eof; 
	_test_eof509:  state->cs = 509; goto _test_eof; 
	_test_eof510:  state->cs = 510; goto _test_eof; 
	_test_eof511:  state->cs = 511; goto _test_eof; 
	_test_eof512:  state->cs = 512; goto _test_eof; 
	_test_eof513:  state->cs = 513; goto _test_eof; 
	_test_eof514:  state->cs = 514; goto _test_eof; 
	_test_eof515:  state->cs = 515; goto _test_eof; 
	_test_eof516:  state->cs = 516; goto _test_eof; 
	_test_eof517:  state->cs = 517; goto _test_eof; 
	_test_eof518:  state->cs = 518; goto _test_eof; 
	_test_eof519:  state->cs = 519; goto _test_eof; 
	_test_eof520:  state->cs = 520; goto _test_eof; 
	_test_eof521:  state->cs = 521; goto _test_eof; 
	_test_eof522:  state->cs = 522; goto _test_eof; 
	_test_eof523:  state->cs = 523; goto _test_eof; 
	_test_eof524:  state->cs = 524; goto _test_eof; 
	_test_eof525:  state->cs = 525; goto _test_eof; 
	_test_eof526:  state->cs = 526; goto _test_eof; 
	_test_eof527:  state->cs = 527; goto _test_eof; 
	_test_eof528:  state->cs = 528; goto _test_eof; 
	_test_eof529:  state->cs = 529; goto _test_eof; 
	_test_eof530:  state->cs = 530; goto _test_eof; 
	_test_eof531:  state->cs = 531; goto _test_eof; 
	_test_eof532:  state->cs = 532; goto _test_eof; 
	_test_eof533:  state->cs = 533; goto _test_eof; 
	_test_eof534:  state->cs = 534; goto _test_eof; 
	_test_eof535:  state->cs = 535; goto _test_eof; 
	_test_eof536:  state->cs = 536; goto _test_eof; 
	_test_eof537:  state->cs = 537; goto _test_eof; 
	_test_eof538:  state->cs = 538; goto _test_eof; 
	_test_eof539:  state->cs = 539; goto _test_eof; 
	_test_eof540:  state->cs = 540; goto _test_eof; 
	_test_eof541:  state->cs = 541; goto _test_eof; 
	_test_eof542:  state->cs = 542; goto _test_eof; 
	_test_eof543:  state->cs = 543; goto _test_eof; 
	_test_eof544:  state->cs = 544; goto _test_eof; 
	_test_eof545:  state->cs = 545; goto _test_eof; 
	_test_eof546:  state->cs = 546; goto _test_eof; 
	_test_eof547:  state->cs = 547; goto _test_eof; 
	_test_eof548:  state->cs = 548; goto _test_eof; 
	_test_eof549:  state->cs = 549; goto _test_eof; 
	_test_eof550:  state->cs = 550; goto _test_eof; 
	_test_eof551:  state->cs = 551; goto _test_eof; 
	_test_eof552:  state->cs = 552; goto _test_eof; 
	_test_eof553:  state->cs = 553; goto _test_eof; 
	_test_eof554:  state->cs = 554; goto _test_eof; 
	_test_eof555:  state->cs = 555; goto _test_eof; 
	_test_eof556:  state->cs = 556; goto _test_eof; 
	_test_eof557:  state->cs = 557; goto _test_eof; 
	_test_eof558:  state->cs = 558; goto _test_eof; 
	_test_eof559:  state->cs = 559; goto _test_eof; 
	_test_eof560:  state->cs = 560; goto _test_eof; 
	_test_eof561:  state->cs = 561; goto _test_eof; 
	_test_eof562:  state->cs = 562; goto _test_eof; 
	_test_eof563:  state->cs = 563; goto _test_eof; 
	_test_eof564:  state->cs = 564; goto _test_eof; 
	_test_eof565:  state->cs = 565; goto _test_eof; 
	_test_eof566:  state->cs = 566; goto _test_eof; 
	_test_eof567:  state->cs = 567; goto _test_eof; 
	_test_eof568:  state->cs = 568; goto _test_eof; 
	_test_eof569:  state->cs = 569; goto _test_eof; 
	_test_eof570:  state->cs = 570; goto _test_eof; 
	_test_eof571:  state->cs = 571; goto _test_eof; 
	_test_eof572:  state->cs = 572; goto _test_eof; 
	_test_eof573:  state->cs = 573; goto _test_eof; 
	_test_eof574:  state->cs = 574; goto _test_eof; 
	_test_eof575:  state->cs = 575; goto _test_eof; 
	_test_eof576:  state->cs = 576; goto _test_eof; 
	_test_eof577:  state->cs = 577; goto _test_eof; 
	_test_eof578:  state->cs = 578; goto _test_eof; 
	_test_eof579:  state->cs = 579; goto _test_eof; 
	_test_eof580:  state->cs = 580; goto _test_eof; 
	_test_eof581:  state->cs = 581; goto _test_eof; 

	_test_eof: {}
	if ( p == eof )
	{
	switch (  state->cs ) {
	case 1: 
	case 2: 
	case 3: 
	case 4: 
	case 5: 
	case 6: 
	case 7: 
	case 8: 
	case 9: 
	case 10: 
	case 11: 
	case 12: 
	case 13: 
	case 14: 
	case 15: 
	case 16: 
	case 17: 
	case 18: 
	case 19: 
	case 20: 
	case 21: 
	case 22: 
	case 23: 
	case 24: 
	case 25: 
	case 26: 
	case 27: 
	case 28: 
	case 29: 
	case 30: 
	case 31: 
	case 32: 
	case 33: 
	case 34: 
	case 35: 
	case 36: 
	case 37: 
	case 38: 
	case 39: 
	case 40: 
	case 41: 
	case 42: 
	case 43: 
	case 44: 
	case 45: 
	case 46: 
	case 47: 
	case 48: 
	case 49: 
	case 50: 
	case 51: 
	case 52: 
	case 53: 
	case 54: 
	case 55: 
	case 56: 
	case 57: 
	case 58: 
	case 59: 
	case 60: 
	case 61: 
	case 62: 
	case 63: 
	case 64: 
	case 65: 
	case 66: 
	case 67: 
	case 68: 
	case 69: 
	case 70: 
	case 71: 
	case 72: 
	case 73: 
	case 74: 
	case 75: 
	case 76: 
	case 77: 
	case 78: 
	case 79: 
	case 80: 
	case 81: 
	case 82: 
	case 83: 
	case 84: 
	case 85: 
	case 86: 
	case 87: 
	case 88: 
	case 89: 
	case 90: 
	case 91: 
	case 92: 
	case 93: 
	case 94: 
	case 95: 
	case 96: 
	case 97: 
	case 98: 
	case 99: 
	case 100: 
	case 101: 
	case 102: 
	case 103: 
	case 104: 
	case 105: 
	case 106: 
	case 107: 
	case 108: 
	case 109: 
	case 110: 
	case 111: 
	case 112: 
	case 113: 
	case 114: 
	case 115: 
	case 116: 
	case 117: 
	case 118: 
	case 119: 
	case 120: 
	case 121: 
	case 122: 
	case 123: 
	case 124: 
	case 125: 
	case 126: 
	case 127: 
	case 128: 
	case 129: 
	case 130: 
	case 131: 
	case 132: 
	case 133: 
	case 134: 
	case 135: 
	case 136: 
	case 137: 
	case 138: 
	case 139: 
	case 140: 
	case 141: 
	case 142: 
	case 143: 
	case 144: 
	case 145: 
	case 146: 
	case 147: 
	case 148: 
	case 149: 
	case 150: 
	case 151: 
	case 152: 
	case 153: 
	case 154: 
	case 155: 
	case 156: 
	case 157: 
	case 158: 
	case 159: 
	case 160: 
	case 161: 
	case 162: 
	case 163: 
	case 164: 
	case 165: 
	case 166: 
	case 167: 
	case 168: 
	case 169: 
	case 170: 
	case 171: 
	case 172: 
	case 173: 
	case 174: 
	case 175: 
	case 176: 
	case 177: 
	case 178: 
	case 179: 
	case 180: 
	case 181: 
	case 182: 
	case 183: 
	case 184: 
	case 185: 
	case 186: 
	case 187: 
	case 188: 
	case 189: 
	case 190: 
	case 191: 
	case 192: 
	case 193: 
	case 194: 
	case 195: 
	case 196: 
	case 197: 
	case 198: 
	case 199: 
	case 200: 
	case 201: 
	case 202: 
	case 203: 
	case 204: 
	case 205: 
	case 206: 
	case 207: 
	case 208: 
	case 209: 
	case 210: 
	case 211: 
	case 212: 
	case 213: 
	case 214: 
	case 215: 
	case 216: 
	case 217: 
	case 218: 
	case 219: 
	case 220: 
	case 221: 
	case 222: 
	case 223: 
	case 224: 
	case 225: 
	case 226: 
	case 227: 
	case 228: 
	case 229: 
	case 230: 
	case 231: 
	case 232: 
	case 233: 
	case 234: 
	case 235: 
	case 236: 
	case 237: 
	case 238: 
	case 239: 
	case 240: 
	case 241: 
	case 242: 
	case 243: 
	case 244: 
	case 245: 
	case 246: 
	case 247: 
	case 248: 
	case 249: 
	case 250: 
	case 251: 
	case 252: 
	case 253: 
	case 254: 
	case 255: 
	case 256: 
	case 257: 
	case 258: 
	case 259: 
	case 260: 
	case 261: 
	case 262: 
	case 263: 
	case 264: 
	case 265: 
	case 266: 
	case 267: 
	case 268: 
	case 269: 
	case 270: 
	case 271: 
	case 272: 
	case 273: 
	case 274: 
	case 275: 
	case 276: 
	case 277: 
	case 278: 
	case 279: 
	case 280: 
	case 281: 
	case 282: 
	case 283: 
	case 284: 
	case 285: 
	case 286: 
	case 287: 
	case 288: 
	case 289: 
	case 290: 
	case 291: 
	case 292: 
	case 293: 
	case 294: 
	case 295: 
	case 296: 
	case 297: 
	case 298: 
	case 299: 
	case 300: 
	case 301: 
	case 302: 
	case 303: 
	case 304: 
	case 305: 
	case 306: 
	case 307: 
	case 308: 
	case 309: 
	case 310: 
	case 311: 
	case 312: 
	case 313: 
	case 314: 
	case 315: 
	case 316: 
	case 317: 
	case 318: 
	case 319: 
	case 320: 
	case 321: 
	case 322: 
	case 323: 
	case 324: 
	case 325: 
	case 326: 
	case 327: 
	case 328: 
	case 329: 
	case 330: 
	case 331: 
	case 332: 
	case 333: 
	case 334: 
	case 335: 
	case 336: 
	case 337: 
	case 338: 
	case 339: 
	case 340: 
	case 341: 
	case 342: 
	case 343: 
	case 344: 
	case 345: 
	case 346: 
	case 347: 
	case 348: 
	case 349: 
	case 350: 
	case 351: 
	case 352: 
	case 353: 
	case 354: 
	case 355: 
	case 356: 
	case 357: 
	case 358: 
	case 359: 
	case 360: 
	case 361: 
	case 362: 
	case 363: 
	case 364: 
	case 365: 
	case 366: 
	case 367: 
	case 368: 
	case 369: 
	case 370: 
	case 371: 
	case 372: 
	case 373: 
	case 374: 
	case 375: 
	case 376: 
	case 377: 
	case 378: 
	case 379: 
	case 380: 
	case 381: 
	case 382: 
	case 383: 
	case 384: 
	case 385: 
	case 386: 
	case 387: 
	case 388: 
	case 389: 
	case 390: 
	case 391: 
	case 392: 
	case 393: 
	case 394: 
	case 395: 
	case 396: 
	case 397: 
	case 398: 
	case 399: 
	case 400: 
	case 401: 
	case 402: 
	case 403: 
	case 404: 
	case 405: 
	case 406: 
	case 407: 
	case 408: 
	case 409: 
	case 410: 
	case 411: 
	case 412: 
	case 413: 
	case 414: 
	case 415: 
	case 416: 
	case 417: 
	case 418: 
	case 419: 
	case 420: 
	case 421: 
	case 422: 
	case 423: 
	case 424: 
	case 425: 
	case 426: 
	case 427: 
	case 428: 
	case 429: 
	case 430: 
	case 431: 
	case 432: 
	case 433: 
	case 434: 
	case 435: 
	case 436: 
	case 437: 
	case 438: 
	case 439: 
	case 440: 
	case 441: 
	case 442: 
	case 443: 
	case 444: 
	case 445: 
	case 446: 
	case 447: 
	case 448: 
	case 449: 
	case 450: 
	case 451: 
	case 452: 
	case 453: 
	case 454: 
	case 455: 
	case 456: 
	case 457: 
	case 458: 
	case 459: 
	case 460: 
	case 461: 
	case 462: 
	case 463: 
	case 464: 
	case 465: 
	case 466: 
	case 467: 
	case 468: 
	case 469: 
	case 470: 
	case 471: 
	case 472: 
	case 473: 
	case 474: 
	case 475: 
	case 476: 
	case 477: 
	case 478: 
	case 479: 
	case 480: 
	case 481: 
	case 482: 
	case 483: 
	case 484: 
	case 485: 
	case 486: 
	case 487: 
	case 488: 
	case 489: 
	case 490: 
	case 491: 
	case 492: 
	case 493: 
	case 494: 
	case 495: 
	case 496: 
	case 497: 
	case 498: 
	case 499: 
	case 500: 
	case 501: 
	case 502: 
	case 503: 
	case 504: 
	case 505: 
	case 506: 
	case 507: 
	case 508: 
	case 509: 
	case 510: 
	case 511: 
	case 512: 
	case 513: 
	case 514: 
	case 515: 
	case 516: 
	case 517: 
	case 518: 
	case 519: 
	case 520: 
	case 521: 
	case 522: 
	case 523: 
	case 524: 
	case 525: 
	case 526: 
	case 527: 
	case 528: 
	case 529: 
	case 530: 
	case 531: 
	case 532: 
	case 533: 
	case 534: 
	case 535: 
	case 536: 
	case 537: 
	case 538: 
	case 539: 
	case 540: 
	case 541: 
	case 542: 
	case 543: 
	case 544: 
	case 545: 
	case 546: 
	case 547: 
	case 548: 
	case 549: 
	case 550: 
	case 551: 
	case 552: 
	case 553: 
	case 554: 
	case 555: 
	case 556: 
	case 557: 
	case 558: 
	case 559: 
	case 560: 
	case 561: 
	case 562: 
	case 563: 
	case 564: 
	case 565: 
	case 566: 
	case 567: 
	case 568: 
	case 569: 
	case 570: 
	case 571: 
	case 572: 
	case 573: 
	case 574: 
	case 575: 
	case 576: 
	case 577: 
	case 578: 
	case 579: 
	case 580: 
	case 581: 
#line 72 "build/lm048lib.rl"
	{
		state->on_error((_ps), (*p));
	}
	break;
#line 9980 "build/lm048lib.c"
	}
	}

	_out: {}
	}

#line 1023 "build/lm048lib.rl"

		*length = *length - (size_t)(p - data);
	}

	if(state->cs >= 582){
		state->cs = 1;
		return dequeue(state->queue, pkt);
	}else if(state->cs == 0){
		state->cs = 1;
		return LM048_ERROR;
	}else{
		return LM048_OK;
	}
}

LM048_API enum LM048_STATUS
lm048_input(char const *const data, size_t *const length)
{
	enum LM048_STATUS s =
		lm048_inputs(&lm048_default_state, data, length);
	return s;
}

LM048_API void lm048_restart(struct lm048_parser *state)
{
	if(state == NULL){
		state = &lm048_default_state;
	}

	state->cs = 1;
}

LM048_API void lm048_init(struct lm048_parser *state)
{
	state->cs = 1;
	state->on_ok_response = lm048_no_op;
	state->on_error_response = lm048_no_op;
	state->on_error = lm048_no_op_e;
	state->on_completed = NULL;
	state->queue = &default_queue;

	lm048_packet_init(&state->current,
			  default_payload,
			  LM048_DEFAULT_PAYLOAD_LENGTH);
}

LM048_API enum LM048_STATUS
lm048_packet_payload(struct lm048_packet const *const packet,
		     char *const buffer,
		     size_t const length)
{
	struct lm048_packet const *pkt =
		&(lm048_default_state.current);

	if(packet != NULL){
		pkt = packet;
	}

	if(length <= pkt->payload_length){
		return LM048_FULL;
	}

	if(pkt->payload_length < 1){
		return LM048_OK;
	}

	memcpy(buffer, pkt->payload, pkt->payload_length);
	buffer[pkt->payload_length] = '\0';

	return LM048_COMPLETED;
}
